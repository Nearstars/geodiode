import React, { useEffect } from "react";
import NavPanelThumb from "../components/nav-panel-thumb";
import Share from "../components/share";

export default function ClimateCasebook(props) {
  useEffect(() => {
    window.scrollTo(0, 0);
    document.title = "The Climate Casebook - A Series of Articles on World Climate";
    document
      .querySelector('meta[name="description"]')
      .setAttribute(
        "content",
        "The Asian Monsoon. The Pacific Northwest Climate. Climate and Population. Seasons. Just some of the topics to be found in my Climate Casebook, exploring aspects of climate through text, videos, photos, charts and maps"
      );
  }, []);

  const KoppenThumbPic = "Koppen-Tiles.gif";
  const PnwThumbPic = "PnwThumb.jpg";
  const AsianMonsoonThumbPic = "AsianMonsoonThumb.jpg";
  const PopulationThumbPic = "PopulationThumb.jpg";
  const SeasonsThumbPic = "SeasonsThumb.jpg";
  const LandAreasThumbPic = "LandAreasThumb.jpg";
  const HabitabilityThumbPic = "HabitabilityThumb.jpg";
  const LimaThumbPic = "LimaThumb.jpg";
  const MicroclimatesThumbPic = "MicroclimatesThumb.jpg";
  const TheSubtropicalQuestionThumbPic = "TheSubtropicalQuestionThumb.jpg";

  //const thisSeries = { name: "Climate Casebook", to: "/climate/casebook" };
  return (
    <>
      <div className="title-block">
        <div className="title">The Climate Casebook</div>
        <div className="summary">
          A series of individual topics on particular places around the world or more advanced concepts relating to climate
        </div>
        <div className="chapters-heading">Topics</div>
      </div>
      <div className="chapters-list">
        <NavPanelThumb src={KoppenThumbPic} to="/climate/koppen-classification" subtitle="Koppen Climate Classification" />
        <NavPanelThumb src={PnwThumbPic} to="/climate/pacific-northwest" subtitle="Pacific Northwest Climate" />
        <NavPanelThumb src={AsianMonsoonThumbPic} to="/climate/asian-monsoon" subtitle="Asian Monsoon" />
        <NavPanelThumb src={SeasonsThumbPic} to="/climate/seasons" subtitle="Climate &amp; Seasons" />
        <NavPanelThumb src={PopulationThumbPic} to="/climate/population" subtitle="Climate &amp; Population" />
        <NavPanelThumb src={LandAreasThumbPic} to="/climate/land-areas" subtitle="Climate Land Areas" />
        <NavPanelThumb src={HabitabilityThumbPic} to="/climate/habitability" subtitle="Climate &amp; Habitability" />
        <NavPanelThumb src={LimaThumbPic} to="/climate/lima" subtitle="Tropical Desert Metropolis" />
        <NavPanelThumb src={MicroclimatesThumbPic} to="/climate/microclimates" subtitle="Microclimates" />
        <NavPanelThumb src={TheSubtropicalQuestionThumbPic} to="/climate/the-subtropical-question" subtitle="The Subtropical Question" />
      </div>
      <div className="center-cont">
        <p>&nbsp;</p>
        <p>
          <b>Prefer to watch the series as a playlist on Youtube? Then</b>{" "}
          <a href="https://www.youtube.com/playlist?list=PLu83ZzwRbQvI1ULwqa_EP7z1wvOPLBB7u" target="_blank" rel="noreferrer">
            click here!
          </a>
        </p>
        <p>&nbsp;</p>
      </div>
      <Share />
    </>
  );
}
