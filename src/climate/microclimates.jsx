import React from "react";
import Chapter from "../components/chapter";
import ChapterImageList from "../components/chapter-image-list";
import ChapterImage from "../components/chapter-image";
import Credits from "../components/credits";
import B from "../components/B";

const headerImage = "microclimates-hero.jpg";
const ImageCoolForest = "microclimates-cool-forest.jpg";
const ImageUrbanHeatIsland = "microclimates-urban-heat-island.jpg";
const ImageVineyardSlope = "microclimates-vineyard-slope.jpg";
const ImageLaPaz = "microclimates-la-paz-bolivia.jpg";
const ImagePlockton = "microclimates-plockton.jpg";
const ImageMauiHawaii = "microclimates-maui-hawaii.jpg";
const ImageSfFog = "microclimates-san-francisco-golden-gate-fog.jpg";

const GlobalDistributionMapPalms = "Palm-Distribution-with-Titles.jpg";
const GlobalDistributionMap = "Continental-Climate-Global-Distribution.jpg";
const WestCoastMicroclimatesMap = "Promo-West-Coast-Microclimates.jpg";

export default function CC8Microclimates(props) {
  const images1 = [
    {
      src: ImageCoolForest,
      caption:
        "Under the canopy of a forest can be several degrees cooler than the surrounding open country on hot days",
    },
    {
      src: ImageUrbanHeatIsland,
      caption: "Large cities can be several degrees warmer than the surrounding countryside at night",
    },
    {
      src: ImageVineyardSlope,
      caption:
        "Northern or Southern facing slopes can differ in temperature by several degrees, influencing choices in vine cultivation",
    },
  ];

  const images2 = [
    {
      src: ImagePlockton,
      caption:
        "At 57°N of the equator, Plockton in Scotland can yet support the growth of palms, owing to its sheltered frost-free climate. Image courtesy of Wojsyl.",
    },
    {
      src: GlobalDistributionMapPalms,
      caption:
        "Global Distribution of Palms. Note the position of the British Isles, far above the palm line, where local sheltered frost-free conditions permit the growth of palms.",
    },
  ];

  const images3 = [
    {
      src: WestCoastMicroclimatesMap,
      caption:
        "The most pronounced of the world's microclimates occur on certain continental west coasts, where cold oceans meet sun-scorched land.",
    },
    {
      src: ImageSfFog,
      caption:
        "San Francisco is famous for its fogs that roll in from the cold Pacific Ocean. The same air masses lead to temperature differences of up to 14°C between the city and nearby inland areas.",
    },
  ];

  return (
    <>
      <Chapter
        series="casebook"
        heading="Microclimates"
        subheading="Climate changes over a matter of miles"
        headerImageSrc={headerImage}
        documentTitle="Microclimates"
        documentDescription="Microclimates, where long term weather patterns can change in just a few miles. In this video I explore the 5 basic types of microclimates with a detailed look at the oceanic influence type"
        youTubeCode="rvJ68k2sKyo"
        chapterPrevText="Lima: Tropical Desert Metropolis"
        chapterPrevUrl="/climate/lima"
        chapterNextText="The Subtropical Question"
        chapterNextUrl="/climate/the-subtropical-question"
      >
        <h3>Introduction</h3>
        <ChapterImage
          right
          src={GlobalDistributionMap}
          caption="The opposite of a microclimate - the long stretch between Eastern Europe and the Steppes of Central Russia, that is essentially the same climate"
        />
        <p>
          Imagine that you’re standing in a field on the plains of Western Russia, and by some strange quirk of fate,
          you’ve had to stand there over a whole year. You’ll have witnessed snowy winters, fresh springs, warm summers
          and wet autumns. Incredibly if you had chosen a spot 1,300km west, on the borders of Germany and Poland, or
          2,700km east, deep into the Central Asian Steppe, then your experience wouldn’t have been hugely different.
          This is because all these places, spanning 4,000km from east to west are in the same climate zone – the warm
          summer continental climate.
        </p>
        <p>
          If one were to travel from the Atlantic coasts of Mauretania, across the Sahara, the Nile Valley, Arabia,
          Southern Iran to arrive at the estuary of the Indus River at Karachi in Pakistan one would have experienced
          every day on that 8,000km journey to be more or less the same, since this whole region is within the Hot
          Desert climate zone.
        </p>
        <p>
          The fact that one can experience the same climate across thousands of continuous kilometres makes their
          opposites even more incredible. Now imagine that you are on a cool foggy coast of California, and that you get
          in your car, and drive for about 20 minutes over a set of coastal hills, and into bright sun. It’s only when
          you get out of the car that you are amazed to feel the hot breeze blowing over your face. In just a few miles
          the temperature climbed 14 degrees.
        </p>
        <p>
          How is this sudden change possible? What other areas of earth see dramatic shifts in temperature, sunshine and
          rainfall in only a few short miles? And in what other ways? Well, the answers lie in the subject of
          Microclimates, the latest case of my Climate Casebook.
        </p>
        <h3>Definition and Basic Types</h3>
        <p>
          A microclimate is defined as a noticeable change in long-term weather patterns over a short distance,
          typically within 20-30km. As climate is principally defined by long term patterns of precipitation and
          temperature, we are primarily concerned here with sudden changes in temperature and rainfall, although, as we
          shall see, other atmospheric effects, such as fog and cloud enter in.
        </p>
        <p>I have identified five essential ways in which microclimates can come about:</p>
        <ol>
          <li>Proximity to ocean water</li>
          <li>Altitude</li>
          <li>Slope direction</li>
          <li>Built environment</li>
          <li>Plant cover</li>
        </ol>
        <h3>Plant Cover, Built Environment and Slope Direction</h3>
        <p>
          Let’s look at each of these in ascending order, starting with plant cover. Anyone who has walked from a field
          into in a forest on a hot day will have felt the sudden cooling of the air, whether it be in the tropics,
          temperate or subarctic regions. Forests can drop the air and ground temperatures by a considerable margin
          throughout the year in the tropics, and on sunny summer days in temperate and sub-arctic regions. This cooling
          is the result of the shade provided by the tree canopy and the cooling effect of evapotranspiration of water
          from the trees into the air. In winter, however, the effect is reversed, with forests being slightly warmer
          over a 24-hour period than the open lands outside, due to the dark foliage absorbing more heat from the sun
          compared to lighter grasslands or indeed snow.
        </p>
        <ChapterImageList images={images1} />
        <p>
          Next there is the built environment, a fancy term to describe buildings within cities. When large areas of the
          surface are covered in concrete, asphalt and brick they absorb much of the heat from the sun during the day,
          then radiate this out at night as they slowly cool. This radiative effect also occurs during the day, but is
          not as strong. This phenomenon is known as an Urban Heat Island, and can result in large cities being several
          degrees warmer than the surrounding countryside.
        </p>
        <p>
          And then there is slope direction. In valleys that run east-west in the Northern Hemisphere, the northern
          slopes that face south receive the sun at a steeper angle to the ground compared to the opposite slopes, and
          so will be significantly warmer. This effect is noticeable in wine-growing regions of Europe, and can affect
          which grapes would be grown on which slope, and at what time they would be harvested, since this crop is
          notoriously sensitive to temperatures and humidity.
        </p>
        <h3>Altitude</h3>
        <ChapterImage
          right
          src={ImageLaPaz}
          caption="Built on steep slopes, La Paz, Bolivia experiences two different climate zones. The lower city has a Koppen code Cwb while the upper city has Cwc - the difference being warm or cool summers. Image courtesy of Canem World Tour."
        />
        <p>
          Next, we have altitude. As I covered in my Subtropical Highlands episode of Secrets of World Climate, air
          cools as altitude rises. Anyone who has driven up a mountain road will experience this in just a 30-minute
          drive. In summer the top is noticeably cooler than the bottom of such a road, while in winter, one can
          experience going from a snowless terrain into one in which there are metres-thick snow-drifts on each side of
          the road. In some cities built on steep slopes, such as La Paz in Bolivia, this can result in higher parts of
          the city being cool enough to be in a different climate zone.
        </p>
        <p>
          A less obvious effect of altitude occurs in certain low-lying areas surrounded by mountains, which can produce
          much colder winter temperatures than is typical. Cold air is more dense than warmer air and so if left
          undisturbed in such topographical conditions will sit over an area in what is known as a Cold Air Pool. In
          fact the coldest winters outside of Antarctica, experienced by the lucky town of Verkhoyansk in North-East
          Siberia, are enhanced by this effect.
        </p>
        <h3>Ocean Proximity - Frost Free Oceanic Climate</h3>
        <p>
          Ok, so these effects are all valid as microclimates, but in my view, the most noticeable and famous
          microclimates occur in the final category, the proximity of ocean water. If you’ve been watching my other
          series on climate, then you’ll know about the moderating effect that ocean water has on lands nearby, think of
          the mild climate of the British Isles, compared the Subarctic climate of Eastern Siberia at the same latitude.
          But this effect occurs over thousands of miles, and so is not in microclimate territory. But there are three
          ocean effects which can occur over local distances, and we’ll look at each of these in turn.
        </p>
        <p>
          The first is the ocean providing year-round frost-free conditions in the Oceanic climate of Britain and other
          places that otherwise experience regular winter frosts. Why is this important? Because practically all
          tropical, and many subtropical species of plants have evolved in climates that experience no frost, and so if
          exposed to it, can die-off in just a couple of nights. This effect occurs in several places in Britain, but
          only in sheltered coastal areas, including the southern coasts of the counties of Devon and Cornwall, and
          certain spots in North-West Scotland. This has led to the remarkable situation in these places where palm
          trees are part of the streetscape, a thousand miles north of the “Palm Line” of the Northern Mediterranean.
          The reason for this is that Britain receives the warm Gulf Stream ocean current which is responsible in
          general for the mild climate at these high latitudes, but in particular for local spots right on the ocean
          that are also sheltered by surrounding hills that prevent cold winds blowing out from the interior that would
          otherwise bring frost.
        </p>
        <ChapterImageList images={images2} />
        <h3>Ocean Proximity - Trade Wind Islands</h3>
        <ChapterImage
          right
          src={ImageMauiHawaii}
          caption="A satellite image of the middle section of Hawaiian Islands shows the contrast between windward and leeward sides of the islands in terms of lushness of vegetation."
        />
        <p>
          The second type of ocean-generated microclimate occurs on tropical and subtropical islands subject to trade
          winds. As mentioned in Episode 2 of my Secrets of World Climate series, Trade Winds are the consistent winds
          that blow in tropical and subtropical regions, almost always coming from one general direction. When such
          winds hit a mountainous island, this will have the effect of producing orographic lift of the air on the
          windward side, resulting in high rainfall, and leeching the ocean air of its moisture, such that the leeward
          side of the island is much drier. If you have ever visited the Hawaiian Islands, the Canary Islands or indeed
          some Caribbean Islands, you will see, quite dramatically, the effect of this on the landscape. Windward sides
          are lush and forested, while leeward sides are covered in grassland, shrubs or in some cases, even desert. My
          own personal experience of this was on visiting Gran Canaria in the Canaries, which has been dubbed a
          continent in miniature, owing to its incredibly diverse set of climates and biomes, despite being only 50km in
          diameter. On one afternoon I drove from a resort on the south coast to the capital of Las Palmas in the north.
          This took only half-an-hour, but in that time I went from a desert landscape and blistering sun over 30°C to
          grey skies at 20°C and lush green landscapes.
        </p>
        <h3>Ocean Proximity - Cold Ocean vs Hot Land</h3>
        <p>
          The final microclimate effect that I’ll look at in this chapter is that of a cold ocean meeting hot land. This
          occurs exclusively on the eastern margins of the oceans where they meet temperate westerly coastlines. In
          these areas we find Mediterranean or Desert climates, both of which experience hot and dry summers, but the
          ocean currents passing down these coastlines have come from polar regions and so carry cold water. Needless to
          say, putting these two factors together is always going to create interesting effects. In one of these areas,
          the coastline of Peru, I discussed this in detail in the previous Casebook episode, where that city
          experiences frequent fog and low cloud, and yet just a few miles inland, the air is clear, and temperatures
          much higher.
        </p>
        <ChapterImageList images={images3} />
        <p>
          Now the capital of Peru, Lima is no small city, having ten million population, but in the areas of Greater Los
          Angeles and the San Francisco Bay Area of California, up to 30 million souls experience the well-known
          microclimates of these areas. San Francisco is famous for its rolling fogs, and anyone who has visited that
          city will likely have experienced them. They are the result of a layer of cold moist air rolling in from the
          Pacific, trapped below warmer drier air above. This air soon meets the hot dry air of the Central Valley of
          California and dissipates. In just a one hour drive from San Francisco to the inland city of Tracy, the air
          temperature on any given summer afternoon can rise by as much as 14°C, with the July average high in Tracy
          being 33°C while San Francisco is only 19°C.
        </p>
        <p>
          An even more dramatic effect occurs around the City of Angels, where at the coast in Santa Monica the August
          afternoon temperature can be 21°C with low cloud, while Santa Clarita, just a 40 minute drive north is 14°C
          warmer at 35°C. The reason for an even more compressed change in temperatures is that here we have two
          ridgelines along this journey that separate the cool ocean air, known locally as the “Marine Layer” from the
          hot semi-desert interior of Santa Clarita. I experienced this phenomenon personally on many occasions during
          my time living in this region.
        </p>
        <p>
          And no I haven’t forgotten you, San Diego. You experience this effect too, to the degree that local TV weather
          spots provide a “Microclimate Forecast” during the hot months of the year. You Stay Classy, San Diego!
        </p>
      </Chapter>
      <Credits>
        <h4>SPECIAL THANKS to the following contributors who granted permission for use of their material:</h4>
        <p>
          Yunnan Mountains, China <B to="https://www.youtube.com/user/milosh9k">Milosh Kitchovitch</B>
        </p>
        <p>
          Verkhoyansk, Russia <B to="https://flic.kr/p/HeBrzN">Pierre Lanthemann</B>
        </p>
        <p></p>
        <h4>SPECIAL THANKS to the following channels, whose Creative Commons content made this video possible:</h4>
        <p>
          La Paz, Bolivia <B to="https://youtu.be/Cv2tyX3uQUo">Canem World Tour</B>
        </p>
        <p>
          Gran Canaria <B to="https://youtu.be/2aigqayvJO4">Tempo Relax</B>
        </p>
        <p>
          Santa Monica <B to="https://youtu.be/nde5T6Qprhc">The Dronalist</B>
        </p>
        <p>
          Santa Monica Mountains <B to="https://youtu.be/JnIQc_l7OFo">Sean Anderson</B>
        </p>
        <p>
          Scilly Isles Palms <B to="https://flic.kr/p/9xbmaB">UK Garden Photos</B>
        </p>
        <p>
          Torquay Palms <B to="https://flic.kr/p/rKwtvF">Torquay Palms</B>
        </p>
        <p>
          Plockton, Scotland Palms <B to="https://commons.wikimedia.org/wiki/File:Scotland_Plockton_2.jpg">Wojsyl</B>
        </p>
        <h4>The following reproduced under Fair Use Doctrine</h4>
        <p>
          San Diego Microclimate Forecast <B to="https://youtu.be/HuquCMgH8EQ">©2021 CBS 8 San Diego</B>
        </p>
        <p>
          Anchorman, The Legend of Ron Burgundy - ©2004 Dreamworks Pictures / Apatow Productions / Herzog-Cowen
          Entertainment
        </p>
      </Credits>
    </>
  );
}
