import React from "react";
import Chapter from "../components/chapter";
//import ChapterImageList from "../components/chapter-image-list";
import ChapterImage from "../components/chapter-image";
import Credits from "../components/credits";
import B from "../components/B";
import A from "../components/A";

const headerImage = "the-subtropical-question-hero.jpg";
const ImageAllC = "the-subtropical-question-all-temperate-climates-map.jpg";
const ImageCfa = "the-subtropical-question-cfa-subtypes.jpg";
const ImageCfb = "the-subtropical-question-cfb-subtypes.jpg";
const ImageTrewartha = "the-subtropical-question-trewartha-usa.png";
// const ImageUrbanHeatIsland = "microclimates-urban-heat-island.jpg";
// const ImageVineyardSlope = "microclimates-vineyard-slope.jpg";

// const GlobalDistributionMap = "Continental-Climate-Global-Distribution.jpg";

export default function CC9TheSubtropicalQuestion(props) {
  // const images1 = [
  //   {
  //     src: ImageCoolForest,
  //     caption:
  //       "Under the canopy of a forest can be several degrees cooler than the surrounding open country on hot days",
  //   },
  //   {
  //     src: ImageUrbanHeatIsland,
  //     caption: "Large cities can be several degrees warmer than the surrounding countryside at night",
  //   },
  //   {
  //     src: ImageVineyardSlope,
  //     caption:
  //       "Northern or Southern facing slopes can differ in temperature by several degrees, influencing choices in vine cultivation",
  //   },
  // ];

  return (
    <>
      <Chapter
        series="casebook"
        heading="The Subtropical Question"
        subheading="Breaking down the Humid Subtropical and Oceanic Climates"
        headerImageSrc={headerImage}
        documentTitle="The Subtropical Question"
        documentDescription="The Humid Subtropical Climate is considered by many to be too broad and so I have split this and the Oceanic climate zones into cool and mild winter variants"
        youTubeCode="uPx2tuM4W3c"
        chapterPrevText="Microclimates"
        chapterPrevUrl="/climate/microclimates"
        //chapterNextText="Microclimates"
        // chapterNextUrl="/climate/microclimates"
      >
        <h3>Introduction</h3>
        <p>
          New York is Subtropical. WHAT? The implication that this great world city, with its notoriously chill winters,
          is subtropical, was made in my Secrets of World Climate article on{" "}
          <A to="humid-subtropical">Koppen’s Humid Subtropical Climate</A>. The comments section of this video makes for
          some interesting reading. I had to go into hiding following the death threats. Ok, there were no death
          threats, but I still receive comments of protest and complaint about this climate zone being too much of a one
          size does NOT fit all when it comes to eastern temperate continental margins. How is it possible that New York
          could have the same climate, and accompanying natural vegetation, as that of New Orleans in the Deep South?
          How could Milan in the foggy Po Valley of Northern Italy have the same climate as sunny Sydney, Australia?
        </p>
        <p>
          And on a related subject how is it that London, with its cool winters, the mild Azores and even Quito, just
          miles from the equator, experience the same climate, since Koppen said they were all Oceanic, right? The
          sensible answer to all of these questions is clearly in the negative. And in this article I will pick apart
          these weaknesses of the Koppen Climate Classification and provide my own solution to the Subtropical Question.
        </p>
        <h3>Koppen Cfa and Cfb Definition</h3>
        <ChapterImage right src={ImageAllC} caption="Map of all Koppen 'C'-type Temperate Zones" />
        <p>
          The <A to="koppen-classification">Koppen Climate Classification</A> is the oldest and most widely used system
          of its type, and is simple enough to be understood by wider audience than academics. But because of its
          relative simplicity compared to other systems, limitations can be exposed, and two of these are the rather too
          broad definitions of the Cfa and Cfb climate zones – the Humid Subtropical and Oceanic respectively. These
          codes are broken down as follows. C denotes a temperate climate compared to, say, tropical or subarctic, while
          f denotes year round rain, and either a or b denoting a hot or warm summer. In the respect of these code
          explanations, everything about them is correct. All these areas would be considered temperate compared to
          subarctic, continental or tropical zones, with no extreme winter temperatures, and some relief from heat at
          some time in the year. All areas have rain that can occur at any time of the year, and all get either a hot or
          warm summer. By hot we mean average summer day/night temperatures being above 22°C and warm being below this.
        </p>
        <h3>South-East USA, NYC vs New Orleans</h3>
        <p>
          Let’s focus on Cfa and the most well-known area of this is the South Eastern United States, with this climate
          zone taking up a full quarter of the land area and over half the total population of that country. Now anyone
          who lives in this region or has some familiarity with the United States is immediately going to think that
          this cannot possibly represent a single type of climate, along with accompanying natural vegetation.
        </p>
        <p>
          Comparing two iconic cities at opposite ends of this zone, and we find that New York City and New Orleans have
          similar average highs in summer of 30 and 33 degrees Celsius respectively. With high humidity and regular
          thunderstorms, both these cities fit well the hot and wet summers that typify the Humid Subtropical Climate.
          But it’s when we compare winters that we run into trouble. New York has a January day/night average of just
          above freezing, with night low averages of minus -2 and day average highs at a chilly 4 celsius. So with
          regular frosts and a thick overcoat required just to get about in the daytime, this would fit anyone’s
          description of a proper winter.
        </p>
        <p>
          In New Orleans, however, things are a lot balmier, with a day time high of 17 celsius, and night time lows
          significantly above freezing at 8 celsius. This is a clearly very different winter picture in terms of human
          experience. And the type of natural vegetation we find all along the Gulf Coast, with cypress, hickory, oak
          and pine forest dominating differs much compared to the north where spruce, fir, maple, beech and birch are
          most common.
        </p>
        <p>
          Similar comparisons can be made around the world in the other Cfa regions. Cool winters are experienced in
          Northern Italy, Central China and Japan, while mild winters predominate in the Southern Hemisphere zones
          around the Rio de la Plata of South America, the Natal of South Africa, and the Eastern Seaboard of Australia.
        </p>
        <h3>Trewartha Climate Classification</h3>
        <ChapterImage
          right
          src={ImageTrewartha}
          caption="Trewartha Climate Classification Map of the USA. Image Courtesy of Adam Peterson."
        />
        <p>
          So this is clearly a problem of too much generality in this Koppen climate zone. Well what about other climate
          classifications? Do they address it? An attempt to fix some of Koppen’s issues in the temperate band was made
          in the 1960s by Glenn Thomas Trewartha at the University of Wisconsin - Madison. To go into detail about the
          Trewartha system is beyond the scope of this presentation, but in short, he does address the issue of Koppen
          Cfa somewhat by separating out the zone into two regions. The classic Cf Humid Subtropical zone where the
          average day/night temperatures for 8 months of the year are above 10 celsius, and Do where they are below this
          temperature, but with winter averages still above freezing. But his classification, in my opinion, introduces
          other issues, in that this Oceanic zone ends up suggesting that the Pacific North-West has a similar climate
          to Kentucky, which is clearly not the case.
        </p>
        <p>
          Trewartha’s decision to use the 10 celsius average isotherm over 8 months of the year was driven mostly by
          predominant vegetation type. But for me, and probably most non-academics, when we think of climate type, we
          are thinking about our own perception and experience of temperature, humidity and patterns of rain or snow.
          And this is certainly borne out in the vast majority of comments I receive on my videos.
        </p>
        <h3>A Solution to the Cfa Problem</h3>
        <p>
          So to cater for the mob, I have developed my own isotherm to separate out the Cfa zone into what would be
          perceived as either a cool or mild winter. I chose a day/night average of 10 celsius for this, because such an
          average rules out night frosts in all but exceptional circumstances, and so will guarantee the thriving of
          subtropical species of plants, such as palms. Further, it provides for comfortable winter daytime peaks of 15
          celsius or more – that’s t-shirt weather if you’re actively out and about.
        </p>
        <h3>Cool vs Mild Winters Globally for Cfa</h3>
        <ChapterImage
          right
          src={ImageCfa}
          caption="Map of Cool and Mild Winter splits for the Humid Subtropical Climate"
        />
        <p>
          For the south-eastern United States, this line runs fairly close to the Gulf Coast from Texas to along the
          northern border of Florida and then following the Atlantic coast up to about Charleston, South Carolina. Bear
          in mind, like any divisions on a map, changes are gradual, so the perception shouldn’t be that immediately
          above this line it’s a cool winter, or below it, it’s mild. The further north you go, the more likely you are
          to encounter winter frosts, and the number of t-shirt days in winter is going to reduce.
        </p>
        <p>
          In the central south-eastern part of South America, Humid Subtropical mild winters are experienced in most of
          the region except for the plains inland from and south-west of Buenos Aires.
        </p>
        <p>
          All of Europe’s small chunks of Humid Subtropical climate are cool winters, owing to these being the Cfa
          regions with the highest latitude. South Africa’s small chunk around the Natal coast is all mild winter, owing
          to its latitude much closer to the equator. At a similar latitude, the same story goes for Australia with all
          of the eastern seaboard experiencing mild winters.
        </p>
        <p>
          In Asia, we find the line between cool and mild winter varying in latitude dependent upon topography. In India
          and Pakistan, the monsoon variant of the Humid Subtropical climate has mild winters throughout the
          subcontinent due to the protection of the Himalayas from icy winter Siberian winds. In China, lacking such
          protection, the line is closer to the equator, and actually runs more or less between the monsoon and
          year-round rain variants of this climate. Moderated by the warm South China Sea, Taiwan and the Ryukyu chain
          of Japanese islands that includes Okinawa experience mild winters while the southern tip of Korea and all of
          Humid Subtropical Japan get cool winters.
        </p>
        <h3>Oceanic Climate Cool and Mild Winters</h3>
        <ChapterImage
          right
          src={ImageCfb}
          caption="Map splitting the Cfb Oceanic zone into Cool &amp; Mild Winters, plus the Subtropical Highland type"
        />
        <p>
          What about anomalies in Humid Subtropical’s sibling, the <A to="oceanic">Oceanic climate</A>, where summer
          temperatures are cooler. Do we find cool and mild winters? In fact we do, although only in a small number of
          places do we find mild winters as Oceanic regions tend to be further toward the poles. For instance, all of NW
          Europe, where this climate dominates has cool winters, as do smaller instances in British Columbia in Canada,
          and in southern Chile. Where we do find mild winters tends to be in latitudes closer to the equator, but where
          dominant ocean winds moderate summer temperatures, and keeping things cooler. The Azores, in the eastern North
          Atlantic at the latitude of Portugal have a climate close to a mild winter Oceanic, but also bordering on
          Mediterranean and Humid Subtropical.
        </p>
        <p>
          Away from the coast of the Natal in South Africa and Lesotho, we can find a significant region that could be
          described as Mild Winter Oceanic. Although really this is a kind of Subtropical Highland form of Humid
          Subtropical, as it’s at the same latitude, but simply raised in altitude. The same could be said about the
          region around Curitiba in Southern Brazil, as well as upland and inland parts of the Australian Eastern
          Seaboard – it’s a grey area in terms of these “Oceanic” regions that lie at raised altitudes in Subtropical
          regions. A clearer case of mild winter Oceanic in South Africa, however, is the central coast around the city
          of George.
        </p>
        <p>
          The largest and most clear-cut example of mild-winter Oceanic, however, is found, funnily enough, in Oceania,
          specifically the coast of Victoria, Australia, including the state capital Melbourne, and the northern tip of
          North Island, New Zealand, including that country’s largest city, Auckland. Further inland into Victoria, and
          the moderating influence of the Southern Ocean results in cooler winters, while in Tasmania and the rest of
          New Zealand, the more polar latitudes lead to cooler temperatures in general.
        </p>
        <h3>The Subtropical Highland Climate within Cfb</h3>
        <p>
          The last remaining anomaly I’ll cover in this article I’ll briefly touch on, since I have gone over this in
          more detail in my other articles, and that is the Koppen Cfb code shared with the Oceanic and{" "}
          <A to="subtropical-highlands">Subtropical Highland climates</A> found close to the equator, but most notably
          in Colombia and Ecuador where large cities including both capitals are located. In these regions there are no
          seasons in regard to temperature, and so there is no concept of winter at all. But they share the same code as
          NW Europe? Well it’s simply that Koppen categorised this zone as having peak day/night temperatures no higher
          than 22 celsius and lowest temperatures being above freezing. There wasn’t really any specificity regarding
          variation of temperature beyond that. And of course, we get year-round rainfall in each. Aware of this from
          the beginning, I kept these two types of zone apart, and each was addressed in separate chapters in my Secrets
          of World Climate series.{" "}
        </p>
        <h3>Summary</h3>
        <p>
          In summary, then, any classification system will have its limitations. The more detail you go into in natural
          systems such as climate, then the more complex things become. For general patterns, and ease of understanding,
          Koppen works, I would say, in 90% of places around the world. But it’s the 10% that gets the YouTube comments
          section overflowing. I hope this article at least addresses some of those comments.
        </p>
      </Chapter>
      <Credits>
        <h4>SPECIAL THANKS to the following contributors who granted permission for use of their material:</h4>
        <p>
          Quito, Ecuador <B to="https://www.youtube.com/channel/UCRzFzMrebCld4Ujroac7GRw">Thomas Noisel</B>
        </p>
        <p></p>
        <h4>SPECIAL THANKS to the following, whose Creative Commons content made the video possible:</h4>
        <p>
          Bogota, Colombia <B to="https://youtu.be/bjGnAAH1fsE">Dario Fuentes</B>
        </p>
        <p>
          New Orleans <B to="https://youtu.be/5sIflXe5dp0">AirWorthy Tours</B>
        </p>
        <p>
          Azores <B to="https://youtu.be/IagCV4GVoyo">Mikhail Schwartz</B>
        </p>        
        <p>
          Flores Island, Azores <B to="https://youtu.be/dvzNhUBqB7Y">Mikhail Schwartz</B>
        </p>
        <p>Header image for this article, showing the Azores, courtesy of Mikhail Schwartz</p>
        <h4>The following reproduced under Fair Use Doctrine</h4>
        <p>
          Glenn Thomas Trewartha Portrait{" "}
          <B to="https://geography.wisc.edu/madgeognews/support/">University of Wisconsin</B>
        </p>
      </Credits>
    </>
  );
}
