import React from "react";
import Chapter from "../components/chapter";
import ChapterImage from "../components/chapter-image";
import ChapterImageList from "../components/chapter-image-list";

import Credits from "../components/credits";
import B from "../components/B";

export default function BiomesDeepOceans(props) {
  const headerImage = "deep-oceans-hero.jpg";

  const ImageTopography = "Deep-Oceans-Topography.png";
  const ImageWindOnWaves = "deep-oceans-wind-on-waves.jpg";
  const ImageCurrents = "deep-oceans-currents.png";
  const ImageThermocline = "Deep-Oceans-Thermocline.png";

  const images1 = [
    {
      src: ImageWindOnWaves,
      caption:
        "Wind blowing over a water surface produces waves which in turn pull along the mass of water in a current",
    },
    {
      src: ImageCurrents,
      caption: "World Ocean Currents Map",
    },
  ];

  return (
    <>
      <Chapter
        series="biomes"
        seriesChapter="12"
        heading="Deep Oceans"
        subheading="Abyssal Plains, Ocean Trenches and their Strange Creatures"
        documentTitle="Deep Ocean Biome"
        documentDescription="Deep Oceans - how life still thrives in the dark, cold high pressure environment"
        headerImageSrc={headerImage}
        youTubeCode="ZFSN-EHahmU"
        chapterPrevText="Shallow Seas"
        chapterPrevUrl="/biomes/shallow-seas"
      >
        <p>
          Cold. Still. Dark. When describing the largest of all the world’s biomes, these three words would perhaps be
          the furthest from your mind. And it’s true that here we do not find an abundance of life, but the sheer scale
          of the volume of the oceans means that a significant proportion of all life on earth is found in this alien
          place. Millions of years of the same, stable conditions under hundreds of atmospheres of pressure have evolved
          specialised life-forms that bear little resemblance to anything found in the shallow seas or indeed the land.
          With no light, life must rely upon the discarded waste from above, and yet this realm yielded a remarkable
          secret kept hidden until just a generation ago. This strange, dark and vast world is the final biome we’ll
          look at in this series. It is the world of the deep oceans.
        </p>
        <h3>A Biome of Total Darkness</h3>
        <p>
          When making this video, I had to throw out everything I’d learned in researching the previous dozen chapters
          in this series. Because in all the other biomes of the world, the basis of each ecosystem was a miraculous
          process known as photosynthesis, the creation of organic matter from CO2, water and light. The deep oceans
          have little to no light, and so for life to survive here, it must rely upon debris falling from the sunlit
          seas above… or something entirely different. But before we can understand what life survives here, we must
          understand a bit more about the characteristics of the oceans themselves.
        </p>
        <p>
          In the last episode we touched on the origins of the oceans, and why it is that they take up 71% of the
          Earth’s surface. But the emphasis in that video was upon the shallow seas adjoining the great continental land
          masses. These shallow waters make up only a tiny fraction of the total volume of water in the oceans, which is
          estimated to be 1.3 billion cubic kilometers, or 1.3 billion, billion cubic meters. That’s around 97% of all
          the water on the surface of Earth.
        </p>
        <h3>Abyssal Plains, Ocean Trenches and Mid-Ocean Ridges</h3>
        <ChapterImage right src={ImageTopography} caption="A representation of typical ocean topography." />
        <p>
          The depth of the open ocean varies considerably but the majority of it falls from the continental shelf down
          to depths of between 3,000 and 6000 meters, in what are known as abyssal plains. These are extensive flat
          areas of thin ocean crust that in total cover about half of the surface of the earth. Ocean ridges, where most
          of the new crust of the Earth’s surface is formed, lie at relatively shallower depths. The graveyards of these
          oceanic tectonic plates, however, lie in deep trenches, when they are forced below continental or other
          oceanic plates. The deepest of these trenches is the Marianas Trench, east of the Philippines, with the
          Challenger Deep being a further depression within this trench, with its lowest point being 10,920m below sea
          level, the lowest piece of the Earth’s crust to be found anywhere.
        </p>
        <p>
          With all that weight of water sitting upon it, its no wonder that pressures are extremely high at such depths.
          Water pressure increases by 1 atmosphere, or 1 bar, with every 10m of depth, and so at a typical abyssal plain
          depth of 4,000m, the pressure is at around 400 bar, or almost 6,000 psi, while in the Challenger Deep, the
          pressure reaches almost 1,100 bar, or 16,000 psi.
        </p>
        <h3>Ocean Surface Currents</h3>
        <p>
          Because the oceans are so vast, and, naturally, so flat at the surface, winds are free to develop as part of
          large weather systems, be they tropical, temperate or polar in origin. The nature of climate dictates that
          weather systems will form in certain typical ways in certain areas, and produce a prevailing wind direction on
          a given patch of ocean. When the wind blows in the same direction in this way, friction between the air and
          water surface will produce ripples, and waves which in turn move the water in the ocean as a whole mass. This
          is the genesis of ocean surface currents which carry warm tropical water up to temperate latitudes, or cool
          temperate waters down to the tropics. Some currents, such as the Gulf Stream off the Eastern Seaboard of North
          America are so powerful that a water velocity of about 1m/s is achieved, and the effect of this current, which
          originates in the warm seas of the Caribbean, is to warm the continent of Europe by a significant fraction
          beyond what it should otherwise be. Cold ocean currents returning to the tropics, such as the Peru current
          result in cold air that can hold little moisture, and contribute to the driest desert on Earth. So the effect
          of these ocean currents on global climate cannot be overstated.
        </p>
        <ChapterImageList images={images1} />
        <h3>Thermohaline Circulation</h3>
        <p>
          Such currents only operate in about the first 100m of ocean depth, and so the water below this is generally
          still. However, there is a more complex system of currents that run far down into the depths and span the
          entire globe. The water of the oceans is saline, meaning salty, in that it is composed of about 3.5% of
          dissolved salts, the majority of which is sodium chloride, or common table salt. This salt fraction is highly
          uniform throughout all the oceans, but there are places where the salinity will increase, such as when intense
          winds evaporate water molecules, but leave the salt behind in the water, and when sea ice forms, also leaving
          salt behind. When this occurs in cold surface water in the polar regions, this more saline water, being more
          dense, falls into the depths below and, having to go somewhere, is forced toward the equator and eventually
          around the globe, resurfacing in other areas in a gigantic conveyer belt of water. This system is known as the
          Thermohaline Circulation, since it is a combination of water temperature and salinity that acts as its motor.
          Incidentally, where great upwellings occur, when deep ocean water comes to the surface, rich mineral sediments
          are brought up with it, and these act as a fertiliser for phytoplankton at the surface, which then rapidly
          reproduce into huge algal blooms. Being the basis of the food chain, such abundance of plankton has a knock-on
          effect in all other species up this food chain, and such areas, off the coast of Newfoundland, Peru and the
          Eastern Cape of South Africa, are known for their rich fishing grounds.
        </p>
        <h3>Water Temperature and Depth</h3>
        <ChapterImage right src={ImageThermocline} caption="How ocean temperature changes with depth" />
        <p>
          Such movement of water between the layers of the ocean is the exception, however, and not the norm. This is
          because in general, the layers of ocean water sit stably on top of each other, since surface water is heated
          by the sun, while the water below is not. As cold water is more dense than warm water, it will therefore
          stably sit below the warmer surface waters. This effect is most pronounced in the tropics, while at the poles,
          the temperature of seawater is more or less the same from the surface down into the abyssal depths. Through
          wave action, surface water gets mixed down to a depth of about 500 metres or so, and so the temperature falls
          only slightly down to this point. Below 500m however, and waves no longer have an effect, and with so little
          sunlight reaching this depth, the water suddenly becomes much colder, until at 1000m in depth, the water has
          cooled to below 7°C everywhere on the planet. This sudden change in water temperature is known as the
          Thermocline, and the resulting change in water density is so pronounced that submariners will pick it up as a
          “false bottom” since sonar will reflect off this layer. Below the thermocline, the water continues to cool as
          we go deeper, but at a much slower rate, with most of the ocean’s water being at around a chilly 4°C.
        </p>
        <h3>Life in the Deep Ocean - Scavengers of Marine Snow</h3>
        <p>
          Ok so that tells us about the environment of the deep oceans. So what kind of life can survive in such a
          regime of intense pressure, cold temperatures and, most importantly, the absence of light? To begin, we have
          to look at what is going on above, in the sunlit zones of the ocean. Here, phytoplankton build their tiny
          bodies from photosynthesis, and in the open ocean these in turn are fed upon by a whole food chain of
          zooplankton, krill, fish, sharks and whales. In the twilight zone of the middle depths, fish can swim up to
          the shallower depths at night to feed upon plankton or krill, and be safe from predators that would otherwise
          see them during the day. But below this zone, creatures are wholly dependent upon what falls down to them. All
          this energy and biomass in the sunlit zone has to go somewhere, be it in the form of organism death or
          excreta, and… the only way is down, a long way down to the deep ocean floor. It is estimated that a piece of
          organic debris descends the ocean at about 100m per day. This allows for plenty of time for creatures at
          various depths to help themselves to these falling meals. The technical term for this is marine snow, since it
          mostly comprises microscopic or particulate sized debris, since most of upper reaches are indeed plankton or
          krill. However, large animals also eventually die, and especially as in the case of the death of a large
          whale, such a carcass, when it eventually reaches the ocean bottom, will act as a meal for thousands of
          creatures over many months.
        </p>
        <p>
          The dark, high pressure abyssal reaches of the ocean produce probably the most alien looking lifeforms out of
          any found on our world. The most immediately noticeable adaptation to the lack of light is bioluminescence.
          Many creatures use light generated within their bodies to attract a mate, or even prey, or to camouflage
          themselves against being a silhouette from predators below by faintly lighting the undersides of their bodies.
          Siphonophores are perhaps the most spectacular in this regard, and amount to a rare beauty in a world
          otherwise populated by, let’s be honest, some pretty horrific creations. Many of us are familiar with pictures
          of the anglerfish family, which uses a bioluminescent lure in front of its face to attract prey. Other forms
          appear to have come from a different planet, being unlike anything we find in other biomes. With no light
          available, there are no plants, and so most of the biomass is in the form of animals either free swimming, or
          fixed to the ocean floor. Corals can be found here, but lack the photosynthetic symbiotes of their tropical
          sunlit cousins. Sponges are common, and can grow to over a metre in diameter, as are starfish that scavenge
          upon the bottom for any marine snow that might have made it through all the ocean layers above.
        </p>
        <p>
          Most of the forms that are here are believed to be very ancient in origin, having changed little over hundreds
          of millions of years, since the depths of the ocean, as mentioned, are extremely stable in terms of
          temperature, and so are isolated from the direct effects of any shifts in global climate on the surface. These
          adaptations must cope with intensely high water pressures, which can affect even the formation of proteins at
          the most basic level within cells, and so any species attempting to move into the depths from above has found
          it very difficult to cope with competition from the ancient species already there.
        </p>
        <p>
          Another adaptation appears to be in the form of gigantism. Species found in the depths that are related to
          more shallower species are usually much larger, such as giant spider crabs, and most famously, the giant
          squid, which can grow to a length of 12-13m, and possessing eyes the size of dinner plates – the largest in
          the animal kingdom. The giant squid is the favourite prey of the sperm whale, which will dive to depths as far
          as 7,000m in search of it. The sperm whale is able to survive such crushing depths through the evolution of a
          rib cage made of flexible cartilage instead of bone, which allows the collapse of the lungs to a minute size
          on its long way down.
        </p>
        <p>
          It is said that we know more about the surface of the moon than we do of the ocean bottom, and one of the
          reasons why is because specimens of creatures caught in the depths do not survive the journey back to the
          surface intact, since their bodies are wholly adapted to the intense pressures found only down there. Also,
          the development of submersibles that can survive such pressures are usually very expensive one-off creations,
          such as the Trieste that first ventured to the Challenger Deep in 1951.
        </p>
        <h3>Hydrothermal Vents and a Whole Biome Founded upon Chemosynthesis</h3>
        <p>
          And it’s because of such inaccessibility that perhaps the greatest secret of the depths was only revealed
          within my own lifetime. In 1977, a submersible exploring the hydrothermal vents in the geologically active
          mid-ocean ridge off the Galapagos islands discovered, in the total absence of light, a whole ecosystem
          thriving, with the basis of the food chain not being marine snow from above, but bacteria that were building
          their bodies through chemosynthesis of hydrogen sulphide found in the hot, chemical rich waters of the vents.
          Subsequent visits to hydrothermal vents around the world by other teams have revealed more communities living
          in such fashion.
        </p>
        <p>
          For thousands of years we have been aware that plants have formed the basis of all food chains, which we
          ourselves ultimately depend upon. So it is fitting that at the very end of this series that has explored all
          these myriad biomes that depend ultimately upon photosynthesis, that we reveal the one exception to this, the
          secret so recently uncovered, of chemosynthetic bacteria forming the basis of a completely unique biome
          altogether independent of the life-giving star that is our sun. What this discovery implies for the potential
          for life on other planets, such as the ocean of Jupiter’s moon, Europa that lies in the darkness beneath
          kilometres of ice, or perhaps the chemically rich surface of Saturn’s moon Titan, is anyone’s guess.
        </p>
        <p>
          Just as we believe we have understood life, something new comes along to shake our understanding. The effect
          of life upon our world is complex, ever-changing, and in so many places, stunning in its beauty. And without
          it, we, as conscious self-aware beings who depend upon the biomes of the world for our own food and
          sustenance, would not exist to enjoy the privilege of gazing upon these living landscapes of Earth.
        </p>

        <h3>Coursework Questions</h3>
        <ol>
          <li>What are abyssal plains? What is the typical depth of the sea bottom here?</li>
          <li>What are ocean trenches and why do they form? How deep can these go?</li>
          <li>What are mid-ocean ridges? What significant features might be found here?</li>
          <li>How does water temperature in the ocean vary with depth? How does this profile differ by latitude?</li>
          <li>How do ocean currents form? How do these currents impact on global climate patterns?</li>
          <li>What is the main source of energy for creatures living in the dark ocean?</li>
          <li>
            What is the most striking form of adaptation of animals living in the perpetual dark? Why is this adopted?
          </li>
          <li>
            What are hydrothermal vents, and why are they of global significance in terms of biomes? What is
            chemosynthesis?
          </li>
        </ol>
      </Chapter>
      <Credits>
        <h4>EXTRA SPECIAL THANKS TO THE NATIONAL OCEANOGRAPHIC AND ATMOSPHERIC ADMINISTRATION (NOAA)</h4>
        <p>
          Without their <B to="https://www.flickr.com/photos/usoceangov/">Extensive Image Library</B> this production
          would not have been possible
        </p>
        <h4>THANKS TO THE FOLLOWING FOR SHARING IN THE CREATIVE COMMONS:</h4>
        <p>
          World Elevation Map:{" "}
          <B to="http://all-geo.org/metageologist/2012/01/a-journey-through-the-geology-of-mountains/">All Geo</B>
        </p>
        <p>
        Hydrothermal Vents <B to="https://youtu.be/R1koFEKfmLw">NOC News</B>
        </p>         
        <p>
          Fishing Boat <B to="https://youtu.be/ooPIakgwlRE">Gabi Comojo</B>
        </p>
        <p>Anglerfish - Edith Widdler</p>
        <p>
          Sperm Whales <B to="https://youtu.be/VOG_29JCX7w">Terra Azul Azores Whalewatching</B>
        </p>
        <p>
          Giant Spider Crab <B to="https://flic.kr/p/bBdAC9">Brian Gratwicke</B>
        </p>
        <p>
          Giant Squid{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=66155579">Natural History Museum, London</B>
        </p>
        <p>
          Plankton <B to="https://youtu.be/L5Bn0PIjrcg">Pacific Plankton</B>
        </p>
        <p>
          Sharks and Whales <B to="https://youtu.be/BzrFQm3CSLs">Sailing Zingaro</B>
        </p>
        <p>
          Full Earth <B to="https://youtu.be/UZOOL3kYU9k">Blueturn Earth</B>
        </p>
        <p>
          Late Heavy Bombardment <B to="https://youtu.be/5xUROWDJ1Qk">Earth.Parts</B>
        </p>
        <p>
          Seawater Composition{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=5120699">Tcncv / Hannes Grobe / Stefan Majewsky</B>
        </p>
      </Credits>
    </>
  );
}
