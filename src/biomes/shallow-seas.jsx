import React from "react";
import Chapter from "../components/chapter";
import ChapterImage from "../components/chapter-image";
import ChapterImageList from "../components/chapter-image-list";

import Credits from "../components/credits";
import B from "../components/B";

export default function BiomesShallowSeas(props) {
  const headerImage = "shallow-seas-hero.jpg";

  const ImageNoSeaMap = "shallow-seas-map.jpg";
  const ImageIceCapMelt = "shallow-seas-ice-cap-melt.jpg";
  const ImageLastGlacialMaximum = "shallow-seas-last-glacial-maximum.jpg";
  const ImageSeawaterComposition = "shallow-seas-seawater-composition.jpg";
  const ImageSeawaterLightPenetration = "shallow-seas-seawater-light-penetration.jpg";
  
  const images1 = [
    {
      src: ImageLastGlacialMaximum,
      caption:
        "Sea levels during the last glacial maximum. Image courtesy of NOAA",
    },
    {
      src: ImageIceCapMelt,
      caption:
        "Sea levels if the ice caps of Greenland and Antarctica were to melt. Image courtesy of Kevin Gill.",
    },    
  ];

  const images2 = [
    {
      src: ImageSeawaterComposition,
      caption:
        "Seawater ionic composition. Sodium chloride (common table salt) forms the vast majority of dissolved solids. Image courtesy of Tcncv / Hannes Grobe / Stefan Majewsky",
    },
    {
      src: ImageSeawaterLightPenetration,
      caption:
        "Light penetration in seawater and its effects on marine life. Image courtesy of NOAA.",
    },    
  ];

  return (
    <>
      <Chapter
        series="biomes"
        seriesChapter="11"
        heading="Shallow Seas"
        subheading="Plankton, Kelp, Coral and Life within Sunlit Seas"
        documentTitle="Shallow Sea Biome"
        documentDescription="Shallow Seas - how life has evolved in seawater with light"
        headerImageSrc={headerImage}
        youTubeCode="atY1szpyeMs"
        chapterPrevText="Highlands"
        chapterPrevUrl="/biomes/highlands"
        chapterNextText="Deep Oceans"
        chapterNextUrl="/biomes/deep-oceans"
      >
        <p>
          Water. Everywhere. Where the crust of our planet falls away, the billion cubic kilometres of water on our
          planet find their level. In the interface between land and sea, this water is shallow enough that light can
          reach the bottom. And with that solar source of energy, a myriad of unique lifeforms has evolved to thrive in
          this salty world. Algae and plankton, corals and kelp, jellyfish, molluscs, crustaceans, fish and whales… the
          abundance of life here equals any found on land. In fact these regions could well be the origination point of
          life on our planet. Join me in this series on biomes, as we finally dip our toes in… to the shallow seas of
          earth.
        </p>
        <ChapterImage
          right
          src={ImageNoSeaMap}
          caption="A relief map of the Earth including ocean bottoms. Continental shelves are clearly visible extending away from many coastlines. Image courtesy of NOAA."
        />
        <h3>Formation of the Oceans</h3>
        <p>
          Earth is often referred to as the “blue planet”, and this is because 71% of its surface is covered by water.
          Seas and oceans appear blue because water absorbs more red light leaving most of the blue to reflect back to
          the observing eye. When the earth formed over four billion years ago, a small fraction of the original mass
          was ice. The initial violent phase of the planet’s formation resulted in very high temperatures, well above
          the boiling point of water, so this initial ice would have been in the form of a steam atmosphere. But as the
          surface eventually cooled, this water vapour condensed and settled upon the crust, pooling in lower lying
          areas to form the oceans. So the size of the oceans today is related to that initial mix of ice and rock from
          the early days of this part of the solar system. Had it been more, then the earth would have been covered
          completely in ocean. Had it been less, and we would have been looking at only isolated salty lakes surrounded
          by dry land.
        </p>
        <h3>Shallow Seas vs Deep Oceans</h3>
        <p>
          The seas and oceans are such a large subject, that it’s impossible to cover all aspects of it in a single
          episode. So I have subdivided the topic into two. In the next chapter I’ll discuss the deep oceans that hold
          the vast majority of the water on earth, their topography, their strange life forms, and how these large
          masses of water influence the climate of the world overall. In this episode, however, I’m going to discuss the
          small fraction that lies close to the land and where light can reach the bottom – the shallow seas.
        </p>
        <h3>Continental Shelf</h3>
        <p>
          When you think of continents, most of you will likely imagine the outlines of the great land masses, The
          Americas, Eurasia, Africa, Australia and Antarctica. But these continents, in terms of a strict geological
          definition actually extend outward some way into the sea, in the form of shallow continental shelves, before
          suddenly dropping down into the abyssal plains of the deep oceans. The width of these shelves varies
          dramatically from place to place around the world. For instance, in many parts of the coasts of California,
          Western Mexico, Peru and Chile there is virtually no shelf, with the land sloping down into the far depths
          after only a kilometre or so. North of Siberia into the Arctic Ocean, however, and the shelf extends as much
          as 1,300km (800 miles) before dropping away. The British Isles lie upon the shelf that extends out from
          Western Europe, while shelf connects the land of North America and Eurasia at the Bering Strait. But the
          Mediterranean and Caribbean Seas are really oceans, in the sense that they fall away to deep abyssal plains
          where no light can reach.
        </p>
        <p>
          The continental shelfs of today have formed over tens of millions of years through the deposition of sediment
          from land rivers as well as the build up of dead plant and animal life.
        </p>
        <h3>Climate Change and Sea Level</h3>
        <p>
          The earth goes through many natural cycles of climate change as a result of slight perturbations in its orbit
          around the sun, the sun itself varying in energy output, and the Earth wobbling on its own axis. Such changes
          in surface temperature result in a redistribution of the total water between the oceans and ice caps at the
          poles. During colder times, the usual process of evaporation of the oceans goes out of balance, with the
          resulting precipitation of this water vapour accumulating as snow on expanding and deepening ice caps. As a
          result, the sea level falls. When warmer temperatures come, the ice melts, and the water is returned to the
          ocean, leading to sea level rise. Because continental shelfs are gently sloping, this has led to dramatic
          changes in the land area of continents throughout geological time.
        </p>
        <ChapterImageList images={images1} />
        <h3>Composition of Seawater, Global Salinity</h3>
        <p>
          As anyone who has swum in the sea knows, seawater is saline, meaning it has a relatively high amount of
          dissolved salts in it compared to freshwater. The actual composition of seawater globally is remarkably
          uniform, with the salinity varying only between 3.4 and 3.6% by mass when considering large volumes of ocean.
          In localised areas, such as near melting icebergs, or by large river estuaries, the salinity is less. Where
          high evaporation occurs, such as in tropical seas on a clear day, salinity can increase. The majority of the
          salts are in the form of sodium chloride, that is, common table salt. Any organism that wants to survive in
          the ocean must evolve its structure and internal organs so that it can cope with this amount of dissolved
          salt. This is important as virtually all organisms have some kind of membranes within their systems that are
          designed to pass a certain proportion of salts or water. This explains, for instance, why species of fish are
          considered of either freshwater or seawater type, with very few species able to tolerate both environments.
        </p>
        <h3>Light Penetration in Seawater</h3>
        <p>
          Seawater is very slightly opaque, meaning that over a long enough distance, light is absorbed and will not
          reach the far point. This absorption varies by wavelength, with blue light able to penetrate the furthest.
          Since all but a handful of specialised life-forms depend upon sunlight as the base of the food chain, what
          depths of water have what amount of light will determine its ability to support life. In general terms, water
          down to a depth of about 200m is regarded as “sunlit”, in that it can support algae and plankton, upon which
          almost all other sea-creatures depend. Below 200m in depth and we have a “twilight zone” where only faint blue
          light reaches, and below 1000m we have the dark zone where effectively all sunlight is extinguished. We’ll
          pick up these two latter zones in the next episode.
        </p>
        <ChapterImageList images={images2} />
        <h3>Plankton</h3>
        <p>
          But in the sunlit zone, photosynthesis is possible, and microscopic life, believed to be the oldest type of
          life on earth, has evolved to capture this light while suspended in seawater. The collective term for this
          life is plankton, which in its strictest sense refers to non-motile organisms – those that just drift in the
          water and cannot propel themselves. In this microscopic world, a vast array of different types of organism
          exist across all biological kingdoms. These can be grouped as follows:{" "}
        </p>
        <ul>
          <li>Phytoplankton – plant-life such as algae and diatoms that photosynthesise sunlight. </li>
          <li>Zooplankton – microscopic animals such as protozoa that feed upon phytoplankton. </li>
          <li>Mycoplankton – fungi that recycle dead plant and animal material.</li>
          <li>Bacterioplankton – bacteria which can be either photosynthetic or feed on other organisms, and</li>
          <li>
            Virioplankton – viruses that outnumber all other types, but which are much smaller in size, and which
            regulate the numbers of bacteria within water.
          </li>
        </ul>
        <p>
          All these types play their role in maintaining balance within the ocean ecosystem, and indeed the entirety of
          the earth. Although making up only about 1% of total global biomass, phytoplankton produce around half of all
          global oxygen through photosynthesis. This process of converting light and carbon-dioxide, and building plant
          matter is the base of the shallow seas food-chain upon which all other forms are dependent. Occasionally,
          conditions originate where massive blooms of this microscopic life are produced, and are so large that they
          can be seen from space.
        </p>
        <h3>Seaweed, Kelp and Animal Food Chains</h3>
        <p>
          But plant life within the sea can extend beyond the microscopic. Seaweed is found throughout the world and can
          be either free-floating, such as sargassum, or attached to the sea bottom as in kelp. Most seaweed is actually
          a form of multicellular algae, and the thousands of species are roughly subdivided into the colours of red,
          green and brown.
        </p>
        <p>
          The marine animal food-chain is well-known to most of us. Krill, that is, small shrimp, feeding on plankton,
          fish and crustations feeding on krill, bigger fish and sharks feeding on other fish, whales feeding on krill
          and fish. Altogether, these food chains are as complex and diverse as any on land.
        </p>
        <h3>Coral</h3>
        <p>
          But one thing that is unique to the shallow seas, and not found either on land or in the deep oceans, is one
          of the most remarkable living wonders to be found anywhere on earth. It can be visible from space, and is the
          only living thing to create land from the sea. These are colonies of specialised animals that have a special
          symbiotic relationship with microscopic plants within their bodies. It sounds like something from a
          science-fiction novel, doesn’t it? And yet they are right here, within the shallow tropical waters of our
          planet. Of course, I refer to corals, which, through the excretion of calcium-carbonate rock at their base,
          create coral reefs that, over centuries, can grow to the size of islands. A coral consists of clonal colonies
          of the same organism, although sexual reproduction also occurs between corals through the release of spores.
        </p>
        <p>
          While some corals use lures and tentacles to catch plankton or even small fish, most corals rely upon the
          presence of symbiotic algae such as dinoflagellates within their tissues to photosynthesis light and provide
          their source of energy in this way. It is the presence of these organisms within the coral that give it its
          colour. Most of this standard type of coral occur within shallow tropical and subtropical waters where the
          water is both warm and able to receive adequate sunlight year-round. Other types of coral that do not have the
          plant symbiotes, however, do exist in deeper waters, or colder seas as far from the equator as Scotland, but
          are rarer.
        </p>
        <h3>Environmental Threats</h3>
        <p>
          Humans have made use of the ecosystems of the shallow seas since before recorded history in the form of
          fishing. But with the rapid growth of global population over the last couple of centuries, such natural
          systems have been put under threat, with over-fishing in Europe, for instance, leading to the introduction of
          legal maximum quotas of fish, in an attempt to restore fish stocks. The difference between fishing and
          agriculture is that the former still relies upon sourcing the food in the wild, as opposed to the
          domestication of plants and the development of intensive farming in the latter. So while farming has been able
          to keep pace with growing population, fishing has not.
        </p>
        <p>
          Another concern is the bleaching of coral reefs, where the corals expel their plant symbiotes, losing their
          colour, and eventually dying. It is still unclear what is causing this bleaching, but suspects include slight
          increases in sea temperatures and marine pollution.
        </p>
        <p>
          The growth of plastics in our daily lives has led to the dramatic rise in marine plastic pollution that can be
          found littering beaches and forming gigantic garbage patches in the middle of ocean gyres. As plastics can
          survive for centuries before breaking down, they can accumulate and poison the food-chain. One thing the
          talking heads in the media never tell you, though, is that this is a problem that is, for once, not coming
          from the developed nations of the West, but rather, that the worst offending countries for this are all in the
          Far East. This is largely due awareness leading to historical efforts within Europe and North America to
          prevent the ingress of plastics into the Atlantic. So while the issue is still very much in the news in the
          West, the message needs to be forwarded to these countries in Asia so that they may also take action.
        </p>

        <h3>Coursework Questions</h3>
        <ol>
          <li>
            How much of the surface of Earth is covered by water? How does this proportion relate to the origin of the
            planet?
          </li>
          <li>What are continental shelfs?</li>
          <li>What is salinity? How saline are the oceans? And how is this important for life evolving within it?</li>
          <li>What is plankton? What are some of its types? And why is it so important?</li>
          <li>What is seaweed? How is it different from kelp?</li>
          <li>What is coral? How does it get its energy?</li>
          <li>What are some of the environmental threats facing the shallow seas?</li>
        </ol>
      </Chapter>
      <Credits>
        <h4>THANKS TO THE FOLLOWING FOR SHARING IN THE CREATIVE COMMONS:</h4>
        <p>
          World Elevation Map:{" "}
          <B to="http://all-geo.org/metageologist/2012/01/a-journey-through-the-geology-of-mountains/">All Geo</B>
        </p>
        <p>
          Coral Reef <B to="https://youtu.be/saZ6lK3aUPI">Gonban Videos</B>
        </p>
        <p>
          Bleached Coral <B to="https://youtu.be/gLctnOKu0zc">JD</B>
        </p>
        <p>
          Jellyfish <B to="https://youtu.be/r0b0vyPPBCo">JD</B>
        </p>
        <p>
          Fishing Boat <B to="https://youtu.be/ooPIakgwlRE">Gabi Comojo</B>
        </p>
        <p>
          Fishing Boat <B to="https://youtu.be/AaNkfE9rvm4">Astonish Tech</B>
        </p>
        <p>
          Seaweed and fish <B to="https://youtu.be/pSQhcmQ1CkE">DigitalAquamarine</B>
        </p>
        <p>
          Kelp Forest <B to="https://youtu.be/p1mh884IyeM">DigitalAquamarine</B>
        </p>
        <p>
          Plastic Pollution <B to="https://youtu.be/eUtfPUPAz2M">Brian Goodall</B>
        </p>
        <p>
          Plankton <B to="https://youtu.be/L5Bn0PIjrcg">Pacific Plankton</B>
        </p>
        <p>
          Sharks and Whales <B to="https://youtu.be/BzrFQm3CSLs">Sailing Zingaro</B>
        </p>
        <p>
          Full Earth <B to="https://youtu.be/UZOOL3kYU9k">Blueturn Earth</B>
        </p>
        <p>
          Late Heavy Bombardment <B to="https://youtu.be/5xUROWDJ1Qk">Earth.Parts</B>
        </p>
        <p>
          Steam{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Steamboat_Geyser_major_eruption_-_steam_phase_(9_36_AM-onward,_4_June_2018)_20_(42282580665).jpg">
            James St. John
          </B>
        </p>
        <p>
          Ice Cap Melt <B to="https://flic.kr/p/aHtxoa">Kevin Gill</B>
        </p>
        <p>
          Seawater Composition{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=5120699">Tcncv / Hannes Grobe / Stefan Majewsky</B>
        </p>
        <p>
          Bacterioplankton <B to="https://commons.wikimedia.org/w/index.php?curid=1428483">Jed Fuhrman</B>
        </p>
        <p>
          Bacterioplankton <B to="http://www.kerfeldlab.org/images.html">Raul Gonzalez and Cheryl Kerfeld</B>
        </p>
        <p>
          Virioplankton <B to="https://commons.wikimedia.org/w/index.php?curid=5035798">Dr Graham Beards</B>
        </p>
        <p>
          Coral Distribution{" "}
          <B to="https://www.icoral.org/wp-content/uploads/2018/10/page-section-3-global-distribution-of-coral-reefs-graphic_cropped.jpg">
            iCoral
          </B>
        </p>
        <p>
          Marine Pollution Gyres{" "}
          <B to="https://sitn.hms.harvard.edu/flash/2018/plastic-oceans-cleanup/">Harvard University</B>
        </p>
        <p>
          Marine Pollution Countries{" "}
          <B to="https://www.statista.com/chart/12211/the-countries-polluting-the-oceans-the-most/">Statista</B>
        </p>
      </Credits>
    </>
  );
}
