import React from "react";
import Chapter from "../components/chapter";
//import ChapterImage from "../components/chapter-image";
import ChapterImageList from "../components/chapter-image-list";

import A from "../components/A";
import Credits from "../components/credits";
import B from "../components/B";

export default function BiomesHighlands(props) {
  const headerImage = "highlands-hero.jpg";

  const ImageHoldridge = "Holdridge-Lifezones.png";
  const ImageAltitudeAndTemp = "biomes-highlands-temperature-and-altitude.jpg";
  const ImagePolar = "biomes-highlands-polar.jpg";
  const ImageSubarctic = "biomes-highlands-subarctic.jpg";
  const ImageTreeLine = "biomes-highlands-tree-line.jpg";
  const ImageTemperate = "biomes-highlands-temperate.jpg";
  const ImageDry = "biomes-highlands-dry.jpg";
  const ImageDryMontaneForest = "biomes-highlands-dry-montane-forest.jpg";
  const ImageTropics = "biomes-highlands-tropics.jpg";
  const ImageTropicalMoorland = "biomes-highlands-tropical-moorland.jpg";
  const ImageBiomesKilimanjaro = "biomes-highlands-kilimanjaro.jpg";

  const imagesIntro = [
    {
      src: ImageAltitudeAndTemp,
      caption: "As altitude rises, temperature falls",
    },
    {
      src: ImageHoldridge,
      caption:
        "The Holdridge Lifezones chart allows us to predict what biome to expect as we rise in altitude with a given level of rainfall",
    },
  ];

  const imagesPolar = [
    {
      src: ImagePolar,
      caption: "Sea-level tundra quickly gives way to glaciers and ice in rising altitude",
    },
    {
      src: ImageSubarctic,
      caption: "Rising in altitude from Boreal Forest gives us Tundra and then Glacial Ice",
    },
  ];

  const imagesTemperate = [
    {
      src: ImageTreeLine,
      caption:
        "The tree line exists in every highland biome below the arctic, where the mountains are high enough to reduce summer temperatures to below 10°C",
    },
    {
      src: ImageTemperate,
      caption:
        "Wet temperate regions having grassland or forest at sea level rise into Montane then Coniferous Forest, with Alpine Meadows and Glaciers above the Tree Line",
    },
    {
      src: ImageDry,
      caption: "Dry climates having scrub or semi-desert at sea level yield dry montane forests on slopes above them",
    },
    {
      src: ImageDryMontaneForest,
      caption: "Dry montane forest found above the scrub and desert of Tenerife in the Canary Islands",
    },
  ];

  const imagesTropics = [
    {
      src: ImageTropics,
      caption:
        "Tropical Forests and Savannah give way to Tropical Montane Forests, then either grassland or unique moorland above the tree line, and glaciers if the mountains are very high",
    },
    {
      src: ImageTropicalMoorland,
      caption:
        "Tropical Moorland is found above the tree line at very high altitudes in the tropics and consists of unusual and unique species adapted to these special conditions. Image courtesy of Milosh Kitchovitch.",
    },
    {
      src: ImageBiomesKilimanjaro,
      caption:
        "Satellite view of Mount Kilimanjaro in East Africa. Note the various colors visible that show the outer low level environment of Savannah, a dark ring of montane forest, then the brown of tropical moorland and finally a semi-desert around the glaciers at the summit",
    },
  ];

  return (
    <>
      <Chapter
        series="biomes"
        seriesChapter="10"
        heading="Highlands"
        subheading="Alpine Moorland, Tundra and Montane Forest"
        documentTitle="Highland Biomes"
        documentDescription="Highlands - how altitude affects the biomes of our planet"
        headerImageSrc={headerImage}
        youTubeCode="UYFZA_aIjbY "
        chapterPrevText="Wetlands"
        chapterPrevUrl="/biomes/wetlands"
        chapterNextText="Shallow Seas"
        chapterNextUrl="/biomes/shallow-seas"
      >
        <p>
          Tropical rainforests, grasslands and deserts, temperate and boreal forests, scrub and tundra. The view of this incredible
          diversity of biomes has been, in this series so far, more or less confined to lowland regions. But what happens when you take
          these biomes, and climb upwards, into thinner, colder air? Plants must adapt to the colder conditions and in rising altitude, the
          biomes shift in an echo of increasing latitude. Set against dramatic mountains and plateaux, plant life leaves its mark creating
          living landscapes which are arguably the most stunning on our planet, in the highlands of Earth.
        </p>
        <h3>Altitude and Biomes</h3>
        <p>
          If you look at a map of the elevation of Earth’s topography, you’ll find a good proportion of the land surface is significantly
          above sea-level. And so in a series on biomes, it would be grave omission to not spend some time looking at how ecosystems have
          evolved to cope with altitude across these diverse regions of our planet.
        </p>
        <p>
          Due to various effects relating to heat transfer, the troposphere, which is the lower portion of the earth’s atmosphere, cools as
          altitude rises. In dry air this effect results in about 10°C of cooling per km (5.4°F/1000ft). As the troposphere extends up to
          about 12km (39,000ft) above sea level, and since the highest point of Mount Everest is some 3km below this point, this cooling
          effect applies to all land surfaces on the planet.
        </p>
        <p>
          How this temperature drop affects plant life that sits upon the higher elevations of earth is determined by the initial
          temperature at sea level, and this of course, is determined by latitude, with the hottest areas in the tropics and in subtropical
          deserts, and the coldest sea level temperatures being at the poles. But regardless of the starting latitude, the result of
          increasing altitude is usually to push the biome poleward in effective latitude. For example, a temperate forest at sea level will
          shift toward taiga in nearby mountains, and again to alpine tundra and eventually to a glacial icecap if the mountains are high
          enough. All at the same latitude.
        </p>
        <ChapterImageList images={imagesIntro} />
        <p>
          Let’s return to our Holdridge Lifezones chart. At last we can look at the right hand side of the diagram, the altitudinal belts
          that closely mimic the latitudinal regions. So we find a general relationship between increasing altitude and latitude in terms of
          effective biome. A second concept to notice is that the type of altitude biome we will get will be determined by rainfall. As we
          increase in altitude, and the temperature cools, for the same amount of rain, there will be less evapotranspiration, allowing a
          higher biomass to accumulate. This explains why you often find forests upon the slopes of hills and mountains in drier regions,
          while the plains below are grassland or scrub. The chart doesn’t describe what type of forests we’ll find, however, and I’ll
          discuss that detail a bit later. As always, when it comes to biomes, simple changes in atmospheric effects and climate can lead to
          complex changes in ecosystems.
        </p>
        <h3>Polar Regions</h3>
        <p>
          In order to break this down, let’s take a journey from the poles of our planet to the equator, and examine the effects of altitude
          in each zone of latitude. Starting at the poles we have, at sea-level, the ice biome. The north and south poles are covered in
          ice, but while the ice at the north pole, sitting on the surface of the Arctic Ocean, is close to sea level, the ice across most
          of Antarctica is at several kilometres in altitude, due to the accumulation of snow over thousands of years into the great ice
          sheets that cover this continent. And this gives us our answer as to what happens to the ice biome with increasing altitude. No
          surprises – it’s just more ice. Well, actually, a bit less, as there is less snowfall in these much colder areas than can be found
          at sea-level. But this doesn’t affect the overall picture of a lifeless world in these areas.
        </p>
        <p>
          Moving into sea-level tundra areas, and these are most widely found in the arctic. If we increase altitude here we quickly reach
          freezing temperatures where no plant life is possible, and accumulations of snow produce glaciation as portions of the ice biome.
          The most marked example of this is in Greenland, which lies at a similar latitude to arctic tundra, evidenced by the fact that the
          coasts are all in that biome. But the world’s largest island is mostly covered in ice, due to the accumulation of millenia of snow
          upon its ice-sheet. This is likely a hangover of a past ice age, with temperatures upon the top of the sheet remaining below
          freezing and preventing melting.
        </p>
        <ChapterImageList images={imagesPolar} />
        <h3>Subarctic</h3>
        <p>
          As we move into the sea-level boreal forest, or taiga, we find that this vast dark green ocean of conifers is peppered with higher
          altitude sections that are turned to tundra. Here is the first time we see the tree-line, a horizontal line that can be seen on
          mountains where trees give way to low-lying vegetation such as grasses, dwarf shrubs, heather, sedges, moss and lichen. Trees
          cannot grow if summer temperatures do not rise above 10°C, and so this tree-line is where we find this subdivision of average
          summer temperature. Rising beyond the alpine tundra, we again reach glaciers and the ice biome if the mountains are high enough.
        </p>
        <h3>Temperate Latitudes</h3>
        <p>
          Continuing toward the equator, we reach the temperate and subtropical zones where things become a bit more complex, because at
          these latitudes we can find temperate forest, grassland, scrub or desert, depending upon prevailing levels of rainfall and other
          factors. Our Holdridge Lifezones Chart navigates us through this complexity. Simplest is the case of temperate forests. As we
          increase in altitude here, broadleaf forests of beech, oak and maple give way to hardier broadleafs such as birch and aspen, but
          more commonly conifers such as spruce, fir, pine and larch. Eventually, only conifers remain in a form of alpine taiga, which
          eventually gives way to alpine tundra and ice if high enough. The treeline here is often lower in altitude than the 10°C summer
          temperature average earlier mentioned, however, as high-winds at these higher elevations often prevent the development of trees.
          An example of this are the heather-covered uplands of Great Britain that were deforested millennia ago, and have never recovered
          despite being unused for agriculture, due to constant high winds that blow across these regions for most of the year. Trees on the
          border of alpine tree line zones tend to have twisted trunks and stunted growth when exposed to constant freezing winds and this
          type of landscape is called krummholz.
        </p>
        <ChapterImageList images={imagesTemperate} />
        <p>
          In the case of grassland and scrub at sea-level, as earlier mention, slopes of hills and mountains in these regions tend to be
          tree-covered as evapotranspiration falls with reducing temperature. These “montane” forests are composed of mixed broadleaf and
          conifer species such as ponderosa pine, juniper and evergreen oak. Above the treeline, these forests yield once again to a form of
          alpine tundra, although this is usually very sparse due to the comparatively low precipitation. And if high enough, glaciers will
          be found on mountain tops. The largest of the temperate latitude alpine tundra areas are the vast and barren steppes of the
          Tibetan plateau, with this region having an average altitude of 4,500m (14,800ft).
        </p>
        <p>
          Sea-level deserts offer the only departure from the general pattern of alpine slopes having forests. Semi-desert, like grassland
          and scrub, gives way to forest at higher elevations, but particularly dry deserts, such as those of the Atacama in Chile, are so
          devoid of moisture, that no forests occur, or even tundra for that matter, with just a thin dusting of snow at the highest peaks
          of these areas which are accumulations of occasional precipitation that has remained frozen for centuries or even millennia.
        </p>
        <h3>The Tropics</h3>
        <p>
          But it’s upon our arrival in the tropics where the most interesting effects of altitude upon biomes occur. The fundamental reason
          for this is the lack of seasonal temperature changes. In temperate regions, changes in seasonal temperature yield frosts in the
          mountains as well as at sea-level, and so we tend to find similar types of trees and shrubs in both. But in the tropics, one can
          rise in altitude into a temperate climate that, unlike climates of the temperate latitudes has little seasonal temperature change,
          and importantly, no frost, which dramatically affects the type of plants that can grow here. Such is the uniqueness of this
          climate zone, that I devoted a whole <A to="../climate/subtropical-highlands">article</A> to it in my Secrets of World Climate
          series, and briefly spoke of the unique plant life that can be found in such regions.
        </p>
        <p>
          In the tropics, we find, at sea-level, tropical rainforest, tropical seasonal (or “dry”) forest, or savannah, depending upon the
          amount and annual distribution of rainfall. At higher elevations in all of these cases, we’ll find some kind of specialised forest
          composed of various broadleaf tree species as well as palms. In the case of savannah, less evapotranspiration due to cooler
          temperatures increases the covering of trees, while the tropical forests morph in terms of the type of species present. What
          species occur depends upon the continent, but like their sea-level companions, fall into three broad groups of Neotropical in
          Central and South America, African and Malesian in South-East Asia.
        </p>
        <ChapterImageList images={imagesTropics} />
        <p>
          Tropical Rainforest often gives way to Cloud Forests such as those found in Ecuador and Costa Rica, which, in addition to regular
          rainfall, receive water in the form of fog drip. The broadleaf trees in these forests are usually not as tall as their sea-level
          counterparts, but often have thicker and more gnarled trunks. They are also referred to as Mossy Forests owing to the moss that
          covers practically every surface owing to the saturated water passing through the forest in the form of fog. In nearby Colombia,
          we find the world’s tallest palms – the Wax Palms of Cocora that rise up to 60m in height. In fact, this country, due to its large
          areas of subtropical highland climate, is believed to have more species of palms than any other.
        </p>
        <p>
          Above the treeline, however, things become even more unusual as we enter a subtropical grassland, moorland or heathland, depending
          upon region. For in these tropical regions, the altitude where the temperature drops below 10°C that prevents tree growth, is very
          high. This results in two effects – the first being much thinner air than in comparable alpine tundra, the second being very high
          day-night temperature variation. And, as mentioned, there are no seasons in regard to temperature. So plants found here are
          uniquely adapted and frankly don’t look like plants found anywhere else in the world. An example of these almost alien-like
          landscapes can be found on the slopes of Mount Kenya in East Africa. A more “normal” form is the Puna Grassland that is found in
          the most extensive of these subtropical highland areas which occur on the vast raised Andean Altiplano of Peru and Bolivia as well
          as adjoining highland areas in Ecuador, Argentina and Chile.
        </p>
        <p>
          In conclusion of this episode and of all the land biomes covered in this series, it is perhaps fitting to look at Africa’s
          mightiest, and most famous peak, the giant Mount Kilimanjaro. This dormant volcano rises to almost 6km (actually 5,895 metres,
          19,341 ft) above sea level and almost 5km (4,900 metres,16,100 ft) above its base on the East African plateau. In those five
          vertical kilometres, we go from savannah grassland (which is now largely farmed), through tropical then temperate montane forests,
          moorland like that just described on Mount Kenya, and then an almost desert-like tundra zone, before reaching the glaciers on its
          summit. Very few places in the world allow someone to experience almost the full gamut of our planet’s biomes in a short trip.
          Kilimanjaro, then, really is like our planet in miniature - diverse, fascinating, awe-inspiring and majestic.
        </p>

        <h3>Coursework Questions</h3>
        <ol>
          <li>How is the general rule for how altitude affects biomes related to latitude?</li>
          <li>Why are forests present on highland slopes above almost every sea-level biome?</li>
          <li>What is the Tree Line? Why does it exist?</li>
          <li>Describe some differences between Highlands in the Tropics and in Temperate Latitudes.</li>
        </ol>
      </Chapter>
      <Credits>
        <h4>THANKS TO THE FOLLOWING FOR PERMISSION TO USE FOOTAGE:</h4>
        <p>Rwenzori/Mountains of the Moon, Uganda: <B to="https://youtu.be/zNDoMBDDyAE">Milosh Kitchovitch</B></p>
        <p>Mount Kenya: <B to="https://youtu.be/jAwL0XtbT1w">Milosh Kitchovitch</B></p>
        <p>Atacama: <B to="https://youtu.be/6hn-pu3TqNQ">Milosh Kitchovitch</B></p>        
        <p>Kilimanjaro: <B to="https://youtu.be/4u6-lwQrnEs">Ingus Kruklitis</B></p>
        <p>Bolivia/Peru/Colombia: <B to="https://www.youtube.com/channel/UCRzFzMrebCld4Ujroac7GRw">Tout Par Drone (Thomas Noisel)</B></p>
        <p></p>
        <h4>THANKS TO THE FOLLOWING FOR SHARING IN THE CREATIVE COMMONS:</h4>
        <p>
          World Elevation Map: <B to="http://all-geo.org/metageologist/2012/01/a-journey-through-the-geology-of-mountains/">All Geo</B>
        </p>
        <p>
          Scottish Highlands: <B to="https://youtu.be/tEmTSvDPVDI">adventures.of.ducduc</B>
        </p>
        <p>
          Tibet steppe: <B to="https://youtu.be/P_u9_5jIeEI">NaturelsHere</B>
        </p>
        <p>
          Kilimanjaro lower forests: <B to="https://youtu.be/348gK3RDC60">Orbea</B>
        </p>        
        <p>
          Krummholz: <B to="https://flic.kr/p/8R4x6z">Jesse Varner</B>
        </p>
        <p>
          Krummholz: <B to="https://flic.kr/p/sRVgW">brewbooks</B>
        </p>
        <p>
          Krummholz: <B to="https://flic.kr/p/4vCgmq">John Spooner</B>
        </p>
        <p>
          Krummholz: <B to="https://flic.kr/p/ahuwTB">Jim Kravitz</B>
        </p>
        <p></p>
      </Credits>
    </>
  );
}
