import React from "react";
import Chapter from "../components/chapter";
//import ChapterImage from "../components/chapter-image";
import ChapterImageList from "../components/chapter-image-list";

//import A from "../components/A";
import Credits from "../components/credits";
import B from "../components/B";

export default function BiomesWetlands(props) {
  const headerImage = "wetlands-hero.jpg";
  
  const ImageMangroveRoots = "wetlands-mangrove roots - Peripitus.jpg";
  const ImageWorldMangroves = "wetlands-World_map_mangrove_distribution.jpg";
  const ImageSundarbans = "wetlands-sundarbans.jpg";

  const ImageCamargue = "wetlands-camargue.jpg";
  const ImageMissDelta = "wetlands-mississippi-delta.jpg";
  
  const ImageEverglades = "wetlands-everglades.jpg";
  const ImagePantanal = "wetlands-pantanal-wet-season - Giovanna Colombi.jpg";
  const ImageMissIowa = "wetlands-mississippi-iowa.jpg";
  const ImagePinsk = "wetlands-pinsk-marshes.jpg";  
  const ImageNileFlood = "wetlands-nile-flooding.jpg";
  const ImageForestedSwamp = "wetlands-forested-swamp.jpg";

  const ImagePeatmap = "wetlands-PEATMAP.jpg";
  const ImageFen = "wetlands-fen.jpg";
  const ImageBog = "wetlands-hero.jpg";
  const ImageHudson = "wetlands-hudson-bay-lowlands.jpg";
  
  

  const imagesMangrove = [
    {
      src: ImageMangroveRoots,
      caption: "Some species of Mangrove trees have unusual roots that poke up out of the mud at high tide. They allow the plant to pull in oxygen. Photo courtesy of Periptus.",
    },    
    { src: ImageSundarbans, caption: "The Sundarbans Mangrove Forest of the Ganges Delta - India/Bangladesh border." },
    { src: ImageWorldMangroves, caption: "Global Distribution of Mangrove Forest. Image courtesy of ChandraGiri." },
  ];

  const imagesSaltMarsh = [
    {
      src: ImageCamargue,
      caption: "The Camargue Salt Marsh of the Rhone Delta, Southern France.",
    },
    { src: ImageMissDelta, caption: "The final part of the Mississippi Delta, Louisiana, USA." },    
  ];

  const imagesFreshMarsh = [    
    { src: ImagePinsk, caption: "An artist's depiction of the Pinsk Marshes of Belarus/Ukraine in the 19th Century." },        
    { src: ImageForestedSwamp, caption: "Bald Cypress trees in a Forested Swamp (or 'Bayou') of Louisiana, USA." },        
    {
      src: ImageEverglades,
      caption: "The Everglades Freshwater Marsh of Southern Florida - perhaps the most well-know of all the world's wetlands.",
    },
  ];

  const imagesRiparian = [
    { src: ImagePantanal, caption: "The Pantanal Riparian Wetland of Bolivia/Brazil/Paraguay, in the wet season. Photo courtesy of Giovanna Colombi." },    
    { src: ImageMissIowa, caption: "The Mississippi River in Iowa flooding its banks. This action is typical of Riparian systems." },        
    { src: ImageNileFlood, caption: "Before the building of the Aswan High Dam in the 1960s, the Nile River would flood Egypt annually in the wet season of East Africa." },        
  ];

  const imagesBogs = [    
    { src: ImageFen, caption: "The Pool at Hartlebury Common, Worcestershire, England - an example of a Fen." },        
    { src: ImageBog, caption: "Hartlebury Common Bog, Worcestershire, England. It has taken thousands of years for this bog to evolve into a 'raised bog' of peat forming above the water line." },    
    {
      src: ImagePeatmap,
      caption: "An estimation of the world's peatlands based on modelling done by researchers at Leeds University, England.",
    },
    { src: ImageHudson, caption: "The Hudson Bay Lowland - the largest wetland in North America." },            
  ];

  return (
    <>
      <Chapter
        series="biomes"
        seriesChapter="9"
        heading="Wetlands"
        subheading="BOGS, FENS, MANGROVES AND MORE!"
        documentTitle="Wetland Biomes"
        documentDescription="Wetlands. The sub-biomes of marsh, bog, fen and mangrove. Description and dynamics, locations and species. Text, video, photos, maps and coursework questions"
        headerImageSrc={headerImage}
        youTubeCode="4bRgUShrC1w "
        chapterPrevText="Tundra and Ice Biomes"
        chapterPrevUrl="/biomes/polar-biomes"
        chapterNextText="Highlands"
        chapterNextUrl="/biomes/highlands"
      >
        <p>
          There are places in the world where water slows down, spreads out and covers the land in shallows. In these calm, undisturbed
          conditions, plants have evolved in many special ways to thrive. From saltwater mangroves along the tropical coasts, to the bogs
          and peatlands of the subarctic, these flooded lands accompany every biome and climate zone on earth. Their ecosystems are among
          the richest and most complex to be found anywhere. I hope you brought your waders. You’ll need them, as we enter the world’s
          wetlands.
        </p>
        <p>
          Wetlands are regions of the planet that have sluggish or standing water at relatively shallow depths, that allow the growth of
          plant material. They can occur in fresh water, such as river floodplains, saltwater, such as in mangroves, or in interfaces
          between the two, such as in river deltas. They occur in practically every parent biome on Earth, from the equator to the polar
          regions, and unlike the biomes we have covered so far in this series, are much more decoupled from climate, with the presence of
          shallow, undisturbed water being the dominant influence. They are highly biodiverse areas in terms of both plant and animal
          species, and for this and other reasons to be revealed, are of global importance.
        </p>
        <p>
          The interface of water above land creates a very complex picture in terms of plant and soil systems, and so this work should only
          be seen as a summary guide to provide a beginner’s overview to the subject, and through imagery, provide some reality to it. Such
          complexity reveals itself immediately with the myriad of terms used to describe such places. In the English language alone there
          are almost a dozen – swamp, mire, moor, marsh, muskeg, mangrove, flat, fen and bog. Some of these overlap, and most are historical
          terms that modern ecologists have attempted to corral into tighter technical definitions.
        </p>
        <p>In general wetlands vary depending upon the salinity and mineral content of the water, as well as overall temperature ranges.</p>
        <h3>Mangroves and Salt Marshes</h3>
        <p>
          Starting with the most salinity, we have mangroves and salt marshes both of which occur in coastal areas where the salinity is
          more or less that of seawater. Both occur in areas protected from wave action that would otherwise disturb the settlement and
          germination of plant seeds. Mangroves are found within the tropics and some subtropical areas provided no frosts occur. They are
          characterised by a thick covering of salt-tolerant tree and shrub species, and some species have roots with stick up out of the
          mud like spikes, so that the plant can breathe at low tide. Examples of extensive mangrove forests are the Sundarbans of the
          Ganges Delta in India and Bangladesh, the Niger delta in West Africa and the Orinoco river delta in Venezuela.
        </p>
        <ChapterImageList images={imagesMangrove} />
        <p>
          Salt marshes are found in sheltered coastal areas outside of the tropics that are free of ice, namely, temperate regions where the
          area is regularly flooded with seawater at high tide. They are formed from salt-tolerant herbs, grasses and sometimes low shrubs
          that are able to bind sediments that flow into the area, and so stabilise the marsh, allowing it to grow. As sediments play a key
          role, they are typically found in river estuaries, but may also occur on the leeward side of barrier islands and spits of land.
          Examples of such marshes are the Camargue at the estuary of the Rhone River in Southern France, the Mississippi delta and The Wash
          of Eastern England.
        </p>        
        <ChapterImageList images={imagesSaltMarsh} />
        <h3>Tidal Freshwater Marshes and Intertidal Flats</h3>
        <p>
          In rivers upstream from salt marshes are found tidal freshwater marshes, where tides further downstream result in the freshwater
          of the river backing up and flooding, and so do not directly see salt water. These are commonly found in large temperate estuaries
          that experience significant tides, such as the St Lawrence of Eastern Canada, and the Severn of Great Britain.
        </p>
        <p>
          The last category of coastal wetlands that experience seawater or tidal influences are the Intertidal flats – these are flat areas
          subjected to tides that are too unstable in terms of sediments and so remain unvegetated. Morecambe Bay, in the North-West of
          England is an example of this.
        </p>
        <p>
          Inland freshwater wetlands come in many forms, and differ in terms of the source of water, whether they produce peat, whether they
          are forested or not, and whether they are flooded seasonally or year-round.
        </p>
        <h3>Freshwater Marshes</h3>
        <p>
          Freshwater marshes are large non-forested flat areas that are either continuously or frequently flooded, but which have water
          flowing through them. Because this water circulates, acidity does not build up in the decomposing dead plant material, and so does
          not form peat – which I’ll touch on more in a moment. The primary vegetation here is grasses and sedges, along with emergent
          plants – those with soft stems that are highly adapted to saturated conditions. Reeds, also known as bulrushes or cattails are a
          common sight here too. The largest freshwater marsh is arguably the world’s most famous wetland – the Everglades of Southern
          Florida. Another notable wetland of this type occurs at the confluence of the Tigris and Euphrates rivers in Southern Iraq. The
          Mesopotamian Marshlands have been home to people for thousands of years, but in the 1980s and 90s, the vast majority of these were
          drained for political and economic reasons, and today are only 10% of their former size.
        </p>
        <h3>Freshwater Forested Swamps</h3>
        <p>
          Freshwater forested swamps have similar water dynamics to the marshes just covered, in that they are large flat areas that are
          either permanently or frequently flooded with fresh water. The difference is that the primary vegetation is trees instead of
          grasses and sedges. They occur in tropical and temperate regions. The largest of these is one of the largest wetlands in the
          world, and that is the central part of the Amazon river basin that floods over an enormous area for months during the wet season.
          This flooding accounts for the wider distribution of evergreen forest compared to the smaller area of tropical rainforest climate
          – areas that have a notable dry season, with either Tropical Monsoon or Savannah climates, still have evergreen rainforest instead
          of the seasonal forest that sees the loss of leaves in the dry season. This is because the flooding lasts well into the dry season
          and provides sufficient water for the trees to continue growing during this time. Equally as notable are the numerous and iconic
          flooded forests of the Eastern United States, which extend from Maine, down the Eastern Seaboard, and along the Gulf States to
          Texas. They are the product of flat plains combined with plenty of rain from the Humid Subtropical climate. In the north, Red
          Maples dominate, while in the south Atlantic White Cedars make up much of the shallower swamps. Deeper swamps in the south see
          Cypress trees dominant.
        </p>
        <p>
          The largest wetland in Europe is part forested swamp, and part freshwater marsh. Known as the Pripyat or Pinsk Marshes, it
          straddles the border of Belarus and Ukraine. Like most swamps, it is relatively impenetrable, and was historically significant
          both World Wars in slowing and dividing invading forces into northern and southern groups.
        </p>
        <ChapterImageList images={imagesFreshMarsh} />
        <h3>Riparian Wetlands</h3>
        <p>
          Related to the freshwater marshes and forested swamps are the Riparian wetlands. Riparian simply means “of a river bank”, and
          these wetland types centre around a river which will regularly burst its banks for part of the year. It can be argued that the
          Amazon wetlands are of this type – there is a degree of overlap in terms of classification. A clear example of a river dominated
          wetland, however is that of Mississippi. Or rather, was, for almost all of the riparian wetlands that extended as far as 50 miles
          (80 km) on each side of the river for up to 500 miles along its lower length have disappeared over the last 200 years. The reasons
          are obvious. Such a long navigable river, reaching from the sea far into the continental north was always going to be an economic
          power line. And so the towns and cities that grew up on its banks needed protection from the regular damaging floods. Levees, or
          protective embankments, sprang along its length, keeping the water in, and eventually leading to the draining of the wetlands,
          whose fertile soils were converted to farmland. That vast area can still be seen outlined today.
        </p>
        <p>
          The most notable wetland of this type that still survives is the vast seasonally flooded basin at the tripoint of Brazil, Bolivia
          and Paraguay known as the Pantanal. Chances are you’ve seen this in wildlife documentaries, and for good reason, as it has one of
          the richest ecosystems in the world. It is regarded as the largest tropical wetland area, and is the largest flooded grassland in
          the world. And then there is the Sudd of South Sudan, a vast flooded grassland and inland delta into which the Nile River, flowing
          north, slows to a crawl, spreading out across thousands of square miles. This area is historically significant as despite their
          might and sophistication neither Ancient Egypt nor the Roman Empire were able to penetrate its endless channels in the search for
          the source of the Nile.
        </p>
        <p>
          While we’re on that river, no discussion of global wetlands would be complete without a mention of the lower reaches and end of
          that mighty watercourse – the Nile Delta. Since records began, that vast area has been irrigated and cultivated to feed the large
          population of Egypt, so we have no way of knowing for sure what the original form of that wetland delta once was. Every year, the
          deluge from the rainy season in the tropical savannah of the East African highlands would flow into the Nile, find its way through
          the Sudd and continue north into Egypt, where it burst the river’s banks. The water, spilling out onto the floodplain south of the
          delta as well as within it, would provide mineral rich silt that fertilised the land and allowing for bountiful harvests year
          after year. This is perhaps the most notable example of a riparian system influencing an entire civilisation. The yearly floods
          ended with the construction of the Aswan Dam in the 1960s, and so, like the Mississippi, this river was tamed by humans, and the
          last vestige of natural wetlands came to an end.{" "}
        </p>
        <ChapterImageList images={imagesRiparian} />
        <h3>Bogs and Fens</h3>
        <p>
          As we come to the last two types of wetland, bogs and fens, it’s time to talk about peat. Many wetland areas have the right
          conditions for the formation of this acidic concentrate of dead plant material, which is, in fact, the first stage in the
          formation of coal. It is formed when dead plant material, most of which is moss, falls to the bottom and becomes compressed as
          further layers accumulate on top. As the water obstructs the flow of oxygen and as acids form to produce an almost vinegary level
          of pH, normal bacterial processes are inhibited and so decomposition is very slow. As a result most of the carbon in the original
          plant is preserved instead of being lost as CO2, as occurs in more regular rates of decomposition. Consequently, peatlands act as
          significant carbon sinks, and it is believed that as much as 30% of all carbon in the biosphere is trapped in peat. So they play
          an important part in the overall carbon cycle, and if these are disrupted, such as in the case of drought, large amounts of CO2
          can be released. This occurred recently in Indonesia, when large areas of peatland there caught fire during a drought, or were
          deliberately ignited by farmers, to make way for farmland. That episode resulted in the release of CO2 equivalent to the entire
          output of the USA in one year.
        </p>
        <p>
          Bogs and fens are both peat-producing, with bogs being the most productive in this regard. They differ from each other in that
          bogs get their water directly from rain or snowmelt, and so the water has low mineral content, which means little neutralisation
          of acids in decomposition. Fens, by contrast get their water additionally from streams or groundwater, and so the water has higher
          mineral content and a greater ability to neutralise acids, leading to slower peat formation.
        </p>
        <h3>Peatlands</h3>
        <p>
          Peatlands can occur anywhere in the world where there is decaying plant matter that is left in stagnant water. The largest
          peatland of the tropics is believed to be the Cuvette Central of Congo, found on either side of the vast Congo river as it arcs
          southwest toward the capital of Kinshasa. It is one of the least researched and poorly understood of all the world’s wetlands.
        </p>
        <p>
          In the temperate regions, one of the most well-known peatland areas is that of the western part of Ireland, where heavy rain
          combined with post-glacial depressions and forest clearance millennia ago have led to the production of extensive bogs that make
          up about one-sixth of the whole island. In addition to typical bogs in lower elevations, the forest clearances in the uplands led
          to the colonisation of these areas with heather, turning the soil more acid, resulting in slower plant decomposition and the
          accumulation of peat. With Ireland being the most deforested country in Europe, firewood was scarce and so such peat was a vital
          source of fuel for home heating for centuries.
        </p>
        <p>
          But the largest stretches of peatland occur in the subarctic, namely Alaska, Canada, Northern Scandinavia and Siberia. The
          subarctic has so many of these wetlands for a number of reasons. Being glaciated in previous ice ages, the land is relatively
          flat, and till, that is rocky material carried by the ice, would be deposited unevenly, forming depressions that could then gather
          water after the ice age. In the cool subarctic summers, low evaporation rates means any ice melt or rain stays around longer. And
          lastly, poor drainage brought on by permafrost, a layer of soil that is frozen either year round or for part of the year, acts as
          a barrier to drainage, and so water builds up above and often flooding the surface. The largest wetland in North America is the
          Hudson Bay Lowland, a mix of peat bogs, fens and saltwater marshes fringing the whole southern edge of Hudson Bay. The largest
          wetland region of any kind in the world is probably the West Siberian Lowland, that encompasses an area of about half the size of
          the United States. It runs east from the Ural Mountains over a vast and extremely flat plain to the Yenisey River about 1,500km to
          the East, and from the Arctic Ocean to about 1,200km south. The area is covered in an endless patchwork of bogs and fens
          interspersed with soggy boreal forest, or taiga, and outside of the winter, was essentially impenetrable until the mid 20th
          Century.
        </p>
        <ChapterImageList images={imagesBogs} />
        <p>
          One last region of wetlands worth mentioning is the Prairie Pothole Region that runs from Alberta down to Iowa. As its name
          suggest, this region of the North American Prairie is covered in thousands of pothole depressions formed by uneven deposition of
          glacial till during the last ice age. These potholes fill with water in the spring, and many evaporate by summer, but some are
          permanent. As they don’t quite fit the description of any of the above types, this final point goes to demonstrate that wetland
          habitats are as complex and varied as earth is in terms of topography, hydrology and biology.
        </p>
        <h3>Biome Threats</h3>
        <p>
          Once viewed as wastelands that could be drained to produce valuable fertile farmland, wetlands outside of the arctic have shrunk
          dramatically over the last few centuries, and those still surviving have often been affected by runoff of fertiliser and
          pollutants which, in a slow-moving or stagnant water system can disrupt the ecosystem significantly. More recently this trend has
          continued in the tropics. But in developed nations, public awareness of the importance of their rich ecosystems has resulted in a
          shift of attitude over the last 50 years, with the result that most are now protected from drainage.
        </p>
        <p>
          Between 10-20% of all bird species globally are dependent upon wetlands, making them the most dependent of all the animal
          kingdoms. By comparison, only around 2% of mammals are wetland dependent. And so reduction in wetland areas puts a stress in
          particular on bird populations.
        </p>
        <p>
          The Ramsar Convention, originally signed in 1971, but which meets every three years, has created a list of over 2000 wetlands
          globally that are of importance and signatory countries work to ensure these remain protected.
        </p>
        <p>
          Lastly, as mentioned, many wetlands act as carbon sinks through the capture of CO2 into plant matter which, when dead, is locked
          into the peat. If these peatlands are disturbed, then this carbon is released once more into the atmosphere, and may contribute
          further to climate change.
        </p>
        <p>
          It is easy to forgive humans for wanting to turn these unproductive areas into land that can work for them by draining them to
          produce farmland or for urban development. But like any species that ultimately wants to survive, its important that we find the
          right balance between nature and development. Let us hope that our realisation of the importance of these wetland areas has not
          come too late. The world would be a poorer place without them.
        </p>

        <h3>Coursework Questions</h3>
        <ol>
          <li>What is needed to form a wetland?</li>
          <li>List out the basic types of wetlands.</li>
          <li>Briefly describe the two types of saltwater wetlands and how they differ.</li>
          <li>Which wetlands types form peat and which do not? Why is this?</li>
          <li>Briefly describe the different types of inland freshwater wetlands that do not form peat</li>
          <li>What are the differences between bogs and fens?</li>
          <li>Briefly outline why wetland areas are of global importance.</li>
        </ol>
      </Chapter>
      <Credits>
        <h4>THANKS TO THE FOLLOWING FOR SHARING IN THE CREATIVE COMMONS:</h4>
        <p>Mangrove Brazil: <B to="https://youtu.be/y0tosWhDfXQ">O Agro Nosso</B></p>
        <p>Mangrove global distribution: <B to="https://commons.wikimedia.org/wiki/File:World_map_mangrove_distribution.jpg">ChandraGiri</B></p>
        <p>Mangrove roots: <B to="https://commons.wikimedia.org/wiki/File:Pneumatophore_overkill_-_grey_mangrove.JPG</B></p>"> Peripitus</B></p>
        <p>Camargue: <B to="https://youtu.be/q6TbHNNPe7U">Cemil Choses A Te Dire</B></p>
        <p>Severn Estuary: <B to="https://youtu.be/ask4AN7bRaE">David Moore</B></p>
        <p>Severn Estuary: <B to="https://youtu.be/hXknBnBQY2Y">David Moore </B></p>
        <p>Morecambe Bay: <B to="https://youtu.be/HatY6zS8CB0">Simon Greaves </B></p>
        <p>Mesopotamian Marshes: <B to="https://youtu.be/Vt6kgboIyoY">BBC Uzbek</B></p>
        <p>Amazon Basin: <B to="https://youtu.be/_8nMefbx6rM">Global Anchor Cruises</B></p>
        <p>Pripyat: <B to="https://worddisk.com/wiki/File:Prypiat.jpg/">Robert Niedźwiedzki</B></p>
        <p>
          Pripyat WW2: <B to="https://worddisk.com/wiki/File:Bundesarchiv_Bild_183-J16767,_Russland,_Verwundetentransport_mit_Fieseler_Storch.jpg/
         ">Federal Government Commissioner for Culture and Media{" "}
        </B></p>
        <p>Pantanal: <B to="https://youtu.be/JFpragDkxos">toymituable</B></p>
        <p>
          pantanal wet season: <B to="https://pt.wikipedia.org/wiki/Ficheiro:River_in_the_Parque_Nacional_do_Pantanal_Matogrossense_-_Por_dentro_do_pantanal.jpg">Giovanna Colombi
        </B></p>
        <p>Nile Flooding: <B to="https://www.flickr.com/photos/verylastexcitingmoment/8340664541">bunky's pickle</B></p>
        <p>Nile Flooding: <B to="https://flic.kr/p/25rHoXW">Raw Pixel Limited</B></p>
        <p>Peat Stack: <B to="https://commons.wikimedia.org/wiki/File:Peat-Stack_in_Ness,_Outer_Hebrides,_Scotland.jpg">Maclomhair</B></p>
        <p>Peat Cutting: <B to="https://flic.kr/p/nSEdL4">NZ Willowherb</B></p>
        <p>Peat Formation: <B to="https://flic.kr/p/HvNkBv">Grid-Arendal</B></p>
        <p>Irish Upland: <B to="https://jimzglebeblog.blogspot.com/2011/08/my-round-trip-august-2011-part-1.html">The Glebe Blog</B></p>
        <p>Connemara Landscape: <B to="https://commons.wikimedia.org/wiki/File:Aussicht_Diamond_Hill_Connemara.jpg">oiram</B></p>
        <p>Ireland Cottage: <B to="https://wynnanne.blogspot.com/2013/05/omagh-belleek-ballina.html">WynnAnne</B></p>
        <p>Alaska Tundra Landsape: <B to="https://en.wikipedia.org/wiki/File:Alaska_-tundra_landscape-11Aug2008.jpg">Andrea Pokrzywinski</B></p>
        <p>Permafrost: <B to="https://opentextbc.ca/geology/chapter/19-1-what-makes-the-climate-change/">Steven Earle</B></p>
        <p>Vasyugan Swamp: <B to="https://commons.wikimedia.org/wiki/File:Vasyugan.jpg">Vadim tLS Andrianov </B></p>
        <p>West Siberia Lowland: <B to="https://commons.wikimedia.org/wiki/File:Yuganskiy_nature_reserve_aerial_view.jpg">Tatiana Bulyonkova</B></p>
        <p>Prairie Pothole Region Map: <B to="https://commons.wikimedia.org/wiki/File:Prairie_Pothole_Map.svg">Cephas</B></p>
        <p>Prairie Pothole Region: <B to="https://youtu.be/gwaDogC845U">Rick Searle</B></p>
        <p>East Anglia Fenland Loss Map: <B to="https://commons.wikimedia.org/w/index.php?curid=20800088">Amitchell125</B></p>

      <h4>The following reproduced under Fair Use:</h4>
      <p>Peatlands Map: ©2017 <B to="http://archive.researchdata.leeds.ac.uk/251/">Leeds University Library</B></p>
      </Credits>
    </>
  );
}
