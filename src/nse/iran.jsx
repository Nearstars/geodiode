import React from "react";
import Chapter from "../components/chapter";
import B from "../components/B";
import Credits from "../components/credits";

export default function NseIran(props) {
  const headerImage = "iran-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="3"
        heading="Iran"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Iran - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="aNxj5Id_UWk"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <h4>EXTRA SPECIAL THANKS to the following channels, whose Creative Commons content made this video possible:</h4>
        <p>[1] <B to="https://bit.ly/2TOSPNw">AsuhsWorld</B></p>
        <p>[2] <B to="https://bit.ly/2HRzkBL">Fars Travel Company</B></p>
        <p>[3] <B to="https://bit.ly/3oRDCcF">Benyamin Balouchi</B></p>
        <p>[4] <B to="https://bit.ly/3mOypR6">Oxlaey in Iran</B></p>
        <p>[7] This is Tehran – <B to="https://youtu.be/CR0O6fg6AVQ">Mikko Kangas</B></p>
        <h4>Sections of the following reproduced under Fair Use:</h4>
        <p></p>
        <p>[31] Early Oil Industry - <B to="https://bit.ly/387KyMQ">IRNA</B></p>
        <p>[41] <B to="https://bit.ly/3887g7S">Koppen Climate Map</B></p>
        <p>[48] <B to="https://oec.world/en/">Observatory of Economic Complexity</B></p>
        <p>[52] Stealth Fighter - Mehr News Agency</p>
        <p>[53] Cato Institute Iran Page</p>
        <p>Thumbnail credits: مانفی (Mosque Photo) and Sodacan (Lion and Sun emblem)</p>
        <p>&nbsp;</p>
        <p>
          Regrettably, the records for the remaining Creative Commons credits were lost during a transfer of channel status. If you feel
          your property has been used in this video and not been given credit, then please contact me via my <B to="https://www.youtube.com/channel/UC1raaXFgsFBSFR8qNgchF2g/about">Channel "About" page</B>,
          referencing the number in the bottom left of the screen.
        </p>
      </Credits>
    </>
  );
}
