import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseRussia(props) {
  const headerImage = "russia-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="6"
        heading="Russia"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Russia - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="wT8UlOlY3K8"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>
          Special Thanks to the following organisations and individuals for sharing their content through Creative
          Commons, and without which this presentation would not have been possible
        </h4>
        <p>[1] Office of the President (www.kremlin.ru)</p>
        <p>[2] Roskosmos, The Russian Space Agency</p>
        <p>[4] Adam Peterson for the use of his excellent Koppen Climate Zone Maps</p>
        <p>[5] Koryakov Yuri for early historical maps</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [10]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=352915">
          Kurgan Hypothesis on Indo-Europeans - Dbachmann
        </a>
        <br />
        [11]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=11299051">
          East_Slavic_tribes_peoples_8th_9th_century - SeikoEn
        </a>
        <br />
        [11] <a href="https://commons.wikimedia.org/w/index.php?curid=11221416">Mongol Invasion Map - By SeikoEn</a>
        <br />
        [12] <a href="https://commons.wikimedia.org/w/index.php?curid=14943136">Muscovy_1390_1525 - David Liuzzo</a>
        <br />
        [13] <a href="https://commons.wikimedia.org/w/index.php?curid=65101">Ivan The Terrible Throne - Stan Shebs</a>
        <br />
        [14] <a href="https://commons.wikimedia.org/w/index.php?curid=6098211">Partition of Poland - Halibutt/Sneecs</a>
        <br />
        [15] <a href="https://live.staticflickr.com/65535/50175708363_50bdfa9c43_o.jpg">Expansion of European Russia</a>
        <br />
        [16] <a href="https://commons.wikimedia.org/w/index.php?curid=21366121">1812-Europe - Alexander Altenhof</a>
        <br />
        [17] <a href="https://commons.wikimedia.org/w/index.php?curid=5031039">Tsushima_battle_map - historicair</a>
        <br />
        [17]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:Map_Europe_alliances_1914-ca.svg">
          Europe_alliances_1914 - Historicair
        </a>
        <br />
        [18] <a href="https://commons.wikimedia.org/w/index.php?curid=88915549">Lenin Train Route - Kimdime</a>
        <br />
        [19] <a href="https://commons.wikimedia.org/w/index.php?curid=16761719">Civil-War-Map - By Hoodinski</a>
        <br />
        [20] <a href="https://commons.wikimedia.org/wiki/File:Soviet_Union_1924.svg">Soviet_Union_1924- Shadowxfox</a>
        <br />
        [21]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:Woman_and_Man_in_1930s_Soviet_Propaganda_Poster_-_Regional_History_Museum_-_Zaporozhye_-_Ukraine_(42302404620).jpg">
          1930s_Soviet_Propaganda_Poster - Adam Jones
        </a>
        <br />
        [22] <a href="https://commons.wikimedia.org/w/index.php?curid=18353834">Gulag Fence - Gerald Praschl</a>
        <br />
        [23]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=11238381">
          Second_world_war_europe_1943-1945 - San Jose / University of Texas Libraries
        </a>
        <br />
        [24]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=7823226">EasternBloc_BorderChange - Mosedschurte</a>
        <br />
        [25] <a href="https://commons.wikimedia.org/wiki/File:Praga_11.jpg">1968 Prague Tanks - Engramma.it</a>
        <br />
        [26]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:A_factory_in_Rustavi_in_Soviet_Georgia._1957.jpg">
          1957 Factory - Vsevold Tarasyevich
        </a>
        <br />
        [27]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:NATO_vs._Warsaw_(1949-1990).png">
          NATO vs Warsaw Pact North Pole - Heitor Carvalho Jorge.png
        </a>
        <br />
        [28]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:President_Reagan%27s_Trip_to_USSR,_Walking_in_Red_Square_with_Mikhail_Gorbachev,_Moscow,_May_31,_1988.webm">
          1988-Gorbachev-Reagan-Moscow-Walking - Reagan Library
        </a>
        <br />
        [29] <a href="https://youtu.be/_-YwIynj8js">1989-Berlin-Wall - Manfred Krellenberg</a>
        <br />
        [30]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:USSR_Map_timeline.gif">
          1991 USSR Dissolution Animation - FreshCorp619
        </a>
        <br />
        [31] <a href="https://flic.kr/p/wH8LSq">1992 Flea Market - Brian Kelly</a>
        <br />
        [32]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=24443830">1998-Demonstration - Переславская неделя</a>
        <br />
        [33] <a href="https://commons.wikimedia.org/w/index.php?curid=38620627">GDP_of_Russia_since_1989 - heycci</a>
        <br />
        [35] <a href="https://commons.wikimedia.org/w/index.php?curid=106888233">State Duma Chamber - duma.gov.ru</a>
        <br />
        [36]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=76188420">
          Federation Council Chamber - council.gov.ru
        </a>
        <br />
        [37]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=9692358">
          Russian-regions - Tavork - Plavius - Luís Flávio Loureiro dos Santos
        </a>
        <br />
        [38] <a href="https://commons.wikimedia.org/wiki/File:Yeniseirivermap.png">Yenisey Map - Karl Musser</a>
        <br />
        [39] <a href="https://commons.wikimedia.org/wiki/File:Ob_river_basin_map.png">Ob Map - Shannon1</a>
        <br />
        [39] <a href="https://commons.wikimedia.org/wiki/File:Lena_River_basin.png">Lena Map - Shannon1</a>
        <br />
        [38] <a href="https://commons.wikimedia.org/wiki/File:Amurrivermap.png">Amur Map - Karl Musser</a>
        <br />
        [38] <a href="https://commons.wikimedia.org/wiki/File:Volgarivermap.png">Volga Map - Karl Musser</a>
        <br />
        [40]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=84020385">
          Trans-Siberian Railway Map - Natural Earth, Tom Patterson, Nathaniel Vaughn Kelso, Open Street Map,
          TownsNatural Earth
        </a>
        <br />
        [41]{" "}
        <a href="https://realrussia.co.uk/trains/trans-siberian/timetables">Trans-Siberian Timetable - RealRussia</a>
        <br />
        [43] Siberian Larch (Winter) - Anneli Salo
        <br />
        [44] Yakutia Taiga - Pavel Kazachkov
        <br />
        [45]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=58228192">Ethnic Groups Pie Chart - Nikolay Omonov</a>
        <br />
        [46]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=19833842">
          Linguistic Map - Maximilian Dörrbecker / Dr. phil. İhsan Yılmaz Bayraktarlı
        </a>
        <br />
        [47]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:Russia%27s_population_density_by_region.jpg">
          Population Density - Kartoshka1994
        </a>
        <br />
        [48]{" "}
        <a href="https://commons.wikimedia.org/w/index.php?curid=22112928">Slavic Languages Chart - MacedonianBoy</a>
        <br />
        [49] Mining in Russia - <a href="https://youtu.be/sdalfzLnbzw">Video Link</a> - Газомобиль
        <br />
        [50] Gas Industry - <a href="https://youtu.be/7a4hwbIfNqY">Video Link</a> - Telman 777
        <br />
        [51] Zircon hypersonic missile test - <a href="https://youtu.be/00XZ0pCvtxE">Video Link</a> - Odo Puiu Events
        <br />
        [52] 1950s Bomber - <a href="https://youtu.be/qC00d3bbC7E">Video Link</a> - Патриоты Родины
        <br />
        [53] Tupolev Tu-144 Supersonic - <a href="https://youtu.be/Job5qO5K3-E">Video Link</a> - World Army Club
        <br />
        [54] Mikoyan – Gurevich Mig-25 Foxbat - <a href="https://youtu.be/q2hylGyOVho">Video Link</a> - Hefa Productions
        <br />
        [55] Tu-160 White swan - <a href="https://youtu.be/IfKI63AuHWM">Video Link</a> - eXrusso
        <br />
        [56] Tu-134 Airliner - <a href="https://flic.kr/p/ac6Fyu">Hugh Llewelyn</a>
        <br />
        [57]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:EW-85748_Tupolev_Tu-154M_Belavia_parking_at_MSQ_(1).jpg">
          Yu-154 Airline - Vasyatka
        </a>
        <br />
        [58] MC-21 - <a href="https://youtu.be/UN_uBhLkSnU">Video Link</a> - IRKUT Corporation
        <br />
        [58] MC-21 - <a href="https://youtu.be/Wn5SGpmfmLA">Video Link</a> - IRKUT Corporation
        <br />
        [70] Faberge Eggs - <a href="https://youtu.be/qkwQrHgEF70">iFly Magazine</a>
        <br />
        [71] <a href="https://flic.kr/p/WMpuDa">Caviar - NuCastiel</a>
        <br />
        [72] Anatoly Karpov Chess grandmaster - <a href="https://youtu.be/AY4nENgFiCE">Video Link</a> - GibChess
        <br />
        [73] Russian National Orchestra, Moscow Conservatory - <a href="https://youtu.be/ZfCiJfrzJYM">Video Link</a> -
        Центр Стаса Намина
        <br />
        [74] <a href="https://commons.wikimedia.org/wiki/File:Nureyev_66_Allan_Warren.jpg">Nureyev - Allan Warren</a>
        <br />
        [75]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:Maria_Sharapova_at_2009_Roland_Garros,_Paris,_France.jpg">
          Sharapova - Misty
        </a>
        <br />
        [76]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:Maria_Sharapova_(2016).JPG">Sharapova - Tourism Victoria</a>
        <br />
        [77] <a href="https://www.flickr.com/photos/25951169@N00/474574895">Karpov - Ramon Peco</a>
        <br />
        [78]{" "}
        <a href="https://commons.wikimedia.org/wiki/File:RIAN_archive_628703_Soviet_cosmonauts,_Heroes_of_the_Soviet_Union_Pavel_Popovich,_Yuri_Gagarin,_and_Valentina_Tereshkova.jpg">
          Gagarin & Tereshkova - RIA Novosti
        </a>
        <br />
        [79] <a href="https://commons.wikimedia.org/wiki/File:2014-SakharovAN-PivovarovYuS.jpg">Sakharov - Ivtorov</a>
        <br />
        [80] <a href="https://commons.wikimedia.org/w/index.php?curid=6537369">Shostokovich - Deutsche Fotothek</a>
        <br />
        <h4>Content from the following organisations has been shown under the Fair Use Doctrine</h4>
        <p>[81] Cato Institute</p>
        <p>[82] OEC</p>
        <p>[83] Bolshoi Theatre</p>
        <p>[84] Kaspersky</p>
        <p>[85] Stolichnaya</p>
        <p>[86] Penguin Books</p>
        <p>[87] Deutsche Grammophon</p>
      </Credits>
    </>
  );
}
