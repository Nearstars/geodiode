import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseNz2(props) {
  const headerImage = "nz-2-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="14"
        heading="New Zealand"
        //subheading="KOPPEN CODE: Af"
        documentTitle="New Zealand - Its geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="P4IQy8juWGY"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=4503992">Polynesian Migration Map - David Eccles</B>
        <br />
        [3] <B to="https://www.earth.huji.ac.il/~tbird/">Plate Tectonics Map - Peter Bird</B>
        <br />
        [5]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Christchurch_Earthquake_220211.jpg">
          Christchurch Earthquake - Geoff Wilson
        </B>
        <br />
        [6] <B to="https://commons.wikimedia.org/wiki/File:Christchurch_quake_3.jpg">Christchurch Earthquake - Tim</B>
        <br />
        [7]{" "}
        <B to="https://upload.wikimedia.org/wikipedia/commons/a/a8/Christchurch_%285890644860%29.jpg">
          Christchurch Earthquake - Steve Collis
        </B>
        <br />
        [8] <B to="https://commons.wikimedia.org/w/index.php?curid=50252806">Koppen Climate Map - Adam Peterson</B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=64227608">Weka Bird - Skyring</B>
        <br />
        [10] <B to="https://commons.wikimedia.org/w/index.php?curid=19459532">Adzbill Bird - Nobu Tamura</B>
        <br />
        [11]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=71595167">Kiwi Birds - Judi Lapsley Miller, The.Rohit</B>
        <br />
        [12] <B to="https://commons.wikimedia.org/w/index.php?curid=8937367">Polynesian Languages - kwami</B>
        <br />
        [13]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=42753023">Austronesian Languages - Stefano Coretta</B>
        <br />
        [15] <B to="https://commons.wikimedia.org/w/index.php?curid=56957024">Regions and Municipalities - Korakys</B>
        <br />
        [16] <B to="https://commons.wikimedia.org/w/index.php?curid=41859036">Premier House - Ballofstring</B>
        <br />
        [17] <B to="https://commons.wikimedia.org/w/index.php?curid=6353680">Coat of Arms - Sodacan</B>
        <br />
        [20]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:New_Zealand_cricket_team,_Shoaib_Malik,_Dunedin,_NZ,_2009.jpg">
          Cricket - Benchill
        </B>
        <br />
        [20]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Emirates_Team_New_Zealand_at_the_Louis_Vuitton_Cup_2013.jpg">
          Yachting - Frank Schulenburg
        </B>
        <br />
        [22]{" "}
        <B to="https://thenounproject.com/browse/icons/term/nz-kiwi-bird">
          Kiwi Icon - The Kiwi Joanne Lush / The Noun Project
        </B>
        <br />
        [23]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Jonah_Lomu_Cardiff_cropped.jpg">
          Jonah Lomu - Russell J. Watkin
        </B>
        <br />
        [23] <B to="https://commons.wikimedia.org/wiki/File:Dan_Carter_2011.jpg">Dan Carter - Geoff Trotter</B>
        <br />
        [23]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Richie_McCaw_and_the_Webb_Ellis_cup_after_the_Rugby_World_Cup_final_2011.jpg">
          Richie McCaw - Jeanfrancois Beausejour
        </B>
        <br />
        [26]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Dave_Dobbyn_843_(51472787734).jpg">
          Sir Dave Dobbyn - Wendy Collings
        </B>
        <br />
        [26]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Kiri_Te_Kanawa_2013_(cropped).jpg">
          Dame Kiri Te Kanawa - Office of the Governor General
        </B>
        <br />
        [28] <B to="https://commons.wikimedia.org/wiki/File:NeilFinn1996.jpg">Neil Finn - Kai Gerullis</B>
        <br />
        [28]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:LordeRoundhse010622_(17_of_66)_(52119260286)_(cropped2).jpg">
          Lorde - Raph_PH
        </B>
        <br />
        [30]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Cliff_Curtis_(cropped).jpg">
          Cliff Curtis - Office of the Governor General
        </B>
        <br />
        [30] <B to="https://commons.wikimedia.org/wiki/File:Antony_Starr_(cropped).jpg">Anthony Starr - Eva Rinaldi</B>
        <br />
        [30]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Temuera_Morrison_2016.jpg">
          Temuera Morrison (Boba Fett) - US Embassy NZ
        </B>
        <br />
        [33]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Taika_Waititi_by_Gage_Skidmore_2.jpg">
          Taika Waititi - Gage Skidmore
        </B>
        <br />
        [33]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Michael_Dorman,_Patriot-_A_Special_Preview.jpg">
          Michael Dorman - New America
        </B>
        <br />
        [33]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Anna_Paquin_by_Gage_Skidmore.jpg">Anna Paquin - Gage Skidmore</B>
        <br />
        [36]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Lucy_Lawless_(7595206416).jpg">Lucy Lawless - Gage Skidmore</B>
        <br />
        [36] <B to="https://commons.wikimedia.org/wiki/File:Karl_Urban_(26512263814).jpg">Karl Urban - Crosa</B>
        <br />
        [38] <B to="https://commons.wikimedia.org/wiki/File:Russell_Crowe_2,_2012.jpg">Russell Crowe - Eva Rinaldi</B>
        <br />
        [38] <B to="https://commons.wikimedia.org/wiki/File:Sam_Neill_2010.jpg">Sam Neill - Sean Koo</B>
        <br />
        [40] <B to="https://commons.wikimedia.org/wiki/File:PERX0152_05.jpg">Dame Jane Campion - Gorup de Besanez</B>
        <br />
        [41]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Peter_Jackson_ONZ_(cropped).jpg">
          Peter Jackson - Office of the Governor General
        </B>
        <br />
        <h4>FAIR USE:</h4>
        Human Freedom Index - Cato Institute
        <br />
        Exports Graph - oec.world
        <br />
        All Blacks
        <br />
        Xena Warrior Princess
        <br />
        Spartacus
        <br />
        Shannara Chronicles
        <br />
        Lord of the Rings - New Line Cinema
        <br />
      </Credits>
    </>
  );
}
