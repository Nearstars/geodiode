import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NsePhilippines(props) {
  const headerImage = "philippines-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="5"
        heading="The Philippines"
        //subheading="KOPPEN CODE: Af"
        documentTitle="The Philippines - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="9Mzw4Ln-nNk"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>

        <p>
          [1] Banaue Rice Terraces <B to="https://youtu.be/bRfaGDgsxyU"> Gene Hettel</B>
        </p>
        <p>
          [2] Mayon Eruption <B to="https://youtu.be/M3bG2J7RQkw"> Civil Disturbia</B>
        </p>
        <p>
          [3] Lanzones Festival <B to="https://youtu.be/HDUcgzYfjFM"> Republika ng Paraiso</B>
        </p>
        <p>
          [4] Manila <B to="https://youtu.be/Od2roxA0MY4"> winelfredpasamba</B>
        </p>
        <p>
          [5] Cebu City Aerial view <B to="https://youtu.be/rLxTizJvyZA"> BISDAK NOYPI TV</B>
        </p>
        <p>
          [6] Davao <B to="https://youtu.be/4Q8U_E_PDso"> Isla tv blog</B>
        </p>
        <p>
          [7] Makati, Manila <B to="https://youtu.be/tvYCk_alj1s"> Christian Tandberg Media</B>
        </p>
        <p>
          [8] Philippines Drone Compilation <B to="https://youtu.be/p1E1TkmDfEQ"> res demdam</B>
        </p>
        <p>
          [9] Sinulog Festival <B to="https://youtu.be/9t9wNEE0_II"> globalvideopro1</B>
        </p>
        <p>
          [10] Batanes Islands <B to="https://youtu.be/8EPVpt09aKs"> kayzerific</B>
        </p>
        <p>
          [11] Palompon Leyte <B to="https://youtu.be/VUpjhP2aWRY"> DarkNoctis Vi Charm</B>
        </p>
        <p>
          [12] Luzon (Rice Terraces) <B to="https://youtu.be/3GwyacgYaIw"> Where is Marina</B>
        </p>
        <p>
          [13] Jeepney <B to="https://commons.wikimedia.org/wiki/File:Jeepney_cebu_1.jpg"> Øyvind Holmstad</B>
        </p>
        <p>
          [14] Timeline of Philippine History{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=51933826"> JournalmanManila</B>
        </p>
        <p>
          [15] Battle of Mactan <B to="https://commons.wikimedia.org/w/index.php?curid=10357771"> Nmcast</B>
        </p>
        <p>
          [16] Fort San Pedro <B to="https://commons.wikimedia.org/w/index.php?curid=21583939"> Badz Patanag</B>
        </p>
        <p>
          [17] Old Globe Map{" "}
          <B to="https://www.reddit.com/r/Philippines/comments/ee4e6d/another_old_philippine_map/"> u/ismonec</B>
        </p>
        <p>
          [19] Rules under Japanese Occupation <B to="https://flic.kr/p/nHUXAC"> John Tewell</B>
        </p>
        <p>
          [20] Manourished Girl WWII <B to="https://flic.kr/p/29RGjhv"> John Tewell</B>
        </p>
        <p>
          [21] Imelda's shoes <B to="https://www.flickr.com/photos/22320444@N08/4999794433"> Vince_Lamb</B>
        </p>
        <p>
          [22] Manila_Philippines_Slum{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Manila_Philippines_Slums-in-Manila-01.jpg">
            {" "}
            CEPhoto, Uwe Aranas
          </B>
        </p>
        <p>
          [23] Manila Slum <B to="https://flic.kr/p/4p4kGh"> มุก</B>
        </p>
        <p>
          [24] Samar and Leyte (Visayas) <B to="https://youtu.be/EENCFgOHNqA"> The Philippines Section Channel</B>
        </p>
        <p>
          [25] container ship crew{" "}
          <B to="https://justinsomnia.org/2010/09/barbecue-party-on-the-container-ship/"> just insomnia</B>
        </p>
        <p>
          [26] Domestic Workers{" "}
          <B to="https://www.middleeastmonitor.com/20180318-kuwait-reaches-deal-with-philippines-on-domestic-workers/">
            {" "}
            Middle East Monitor
          </B>
        </p>
        <p>
          [27] Filipina Model <B to="https://flic.kr/p/dqKSKY"> Manny De Guzman, Jr.</B>
        </p>
        <p>
          [28] Labelled_map_of_the_Philippines_-_Provinces_and_Regions{" "}
          <B to="https://en.wikipedia.org/wiki/File:Labelled_map_of_the_Philippines_-_Provinces_and_Regions.png">
            {" "}
            Sanglahi86
          </B>
        </p>
        <p>
          [29] Sulop_Barangay_Hall <B to="https://commons.wikimedia.org/w/index.php?curid=19011670"> QuecyKeith</B>
        </p>
        <p>
          [30] Maybo_Barangay_Hall <B to="https://commons.wikimedia.org/w/index.php?curid=21173953"> By T3n60</B>
        </p>
        <p>
          [31] Mariki_Barangay_Hall{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=48791277"> Wowzamboangacity</B>
        </p>
        <p>
          [32] Mount Apo <B to="https://flic.kr/p/wkZ8qS"> Bro. Jeffrey Pioquinto, SJ</B>
        </p>
        <p>
          [33] Tectonic_plates_boundaries_detailed-en.svg.png{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Tectonic_plates_boundaries_detailed-en.svg"> Eric Gaba</B>
        </p>
        <p>
          [34] PhilippineMobileBelt007{" "}
          <B to="https://commons.wikimedia.org/wiki/File:PhilippineMobileBelt007.jpg"> Gubanatoria</B>
        </p>
        <p>
          [35] Haiyan Destruction <B to="https://commons.wikimedia.org/w/index.php?curid=43911464"> Lawrence Ruiz</B>
        </p>
        <p>
          [36] Rice Transplanting <B to="https://youtu.be/dIA5rKrve4U"> Gene Hettel</B>
        </p>
        <p>
          [37] Population Density Map{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Philippines_Population_density_1km_AsiaPop_source_10v1.pdf">
            {" "}
            Amibreton
          </B>
        </p>
        <p>
          [38] Dialects Map{" "}
          <B to="https://www.reddit.com/r/Philippines/comments/3q5jbx/a_multilingual_philippines_linguistic_map_of_the/">
            {" "}
            u/pansitkanton
          </B>
        </p>
        <p>[39] Languages Pie Chart - meta-chart.com</p>
        <p>
          [40] Spanish Newspaper <B to="https://flic.kr/p/95dLhD"> Rafael Minuesa</B>
        </p>
        <p>
          [41] Austronesians Map <B to="https://commons.wikimedia.org/w/index.php?curid=42753023"> Stefano Coretta</B>
        </p>
        <p>
          [42] Ati_woman <B to="https://commons.wikimedia.org/w/index.php?curid=2227318"> Ken Ilio from Chicago, USA</B>
        </p>
        <p>
          [43] Tagbanua_weaver <B to="https://commons.wikimedia.org/w/index.php?curid=64527389"> By Darwgon0801</B>
        </p>
        <p>
          [45] Historical_GDP_growth_of_the_Philippines{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Historical_GDP_growth_of_the_Philippines.png">
            {" "}
            GiovanniMartin16
          </B>
        </p>
        <p>
          [46] Subic Shipyard{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Hanjin_Subic_Shipyard_panoramio_114271925.jpg">
            {" "}
            schlawginski
          </B>
        </p>
        <p>
          [47] Taganito_Mining_Corp._nickel_ore_conveyor_belts{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Taganito_Mining_Corp._nickel_ore_conveyor_belts_-_Flickr.jpg">
            {" "}
            The EITI
          </B>
        </p>
        <p>
          [48] Geothermal Plant{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=1246507"> Mike Gonzalez (TheCoffee)</B>
        </p>
        <p>
          [49] Call Center <B to="https://commons.wikimedia.org/wiki/File:Teletech_call_cent_BACOLOD.jpg"> Maverx</B>
        </p>
        <p>
          [50] Siargao, Philippines <B to="https://youtu.be/dIyoDd3WB5U"> Joemary Leung</B>
        </p>
        <p>
          [51] Sabah_West_Coast_Bajau_women_in_traditional_dress{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=63324328"> CEphoto / Uwe Aranas</B>
        </p>
        <p>
          [52] Lanzones_Festival_-_Mambajao_Icon.jpg{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=76191673"> Theglennpalacio</B>
        </p>
        <p>
          [53] Sinulog Festival Cebu City{" "}
          <B to="https://commons.wikimedia.org/w/index.php?curid=31124270"> By Jescario</B>
        </p>
        <p>
          [54] Clark Air Base <B to="https://flic.kr/p/7ukhP5"> John Tewell</B>
        </p>
        <p>
          [55] Nicole_Scherzinger{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Nicole_Scherzinger_%26_the_Phantoms_-_12.jpg">
            {" "}
            Royal Variety Charity on Vimeo
          </B>
        </p>
        <p>
          [56] Sharon_Leal_at_the_36th_Annual_Gracie_Awards_Gala
          <B to="https://commons.wikimedia.org/wiki/File:Sharon_Leal_at_the_36th_Annual_Gracie_Awards_Gala.jpg">
            {" "}
            Mingle MediaTV
          </B>
        </p>
        <p>
          [57] Bruno_Mars_-_24K_Magic_Tour_2017 <B to="https://flic.kr/p/BNyGDo"> slgckgc</B>
        </p>
        <p>
          [58] Christina_Gambito_cropped
          <B to="https://commons.wikimedia.org/wiki/File:Christina_Gambito_cropped.jpg"> Tommy Lane</B>
        </p>
        <p>
          [59] LeaSalonga <B to="https://commons.wikimedia.org/wiki/File:LeaSalonga.jpg"> Btvway</B>
        </p>
        <p>
          [60] Vanessa_Hudgens{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Vanessa_Hudgens_(Headshot).jpg"> Josh Hallett</B>
        </p>
        <p>
          [61] Apl.de.ap<B to="https://commons.wikimedia.org/wiki/File:Apl.de.ap_2011.jpg"> Walmart Stores</B>
        </p>
        <p>
          [62] Enrique_Iglesias<B to="https://flic.kr/p/a6ia2C"> Eva Rinaldi</B>
        </p>
        <p>
          [63] Caterina_fake<B to="https://flic.kr/p/xe9w4"> Caterina Fake / Richard Morgenstein</B>
        </p>
        <p>
          [65] Lou_Diamond_Phillips{" "}
          <B to="https://commons.wikimedia.org/wiki/File:Lou_Diamond_Phillips_at_the_Chiller_Theatre_Expo_2017.jpg">
            {" "}
            Rob DiCaterino
          </B>
        </p>
        <p>
          [67] Siquijor Island <B to="https://youtu.be/r4FjGmVY4rI"> SwissHumanity Stories</B>
        </p>
        <p>
          [68] Kingfisher (North of Luzon) <B to="https://youtu.be/bcrfxqqQpk4"> Patrick Kuipers</B>
        </p>
        <p>
          [69] Mayon Eruption <B to="https://youtu.be/SfR-ckiuwz4"> Allan Ecleo</B>
        </p>

        <p></p>
        <h4>Sections of the following reproduced under Fair Use:</h4>
        <p>
          [37] <B to="https://www.cato.org/human-freedom-index-new">Cato Institute</B>
        </p>
        <p>
          [44] <B to="https://oec.world/">OEC graph</B>
        </p>
        <p>
          [64] Monique Lhullier{" "}
          <B to="https://coveteur.com/2016/05/24/monique-lhuillier-iconic-wedding-dresses/">Coveteur Magazine</B>
        </p>
        <p>
          [66] Dado Banatao{" "}
          <B to="https://www.techinasia.com/dado-banatao-education-boost-philippines-tech-startup-ecosystem">
            Tech In Asia
          </B>
        </p>
      </Credits>
    </>
  );
}
