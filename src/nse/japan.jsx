import React from "react";
import Chapter from "../components/chapter";
import B from "../components/B";
import Credits from "../components/credits";

export default function NseJapan(props) {
  const headerImage = "japan-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="2"
        heading="Japan"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Japan - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="uQX8UwV87Os"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        <p>[2] Jomon Statue - <B to="https://bit.ly/2ZKZMCJ">Rc 13</B></p>
        <p>[3] Yayoi Bell - <B to="https://bit.ly/2ZGlKqw">PHGCOM</B></p>
        <p>[4] Book of Han - <B to="https://bit.ly/32AZFeP">Gisling</B></p>
        <p>[5] Records of the Three Kingdoms - <B to="https://bit.ly/3ko9TFs">猫猫的日记本</B></p>
        <p>[6] Early Buddhist temple (Kōfuku-ji) - <B to="https://bit.ly/3iBv1rl">Dariusz Jemielniak</B></p>
        <p>[7] Todai-ji temple, Nara - <B to="https://bit.ly/3ktK4Ux">Wiiii</B></p>
        <p>[8] Map of Sengoku period - <B to="https://flic.kr/p/iV27sX">Stuart Rankin</B></p>
        <p>[9] 1590 Map of Japan - <B to="https://flic.kr/p/25BBmC1">LunchBoxLarry</B></p>
        <p>[10] Early train - <B to="https://bit.ly/32AxM6w">Minato City Local History Museum</B></p>
        <p>[11] Mill - <B to="https://bit.ly/2HaKNeZ">National Diet Library</B></p>
        <p>[12] Star fort in Hakodate - <B to="https://bit.ly/3iABx1i">京浜にけ</B></p>
        <p>[13] 1930s Map - <B to="https://bit.ly/35GzXY8">Emok</B></p>
        <p>[14] 1939 Map - <B to="https://bit.ly/3mrg8Kd">Emok</B></p>
        <p>[15] 1960’s Honda Motorcycles factory – <B to="https://youtu.be/k40WmUci7yU">Shane Conley</B></p>
        <p>[16] History of Sony 60s and 70s - <B to="https://youtu.be/vaskou2gl44">nefesh22</B></p>
        <p>[17] Historical GDP Growth - GiovanniMartin16</p>
        <p>[18] Population Pyramid 1990 - <B to="https://bit.ly/33Ayz6Q">Rickky1409</B></p>
        <p>[19] Nikkei Historical Price - <B to="https://bit.ly/32yhuLk">Encik Tekateki</B></p>
        <p>[20] National Diet Building - <B to="https://bit.ly/2ZKW3ou">Wiiii</B></p>
        <p>[21] House of Representatives - <B to="https://bit.ly/3c3AULi">Kimtaro</B></p>
        <p>[22] House of Councillors - <B to="https://bit.ly/35GUwUl">Chris 73</B></p>
        <p>[23] Koppen map - <B to="https://bit.ly/3iCvkC9">Beck et al</B></p>
        <p>[24] Snowy Hokkaido / Coniferous Forest - <B to="https://youtu.be/LV2N9GRsYB0">이칸트 필름2can't film</B></p>
        <p>[25] Snowy Hokkaido / Coniferous Forest - <B to="https://youtu.be/U4_KtTBo7Bk">Michiyoshi Shirota</B></p>
        <p>[26] Rain in Tokyo - <B to="https://youtu.be/5PaVIa-dS-k">4giga7</B></p>
        <p>[27] Plate tectonics map - <B to="http://peterbird.name/">Peter Bird</B></p>
        <p>[28] <B to="https://youtu.be/rzkyT2cT3-o">Narita Airport Earthquake</B></p>
        <p>[29] 2011 Tsunami - <B to="https://youtu.be/0IZ9cWAXQGE">Vintage throwback classics</B></p>
        <p>[30] Pop density map - <B to="https://bit.ly/35J1Kaf">Masana Yokoya Hideyasu Shimizu Yukito Higuchi</B></p>
        <p>[31] Population pyramid - <B to="https://bit.ly/3cfYVPz">Rickky1409</B></p>
        <p>[32] Japonic languages map - <B to="https://bit.ly/3c2BVTX">Enirac Sum</B></p>
        <p>[36] Tokyo Stock Exchange - <B to="https://bit.ly/33xjDWV">Kakidai</B></p>
        <p>[38] Sumo wrestling - <B to="https://youtu.be/aAsiYXsj28E">Paul O’Brien</B></p>
        <p>[39] Judo - <B to="https://youtu.be/VOomWcgrHis">Chadi</B></p>        
        <h4>Sections of the following reproduced under Fair Use:</h4>
        <p>[33] Exports tree graph - <B to="https://oec.world/en/profile/country/jpn">Observatory of Economic Complexity</B></p>
        <p>[34] Nissan modern - <B to="https://youtu.be/El5vpA8wdAs">Fair Use</B></p>
        <p>[35] Casio modern - <B to="https://youtu.be/eB2uCKTFwpk">Fair Use</B></p>
        <p>[37] Govt debt graph - <B to="https://data.oecd.org/japan.htm ">OECD</B></p>
      </Credits>
    </>
  );
}
