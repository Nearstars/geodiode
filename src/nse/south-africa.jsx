import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseSouthAfrica(props) {
  const headerImage = "south-africa-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="7"
        heading="South Africa"
        //subheading="KOPPEN CODE: Af"
        documentTitle="South Africa - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="VHdBw3k5KcQ"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [5] <B to="https://youtu.be/ZIUNp88wvUY">Apartheid 1957 - Khalbrae</B>
        <br />
        [6] <B to="https://youtu.be/PYm7K4-Yzp8">Music street scene - Ajs</B>
        <br />
        [10]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Maropeng_visitor_centre,_Cradle_of_Humankind,_South_Africa.jpg">
          Maropeng - Olga Ernst
        </B>
        <br />
        [11]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Fossil_Hominid_Sites_of_South_Africa-113354.jpg">
          Hominid Fossils - UNESCO
        </B>
        <br />
        [12] <B to="https://commons.wikimedia.org/w/index.php?curid=68921860">Homo sapiens migrations - Dbachmann</B>
        <br />
        [13] <B to="https://youtu.be/s5fMt5xwNR4">Khoisan People - InsightShare</B>
        <br />
        [14] <B to="https://commons.wikimedia.org/wiki/File:Gama_route_1.png">Vasco de Gama Route - Feydey</B>
        <br />
        [15] <B to="https://commons.wikimedia.org/w/index.php?curid=34945945">Great Trek Map - Discott</B>
        <br />
        [16] <B to="https://commons.wikimedia.org/w/index.php?curid=33565279">Zulu Expansion - Discott</B>
        <br />
        [17] <B to="https://www.youtube.com/watch?v=aTlFao4fuVA">Gold and Diamond Mining 1917 - Union Films</B>
        <br />
        [18]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Map_of_the_provinces_of_South_Africa_1910-1976_with_English_labels.svg">
          1910-1976 Provinces map - Htonl
        </B>
        <br />
        [19] <B to="https://commons.wikimedia.org/w/index.php?curid=11069071">1910-1928 Flag - Fornax</B>
        <br />
        [20]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Bantustans_in_South_Africa.svg">
          Bantustans in South Africa - Htonl
        </B>
        <br />
        [21] <B to="https://commons.wikimedia.org/w/index.php?curid=6376182">Sharpville Massacre - Godfrey Rubens</B>
        <br />
        [22] <B to="https://commons.wikimedia.org/w/index.php?curid=27740971">Soweto Uprising - Sam Nzima</B>
        <br />
        [23] <B to="https://commons.wikimedia.org/w/index.php?curid=26754876">Mandela Voting - Paul Weinberg</B>
        <br />
        [24]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:National_Assembly_(South_Africa)_party_composition_history_de.svg">
          Post 1994 Election History - Htonl
        </B>
        <br />
        [25] <B to="https://commons.wikimedia.org/w/index.php?curid=64481505">Relative Income By Race - Borvan53</B>
        <br />
        [26] <B to="https://ourworldindata.org/homicides">Murder Rate - Our World In Data</B>
        <br />
        [27] <B to="https://www.flickr.com/photos/palaciodoplanalto/48142656917/">BRICS photo - Alan Santos/PR</B>
        <br />
        [28]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Houses_of_Parliament_(Cape_Town).jpg">
          Houses of Parliament Exterior - PhilippN
        </B>
        <br />
        [29] <B to="https://flic.kr/p/7PdVJX">National Assembly Interior - shizhao</B>
        <br />
        [30] <B to="https://flic.kr/p/7PdUHR">National Council of Provinces Interior - shizhao</B>
        <br />
        [31] <B to="https://commons.wikimedia.org/w/index.php?curid=35070214">Union Buildings - Robbie Aspeling</B>
        <br />
        [32] <B to="https://commons.wikimedia.org/w/index.php?curid=5135568">Provinces Map - Htonl</B>
        <br />
        [33] Koppen Map - Adam Peterson
        <br />
        [34] <B to="https://commons.wikimedia.org/w/index.php?curid=1703126">Floristic kingdoms - Dietzel</B>
        <br />
        [35] Pop Density - Adrian Frith
        <br />
        [36] <B to="https://youtu.be/bf7tZng_lEI">Johannesburg - WODE MAYA</B>
        <br />
        [37] <B to="https://youtu.be/tNGpklQ_97Y">Pretoria - Private Property</B>
        <br />
        [38] <B to="https://commons.wikimedia.org/w/index.php?curid=8707950">Port Elizabeth - Ngrund</B>
        <br />
        [39] Mining - Creamer Media - <B to="https://youtu.be/rCOlRqeHgI4">Video 1</B>{" "}
        <B to="https://youtu.be/Ph3r1nvolKI">Video 2</B> <B to="https://youtu.be/Zgzq2GX83zA">Video 3</B>
        <br />
        [40] <B to="https://commons.wikimedia.org/w/index.php?curid=30146150">Gold Production Graph - Plazak</B>
        <br />
        [41]{" "}
        <B to="https://www.banknoteworld.com/south-africa-50-rand-banknote-2018-p-145-unc.html">
          Rand notes - Bank Note World
        </B>
        <br />
        [42]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=61400468">Krugerrand - Gruener Panda / coininvest.com</B>
        <br />
        [43]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:2015_Rugby_World_Cup,_South_Africa_vs._USA_(21845972899).jpg">
          Rugby - David Roberts
        </B>
        <br />
        [44]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:NeillBlomkampCCJuly09.jpg">
          Neil Blomkamp, Johannesburg - Natasha Baucas
        </B>
        <br />
        [45]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Desmond_Tutu_(47327456801)_(cropped_and_adjusted).jpg">
          Desmond Tutu, Klerksdorp - John Mathew Smith & www.celebrity-photos.com
        </B>
        <br />
        [46]{" "}
        <B to="https://www.flickr.com/photos/raph_ph/41036851484/in/album-72157695502428854/">
          Ladysmith Black Mambazo - Raph_PH
        </B>
        <br />
        [47]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=4515287">
          Wilbur Smith, Ndola Northern Rhodesia - jimincairns
        </B>
        <br />
        [48]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=63775171">
          Hugh Masakela, Witbank - WOMEX, Jacob Crawfurd
        </B>
        <br />
        [49]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=83322915">
          Charlize Theron, Benoni, Transvaal - Gage Skidmore
        </B>
        <br />
        [50]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=61400468">Elon Musk, Pretoria - The Royal Society</B>
        <br />
        [51]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Nelson_Mandela_1994.jpg">
          Nelson Mandela, Mvezo - Kingkongphoto & www.celebrity-photos.com
        </B>
        <br />
        <h4>FAIR USE</h4>
        <p>[4] Langa / World Music Legends</p>
        <p>[7] Ford South Africa</p>
        <p>[8] CATO</p>
        <p>[9] OEC.world</p>
      </Credits>
    </>
  );
}
