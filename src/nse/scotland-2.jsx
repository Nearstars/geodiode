import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseScotland2(props) {
  const headerImage = "scotland-2-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="12"
        heading="Scotland"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Scotland - Its geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="DmfuCxqz2fM"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [1] <B to="https://commons.wikimedia.org/w/index.php?curid=50252806">Koppen Climate Map - Adam Peterson</B>
        <br />
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=1321204">Plockton Palms - Wojsyl</B>
        <br />
        [3]{" "}
        <B to="http://www.statsmapsnpix.com/2017/12/population-density-in-europe.html">
          Population Density Map - Alasdair Rae
        </B>
        <br />
        [4] <B to="https://commons.wikimedia.org/w/index.php?curid=56809610">Scottish Identity Maps - Brythones</B>
        <br />
        [5] <B to="https://www.reddit.com/r/MapPorn/comments/aemy7t/dialects_of_scotland/">Dialects of Scotland</B>
        <br />
        [6]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=31997403">Scots and Gaelic Language Maps - SkateTier</B>
        <br />
        [7] <B to="https://commons.wikimedia.org/wiki/File:Welsh_National_Assembly_Senedd.jpg">Welsh Senedd - eNil</B>
        <br />
        [8]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Stormont_-_Belfast_-_Northern_Ireland.jpg">
          Stormont - William Murphy
        </B>
        <br />
        [9]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Scottish_Parliament_Debating_Chamber_3.jpg">
          Scottish Parliament - Colin
        </B>
        <br />
        [10]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Law_Courts_entrance,_Parliament_Square.JPG">
          Law Court - Kim Traynor
        </B>
        <br />
        [11] <B to="https://commons.wikimedia.org/w/index.php?curid=6643558">Councils Map - XrysD</B>
        <br />
        [12]{" "}
        <B to="https://en.wikipedia.org/wiki/Royal_arms_of_Scotland#/media/File:Royal_Arms_of_the_Kingdom_of_Scotland.svg">
          Royal Arms - sodacan
        </B>
        <br />
        [13] <B to="https://commons.wikimedia.org/w/index.php?curid=2237174">Royal Banner - Eyrian</B>
        <br />
        [14] <B to="https://commons.wikimedia.org/wiki/File:Skara_Brae-maison.jpg">Skara Brae - Daniel Bordeleau</B>
        <br />
        [15]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Pizza_in_deep_fat_fryer_2.jpg">
          Deep Fried Pizza - Edward Betts
        </B>
        <br />
        [15]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Anstruther_Fish_Supper.jpg">Fish'n'Chips - Edinburgh Blog</B>
        <br />
        [16]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Anstrutherfishbar2.jpg">Anstruther Fish Bar - Robert Young</B>
        <br />
        [18] <B to="https://youtu.be/FAk_Nj8NEic">Haggis - ScottishGourmet</B>
        <br />
        [19]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Clan_Sutherland_tartan_in_Clan_Munro_exhibition.jpg">
          Clan Tartans - QuintusPetillius
        </B>
        <br />
        [20]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=95944834">
          St Andrews Course Plan - National Records of Scotland
        </B>
        <br />
        [21] <B to="https://commons.wikimedia.org/w/index.php?curid=18564266">Napier Mathematics - Kim Traynor</B>
        <br />
        [22]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=17966715">Growth of Scots Language Map - Hayden120</B>
        <br />
        [25] <B to="https://commons.wikimedia.org/w/index.php?curid=11207816">Deborah Kerr - Allan Warren</B>
        <br />
        [26] <B to="https://commons.wikimedia.org/w/index.php?curid=16104443">Tom Conti - Contains Mild Peril</B>
        <br />
        [27] <B to="https://commons.wikimedia.org/w/index.php?curid=52456024">Brian Cox - Dmitry Rozhkov</B>
        <br />
        [27] <B to="https://commons.wikimedia.org/w/index.php?curid=27702819">Peter Capaldi - Stuart Crawford</B>
        <br />
        [27]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=76249114">David Tennant - Nerd Rabugento / Entrevista</B>
        <br />
        [28] <B to="https://commons.wikimedia.org/w/index.php?curid=80813992">James McIvoy - Gage Skidmore</B>
        <br />
        [29] <B to="https://commons.wikimedia.org/w/index.php?curid=6021113">Kelly Macdonald - Mark Rabinowitz</B>
        <br />
        [29] <B to="https://commons.wikimedia.org/w/index.php?curid=65338074">Karen Gillan - Gage Skidmore</B>
        <br />
        [30] <B to="https://commons.wikimedia.org/wiki/File:Irvine_Welsh_by_Kubik.JPG">Irvine Welsh - Mariusz Kubik</B>
        <br />
        [30] <B to="https://commons.wikimedia.org/w/index.php?curid=7599707">Iain Banks - TimDuncan</B>
        <br />
        [31] <B to="https://www.flickr.com/photos/evarinaldiphotography/7175840985/">Billy Connoly - By Eva Rinaldi</B>
        <br />
        [32] <B to="https://commons.wikimedia.org/w/index.php?curid=27203550">Sean Connery - By Rob Bogaerts / Anefo</B>
        <br />
        <h4>FAIR USE:</h4>
        [17] <B to="https://youtu.be/zj8CIUInwfs">Deep Fried Mars Bar - University of Glasgow</B>
        <br />
        [50] Ewan McGregor - Disney Fox
        <br />
        [51] Robbie Coltrane - Warner Bros
        <br />
        [52] Richard Madden - Nick Briggs
        <br />
      </Credits>
    </>
  );
}
