import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseScotland1(props) {
  const headerImage = "scotland-1-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="11"
        heading="The History of Scotland"
        //subheading="KOPPEN CODE: Af"
        documentTitle="The History of Scotland"
        headerImageSrc={headerImage}
        youTubeCode="Nc4Czmt56Vg"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [1]{" "}
        <B to="https://rachelbrackenridge.wixsite.com/polkadotexplorer/post/glacial-edinburgh">
          Ice Sheet Britain - Rachel Brackenridge
        </B>
        <br />
        [2]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Neolithic_Callanish_Stones.jpg">Callanish Stones - SidBaility</B>
        <br />
        [3]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Barnhouse_Neolithic_village_-_geograph.org.uk_-_3638271.jpg">
          Barnhouse Neolithic - Bill Boaden
        </B>
        <br />
        [4]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Bosta_iron-age_house_-_geograph.org.uk_-_2183889.jpg">
          Iron Age House - Andy Campbell
        </B>
        <br />
        [5]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Early_Iron_Age_socketed_axe_(FindID_510709).jpg">
          Celtic Bronze Axe Heads - Portable Antiquities Scheme
        </B>
        <br />
        [6] <B to="https://commons.wikimedia.org/w/index.php?curid=5622687">Celtic Bronze Shield - QuartierLatin1968</B>
        <br />
        [7] <B to="https://www.flickr.com/photos/rosemania/4121249212">Celtic Helm - Rosemania (CC BY 2.0)</B>
        <br />
        [8] <B to="https://commons.wikimedia.org/w/index.php?curid=4003285">Central Europe Celt Map - Dbachmann</B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=638312">Celtic Expanse Map - QuartierLatin1968</B>
        <br />
        [10] <B to="https://commons.wikimedia.org/w/index.php?curid=28924999">Bronze Age Map - Xoil</B>
        <br />
        [11]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=1642359">
          Celtic Nations Map - QuartierLatin1968 / OsgoodeLawyer
        </B>
        <br />
        <B to="https://commons.wikimedia.org/wiki/File:Roman.Britain.roads.jpg">
          Roman Roads In Britain - Notuncurious
        </B>
        <br />
        [12]{" "}
        <B to="https://en.wikipedia.org/wiki/Venicones#/media/File:Britain.north.peoples.Ptolemy.jpg">
          Roman Scotland Map - Notuncurious
        </B>
        <br />
        [12]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Roman.Britain.north.180.jpg">
          Hadrian's Wall Map - Notuncurious
        </B>
        <br />
        [13]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Antonine_Wall_ditch_looking_west_at_Roughcastle,_Falkirk,_Scotland.jpg">
          Antonine Wall Today - Rosser1954
        </B>
        <br />
        [14]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:The_Antonine_Wall_near_Bar_Hill_Roman_Fort,_Twechar_-_geograph.org.uk_-_846377.jpg">
          Antonine Wall Today - Andrew Barclay
        </B>
        <br />
        [12] <B to="https://www.worldhistory.org/image/6698/britain-383-410-ce/">Roman Rule End - Notuncurious</B>
        <br />
        <B to="https://commons.wikimedia.org/wiki/File:Anglo-Saxon_Homelands_and_Settlements.svg">
          Anglo-Saxon Migration - mbartelsm
        </B>
        <br />
        [16] <B to="https://commons.wikimedia.org/w/index.php?curid=34776657">Iona Cross - NickGibson3900</B>
        <br />
        [17] <B to="https://commons.wikimedia.org/w/index.php?curid=19885072">Britain Map 878 - Hel-hama</B>
        <br />
        [18]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Gjermundbu_helmet_-_cropped.jpg">
          Viking Helm - NTNU Vitenskapsmuseet
        </B>
        <br />
        [19] <B to="https://commons.wikimedia.org/w/index.php?curid=2918974">Norse Kingdoms Map - Sémhur</B>
        <br />
        <B to="https://commons.wikimedia.org/w/index.php?curid=17966715">Growth of Scots Language - Hayden120</B>
        <br />
        [21]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:William_Wallace_statue_edinburgh_2.JPG">
          William Wallace Statue - Ad Meskens
        </B>
        <br />
        [22] <B to="https://commons.wikimedia.org/w/index.php?curid=4877345">Robert The Bruce Bust - Otter</B>
        <br />
        [23]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=32601439">Wars of Independence Figures - William Hole</B>
        <br />
        [25] <B to="https://commons.wikimedia.org/w/index.php?curid=87832845">Little Ice Age Graph - RCraig09</B>
        <br />
        [26] <B to="https://commons.wikimedia.org/w/index.php?curid=27804909">1689 Map - http://maps.bpl.org</B>
        <br />
        [27] <B to="https://commons.wikimedia.org/w/index.php?curid=17206466">Panama Map - Hogweard (CC BY 3.0)</B>
        <br />
        [28] <B to="https://commons.wikimedia.org/w/index.php?curid=36748037">Act of Union voting map - MrPenguin20</B>
        <br />
        [29] <B to="https://commons.wikimedia.org/w/index.php?curid=29564616">Culloden Map - Celtus</B>
        <br />
        [30]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Culloden_battlefield6.jpg">Culloden Monument - Stablenode</B>
        <br />
        [31]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=101750586">RMS Queen Elizabeth - George John Edkins</B>
        <br />
        [32]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=24470156">Protestants in Ulster Map - Wallpaper68</B>
        <br />
        [33] <B to="https://commons.wikimedia.org/w/index.php?curid=16135925">Orange Order Banner - Ardfern</B>
        <br />
        [34] <B to="https://commons.wikimedia.org/w/index.php?curid=127349041">Gordon Brown - HM Treasury</B>
        <br />
        [35]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Vanguard_at_Faslane_04.jpg">
          Faslane Submarine - CPOA(Phot) Tam McDonald/MOD
        </B>
        <br />
        [36]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Castlemilk_public_council_estate_in_1983.jpg">
          Castlemilk Estate - Glenluwin
        </B>
        <br />
        [37]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Housing_on_Carron_Street_-_geograph.org.uk_-_2129349.jpg">
          Housing Carron Street - Lairich Rig
        </B>
        <br />
        [38] <B to="https://www.youtube.com/watch?v=4mq1Y9dKb6Y">Oil Rigs - Free Stock Footage</B>
        <br />
        [39] <B to="https://commons.wikimedia.org/w/index.php?curid=35475027">2014 Referendum Result Map - Brythones</B>
        <br />
        <h4>FAIR USE</h4>
        <B to="https://www.youtube.com/watch?v=1qlyOo3XRmw">Industrial Disputes - STV News</B>
        <h4>Special Thanks to James Ranson for Audio Mastering Assistance</h4>
      </Credits>
    </>
  );
}
