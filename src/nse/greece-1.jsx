import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseGreece1(props) {
  const headerImage = "greece-1-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="8"
        heading="The Eight Ages Of Greece"
        //subheading="KOPPEN CODE: Af"
        documentTitle="The Eight Ages of Greece - A Complete History"
        headerImageSrc={headerImage}
        youTubeCode="Mk-OyRI7c7Q"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        <h4>BRONZE AGE</h4>
        [1] <B to="https://commons.wikimedia.org/w/index.php?curid=99294843">Knossos Leaping Bull - Jebulon</B>
        <br />
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=11014416">Knossos Dolphin - Armagnac-commons</B>
        <br />
        [3]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=61505445">
          Mycenaean Map - Alexikoua/Panthera tigris tigris/Reedside
        </B>
        <br />
        [4] <B to="https://commons.wikimedia.org/w/index.php?curid=54997901">Mycenae Lion Gate - Andy Hay from UK</B>
        <br />
        [5] <B to="https://commons.wikimedia.org/w/index.php?curid=15165017">Mask of Agamemnon - Xuan Che</B>
        <br />
        [6] <B to="https://commons.wikimedia.org/w/index.php?curid=75700197">Akrotiri Amphorae - Rt44</B>
        <br />
        [7] <B to="https://commons.wikimedia.org/w/index.php?curid=72579943">Akrotiri Excavation - Sidvics</B>
        <br />
        [8]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=29034190">Late Bronze Age Collapse Map - Alexikoua</B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=8647640">Geometric Dark Age Pottery - G.dallorto</B>
        <br />
        <h4>ARCHAIC</h4>
        [1] <B to="https://flic.kr/p/2hetAze">Alphabet Latin Greek Comparison - FotoGuy 49057</B>
        <br />
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=471575">Alphabet Pottery - Marsyas</B>
        <br />
        [3]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Greek_Colonization_Archaic_Period.svg#/media/File:Greek_Colonization_Archaic_Period.svg">
          Greek Colonization - Dipa1965
        </B>
        <br />
        [4] <B to="https://commons.wikimedia.org/wiki/File:Bust_Homer_BM_1825_n2.jpg">Homer Bust - Marie-Lan Nguyen</B>
        <br />
        [5]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Centaurus_for_those_who_have_not_memorised_greek_IAU_credited.png">
          Centaurus - IAU Sky & Telescope Magazine
        </B>
        <br />
        [6]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Oracle_of_Delphi,_red-figure_kylix,_440-430_BC,_Kodros_Painter,_Berlin_F_2538,_141666.jpg">
          Oracle Bowl - Zde
        </B>
        <br />
        [8]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Gold_Solidus_of_Julian_the_Apostate,_Sirmium.jpg">
          Julian The Apostate Coin - ANS
        </B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=15468713">Olympic Vase Runners - Marie-Lan Nguyen</B>
        <br />
        [10]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=4746149">
          Olympic Long Jumper - Bowdoin Painter / Marie-Lan Nguyen
        </B>
        <br />
        [11] <B to="https://www.flickr.com/photos/98231059@N00/5221802184">Olympia Columns - John Karakatsanis</B>
        <br />
        [12] <B to="https://commons.wikimedia.org/w/index.php?curid=3123720">Archaic Sculpture - Tetraktys</B>
        <br />
        [13] <B to="https://commons.wikimedia.org/w/index.php?curid=40890658">Archaic Pottery - Sailko</B>
        <br />
        [14] <B to="https://commons.wikimedia.org/wiki/File:Delos_Museum_3119.jpg">Archaic Pottery - Geraki</B>
        <br />
        [15] <B to="https://commons.wikimedia.org/w/index.php?curid=76643009">Coins Lydia - CNG Coins</B>
        <br />
        [16] <B to="https://commons.wikimedia.org/wiki/File:Hoplites.jpg">Hoplites - Megistias</B>
        <br />
        <h4>CLASSICAL</h4>
        [1]{" "}
        <B to="https://en.wikipedia.org/wiki/Greco-Persian_Wars#/media/File:Achaemenid_Empire_under_different_kings_(flat_map).svg">
          Achaemenid Empire Map - Ali Zifan
        </B>
        <br />
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=6606204">Ionian Revolt Map - Eric Gaba</B>
        <br />
        [3] <B to="https://commons.wikimedia.org/w/index.php?curid=1724898">Graeco-Persian Wars map - Bibi Saint-Pol</B>
        <br />
        [4]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:The_Battle_of_Thermopylae.jpg">
          Thermopylae map - Tilemahos Efthimiadis
        </B>
        <br />
        [5] <B to="https://commons.wikimedia.org/w/index.php?curid=12530054">Peloponnesian War Map - Marsyas</B>
        <br />
        [6] <B to="https://commons.wikimedia.org/w/index.php?curid=1542136">Truce Sculpture- Marcus Cyron</B>
        <br />
        [7] <B to="https://commons.wikimedia.org/w/index.php?curid=2442423">Sculpture Neptune - Ricardo André Frantz</B>
        <br />
        [8]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=2935278">
          Sculpture Family Group - Ricardo André Frantz
        </B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=11817128">Sculpture Bronze Youth - Sailko</B>
        <br />
        [10] <B to="https://commons.wikimedia.org/w/index.php?curid=96296061">Socrates Bust - Sting</B>
        <br />
        [11] <B to="https://commons.wikimedia.org/w/index.php?curid=665222">Ahura Mazda - Ginolerhino</B>
        <br />
        [12]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:The_Ancient_Theatre_of_Sparta_on_May_15,_2019.jpg">
          Sparta Landscape - George E. Koronaios
        </B>
        <br />
        [13] <B to="https://commons.wikimedia.org/w/index.php?curid=105862720">Corinthian War - T8612</B>
        <br />
        <h4>HELLENISTIC</h4>
        [1] <B to="https://commons.wikimedia.org/w/index.php?curid=12530054">Peloponnesian War Map - Marsyas</B>
        <br />
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=59428">Companion Cavalry - Marsyas</B>
        <br />
        [3] <B to="https://commons.wikimedia.org/w/index.php?curid=49876943">Expansion of Macedon map - MaryroseB54</B>
        <br />
        [4] <B to="https://commons.wikimedia.org/w/index.php?curid=9662238">Alexander Cartouche - 1PHGCOM</B>
        <br />
        [5]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Persian_warriors_from_Berlin_Museum.jpg">
          Persian Warriors - mshamma
        </B>
        <br />
        [6] <B to="https://www.flickr.com/photos/24065742@N00/151247206/">Ishtar Gate - Rictor Norton</B>
        <br />
        [7]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=76643009">
          Alexander coins - Classical Numismatic Group, Inc.
        </B>
        <br />
        [8]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=61818665">
          Perdiccas Coin - Classical Numismatic Group, Inc.
        </B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=61818665">Diadochi Map - Diadochi PT et al</B>
        <br />
        [10] <B to="https://commons.wikimedia.org/w/index.php?curid=4171689">Rome Seleucia Parthia map - Talessman</B>
        <br />
        [11] <B to="https://commons.wikimedia.org/wiki/File:Rosetta_Stone.JPG">Rosetta Stone - Hans Hillewaert</B>
        <br />
        [12]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=52681377">
          Eratosthenes Earth Measurement - cmglee, David Monniaux, jimht
        </B>
        <br />
        [13]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Seven_Wonders_of_the_Ancient_World.png">
          Seven Wonders Map - kandi
        </B>
        <br />
        <h4>ROMAN</h4>
        [1] <B to="https://commons.wikimedia.org/w/index.php?curid=47345812">Last day of Corinth - Sailko</B>
        <br />
        [2]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=99570181">
          2nd Roman–Macedonian War map - Pasztilla, Marsyas, Willyboy, Amphipolis
        </B>
        <br />
        [3] <B to="https://commons.wikimedia.org/w/index.php?curid=2992630">Roman SPQR banner - Ssolbergj</B>
        <br />
        [5]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=80631904">
          Sulla coin - Classical Numismatic Group, Inc.
        </B>
        <br />
        [6] <B to="https://commons.wikimedia.org/w/index.php?curid=5505254">Actium Map - Future Perfect at Sunrise</B>
        <br />
        [7] <B to="https://commons.wikimedia.org/wiki/File:Roman_school.jpg">Roman School - Shakko</B>
        <br />
        [8]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:The_Temple_of_Zeus_in_Athens_on_17_February_2019.jpg">
          Temple of Zeus in Athens - George E. Koronaios
        </B>
        <br />
        [9]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=91509763">Constantine's head sculpture - Camille King</B>
        <br />
        [10]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=5084599">Byzantine Constantinople Map - Cplakidas</B>
        <br />
        [11]{" "}
        <B to="https://en.wikipedia.org/wiki/Later_Roman_Empire#/media/File:Roman-empire-395AD.svg">
          Late Roman Empire map - AKIKA3D
        </B>
        <br />
        [12]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Invasions_of_the_Roman_Empire.svg">
          Invasions of the Roman Empire Map - MapMaster
        </B>
        <br />
        [13]{" "}
        <B to="https://upload.wikimedia.org/wikipedia/commons/7/76/Gothic_tribes_early.jpg">
          Early Goths - Therontherod
        </B>
        <br />
        [14]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Mosaic_of_Justinianus_I_-_Basilica_San_Vitale_(Ravenna).jpg">
          Justinian mosaic - Petar Milošević
        </B>
        <br />
        <h4>BYZANTINE</h4>
        [1]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Great_Schism_1054_with_former_borders-.png">
          Great Schism map - Tobi85
        </B>
        <br />
        [2]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:The_Byzantine_eagle._Coat_of_arms_of_the_Palaiologos_Dynasty(TR).png">
          Byzantine Eagle - DPG*o0W0o
        </B>
        <br />
        [3]{" "}
        <B to="https://en.wikipedia.org/wiki/Byzantine_Empire#/media/File:4KBYZ.gif">
          Reduction of territory animation - NelmWiki
        </B>
        <br />
        [4] <B to="https://commons.wikimedia.org/w/index.php?curid=1470982">Latin Empire Map - LatinEmpire/Varana</B>
        <br />
        [5] <B to="https://commons.wikimedia.org/w/index.php?curid=92652369">Greek scroll - G. dallorto</B>
        <br />
        <h4>OTTOMAN</h4>
        [1]{" "}
        <B to="https://en.wikipedia.org/wiki/Ottoman_Greece#/media/File:Emp%C3%A8ri_Otoman_-_Expansion_territ%C3%B2riala_de_1307_a_1683.png">
          Ottoman Expansion Map - Nicolas Eynaud
        </B>
        <br />
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=75373598">Samos Harbour - Rawpixel</B>
        <br />
        [3] <B to="https://commons.wikimedia.org/w/index.php?curid=15931807">1821 Map - Various</B>
        <br />
        <h4>MODERN</h4>
        [1]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=92652369">Greek Diaspora Map - Allice Hunter et al</B>
        <br />
        [2]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=1868273">Territorial Gains from 1832 - Historicair</B>
        <br />
        [3]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=79796185">
          The persuading of Tino (Punch 1916) - John Bernard Partridge
        </B>
        <br />
        [4] <B to="https://commons.wikimedia.org/w/index.php?curid=93535045">Territorial Claims map - LegionaryIX</B>
        <br />
        [5] <B to="https://commons.wikimedia.org/w/index.php?curid=30659140">Greco-Turkish War map - Alexikoua</B>
        <br />
        [6] <B to="https://commons.wikimedia.org/w/index.php?curid=44254980">World War II Map - Eric Gaba</B>
        <br />
        [7] <B to="https://commons.wikimedia.org/w/index.php?curid=3742461">WW2 Occupation Map - Cplakidas</B>
        <br />
        [8] <B to="https://commons.wikimedia.org/w/index.php?curid=5476326">Nazi Flag Acropolis - Bundesarchiv, Bild</B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=3819566">Partisans - Revizionist</B>
        <br />
        [10] <B to="https://commons.wikimedia.org/w/index.php?curid=26282550">WW2 End - Υπουργείο Εξωτερικών</B>
        <br />
        [11] <B to="https://athens.indymedia.org/post/1543277/">Colonels - Unknown</B>
        <br />
        [12] <B to="https://commons.wikimedia.org/w/index.php?curid=32033378">Cell door - ΒΑΣΙΛΗΣ ΓΚΟΙΜΗΣΗΣ</B>
        <br />
        [13] <B to="https://commons.wikimedia.org/w/index.php?curid=20690822">Greek debt - Spitzl</B>
        <br />
        [14] <B to="https://commons.wikimedia.org/w/index.php?curid=19761879">2011 Greek uprising - Kotsolis</B>
        <br />
        [15] <B to="https://commons.wikimedia.org/w/index.php?curid=5576651">Riots in Athens - Panayotis Vryonis</B>
        <br />
        [16]{" "}
        <B to="https://www.flickr.com/photos/26040773@N07/5758834027/in/photostream/">Syntagma Square - linmtheu</B>
        <br />
      </Credits>
    </>
  );
}
