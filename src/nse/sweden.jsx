import React from "react";
import Chapter from "../components/chapter";
import B from "../components/B";
import Credits from "../components/credits";

export default function NseSweden(props) {
  const headerImage = "sweden-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="1"
        heading="Sweden"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Sweden - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="pU2OvWSJySQ"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        <p>
          [1] <B to="https://bit.ly/3hiM3cm">Roman Empire in AD125</B> Andrein
        </p>
        <p>
          [2] <B to="https://bit.ly/2E1TM0t">Viking artefact (Helmet)</B> Mararie
        </p>
        <p>
          [3] <B to="https://bit.ly/2BeHd0H">Viking ship head</B> Balou46
        </p>
        <p>
          [4] <B to="https://bit.ly/398R8Bt">Longship</B> Karamell
        </p>
        <p>
          [5] <B to="https://bit.ly/32Ah1cc">Viking Voyages</B> Bogdangiusca
        </p>
        <p>
          [6] <B to="https://bit.ly/2CPjfcR">Sweden 1250AD Map</B> P.S.Burton
        </p>
        <p>
          [7] <B to="https://bit.ly/3hh3w5i">Are Old Church</B> Philaweb
        </p>
        <p>
          [8] <B to="https://bit.ly/398xrtw"> Kalmar Union Map</B> NordNordWest
        </p>
        <p>
          [9] <B to="https://bit.ly/3eN5Fns">Hanseatic League Map</B> Droysen/Andrée
        </p>
        <p>
          [10] <B to="https://bit.ly/32AKtyC">Swedish Empire Map</B> Memnon335bc/M.K.
        </p>
        <p>
          [11] <B to="https://bit.ly/30tX6sq">EU Parliament</B> David Iliff
        </p>
        <p>
          [12] <B to="https://flic.kr/p/ZLAQ9U">UN Ambassador</B> UN Women
        </p>
        <p>
          [13] <B to="https://youtu.be/DHR4V964vEw">King Carl XVI Gustaf</B> ScoutsMessengers
        </p>
        <p>
          [14] <B to="https://flic.kr/p/2cQkQex">Swedish Parliament</B> Hans Permana
        </p>
        <p>
          [15] <B to="https://flic.kr/p/53UU5h">Swedish parliament</B> Wordshore
        </p>
        <p>
          [16] <B to="https://bit.ly/2ZFWIrU">Riksdag seat distribution 1902-2018</B> Tapyrrll
        </p>
        <p>
          [18] <B to="https://bit.ly/2OFE11i">Tundra</B> Matthias Grallert
        </p>
        <p>
          [19] <B to="https://youtu.be/WJX0QbaFGXI">Skane Countryside</B> gattovi
        </p>
        <p>
          [18-19] <B to="https://bit.ly/39a6CVE">Koppen Climate Zones</B> Adam Peterson
        </p>
        <p>
          [20] <B to="https://bit.ly/39clC5F">Blondes Map</B> u/kokkenrole
        </p>
        <p>
          [21] <B to="https://bit.ly/30oCfHf">Coat of Arms</B> Sodacan
        </p>
        <p>
          [22] <B to="https://bit.ly/30pSHae">Krona coin</B> Scanigen
        </p>
        <p></p>
        <h4>Sections of the following reproduced under Fair Use:</h4>
        <p>
          [17] <B to="https://youtu.be/SLs4rQM3GBw">Swedish election news</B> CNBC International TV
        </p>
        <p>
          [23] <B to="https://oec.world/en/profile/country/swe">Exports tree graph</B> Observatory of Economic
          Complexity
        </p>
        <p>
          [24] <B to="https://bit.ly/3eIjcww">Forbes Magazine</B>
        </p>
        <p>
          [25] <B to="https://www.cato.org/human-freedom-index-new">Cato Institute</B>
        </p>
        <p>
          [26] <B to="https://bit.ly/39n2nXb">Nobel Prize Ceremony</B>
        </p>
        <p></p>
      </Credits>
    </>
  );
}
