import React, { useEffect } from "react";
import NavPanelThumb from "../components/nav-panel-thumb";
import Share from "../components/share";

export default function Nse(props) {
  useEffect(() => {
    document.title = "Nation States Of Earth";
    document
      .querySelector('meta[name="description"]')
      .setAttribute(
        "content",
        "A series on the nations of the world, including their geography, history, culture, and more."
      );
  }, []);

  const ImageNse1 = "Thumbnail-NSE1.jpg";
  const ImageNse2 = "Thumbnail-NSE2.jpg";
  const ImageNse3 = "Thumbnail-NSE3.jpg";
  const ImageNse4 = "Thumbnail-NSE4.jpg";
  const ImageNse5 = "Thumbnail-NSE5.jpg";
  const ImageNse6 = "Thumbnail-NSE6.jpg";
  const ImageNse7 = "Thumbnail-NSE7.jpg";
  const ImageNse8 = "Thumbnail-NSE8.jpg";
  const ImageNse9 = "Thumbnail-NSE9.jpg";
  const ImageNse10 = "Thumbnail-NSE10.jpg";
  const ImageNse11 = "Thumbnail-NSE11.jpg";
  const ImageNse11b = "Thumbnail-NSE11b.jpg";
  const ImageNse12a = "Thumbnail-NSE12a.jpg";
  const ImageNse12b = "Thumbnail-NSE12b.jpg";

  return (
    <>
      <div className="title-block">
        <div className="title">Nation States of Earth</div>
        <div className="summary">
          A series presenting the history, geography, culture, and more of the nations of the world.
        </div>
        <div className="chapters-heading">Chapters</div>
      </div>
      <div className="chapters-list">
        <NavPanelThumb src={ImageNse1} to="/nse/sweden" title="" subtitle="" />
        <NavPanelThumb src={ImageNse2} to="/nse/japan" title="" subtitle="" />
        <NavPanelThumb src={ImageNse3} to="/nse/iran" title="" subtitle="" />
        <NavPanelThumb src={ImageNse4} to="/nse/brazil" title="" subtitle="" />
        <NavPanelThumb src={ImageNse5} to="/nse/philippines" title="" subtitle="" />
        <NavPanelThumb src={ImageNse6} to="/nse/russia" title="" subtitle="" />
        <NavPanelThumb src={ImageNse7} to="/nse/south-africa" title="" subtitle="" />
        <NavPanelThumb src={ImageNse8} to="/nse/greece-1" title="" subtitle="" />
        <NavPanelThumb src={ImageNse9} to="/nse/greece-2" title="" subtitle="" />
        <NavPanelThumb src={ImageNse10} to="/nse/chile" title="" subtitle="" />
        <NavPanelThumb src={ImageNse11} to="/nse/scotland-1" title="" subtitle="" />
        <NavPanelThumb src={ImageNse11b} to="/nse/scotland-2" title="" subtitle="" />
        <NavPanelThumb src={ImageNse12a} to="/nse/nz-1" title="" subtitle="" />
        <NavPanelThumb src={ImageNse12b} to="/nse/nz-2" title="" subtitle="" />
      </div>
      <div className="center-cont">
        <p>&nbsp;</p>
        <p>
          <b>Prefer to watch the series as a playlist on Youtube? Then</b>{" "}
          <a
            href="https://www.youtube.com/playlist?list=PLu83ZzwRbQvIC0WCjOGwya_A5b4MKCh_C"
            target="_blank"
            rel="noreferrer"
          >
            click here!
          </a>
        </p>
        <p>&nbsp;</p>
      </div>
      <Share />
    </>
  );
}
