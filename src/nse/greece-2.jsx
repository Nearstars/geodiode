import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseGreece2(props) {
  const headerImage = "greece-2-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="9"
        heading="Greece"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Greece - Its Geography, Economy and Culture"
        headerImageSrc={headerImage}
        youTubeCode="RtNruJYaHHo"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [1] <B to="https://www.freeworldmaps.net/europe/greece/map.html">Physical Map - Free World Maps</B>
        <br />
        [2] <B to="https://www.flickr.com/photos/stg_gr1/226957878/">Mt Olympus - stefg74</B>
        <br />
        [3] <B to="https://commons.wikimedia.org/wiki/File:Samothrace_South_Coast.jpg">Samothrace - DocWoKav</B>
        <br />
        [4] <B to="https://www.flickr.com/photos/132646954@N02/51695986703/">Skiathos - dronepicr</B>
        <br />
        [5] <B to="https://www.flickr.com/photos/132646954@N02/51698566526/">Rhodes - dronepicr</B>
        <br />
        [6]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=16042996">Administrative Regions Map - Philly boy92</B>
        <br />
        [7] <B to="https://www.flickr.com/photos/32629637@N08/4011844038">Greek Parliament Interior - PASOK</B>
        <br />
        [8] <B to="https://commons.wikimedia.org/w/index.php?curid=4124028">Leopard Tank - Konstantinos Stampoulis</B>
        <br />
        [9] <B to="https://commons.wikimedia.org/w/index.php?curid=22426640">Navy Frigate - DVIDSHUB (CC BY 2.0)</B>
        <br />
        [10] <B to="https://bit.ly/3gm7O0m">Epitapth of Seikilos - Marcus Harvey</B>
        <br />
        [11] <B to="https://commons.wikimedia.org/w/index.php?curid=76461885">Koppen Climate Map - Philly boy92</B>
        <br />
        [12] <B to="https://popdensitymap.ucoz.ru/news/2021-02-21">Population Density Map - alex</B>
        <br />
        [13]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=107699398">
          Indo European Languages Map - Bill WIlliams
        </B>
        <br />
        [14] <B to="https://www.facebook.com/ergazomenoi.enae">Greek Shipyards - ENAE</B>
        <br />
        [15]{" "}
        <B to="https://www.charterworld.com/index.html?sub=yacht-charter&charter=issham-al-baher-1876">
          Luxury Yacht - Charter World
        </B>
        <br />
        [16] <B to="https://commons.wikimedia.org/wiki/File:Mousakas.jpg">Moussaka - Dieter Mueller</B>
        <br />
        [17]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Tzatziki_meze_or_appetizer,_also_used_as_a_sauce.jpg">
          Tsatsiki - Nikodem Nijaki
        </B>
        <br />
        [18] <B to="https://commons.wikimedia.org/wiki/File:Gatimetradicionale.jpg">Spanakopita - Gatimetradicionale</B>
        <br />
        [19] <B to="https://commons.wikimedia.org/wiki/File:Kalamataolives.jpg">Kalamata Olives - Michael Fielitz</B>
        <br />
        [20] <B to="https://commons.wikimedia.org/wiki/File:Seikilos2.tif">Seikilos Stele - Nationalmuseet</B>
        <br />
        [21]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Seikilos_epitaph_(variation).png">
          Seikilos Sheet Music - Kanjuzi
        </B>
        <br />
        [22]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Vangelis,_26_July_2012.jpg">
          Vangelis Photo 2012 - Ian Patterson
        </B>
        <br />
      </Credits>
    </>
  );
}
