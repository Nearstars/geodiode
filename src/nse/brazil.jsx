import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseBrazil(props) {
  const headerImage = "brazil-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="4"
        heading="Brazil"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Brazil - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="0RSpnpm8X4I"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>SPECIAL THANKS to the following channels, whose Creative Commons content made this video possible:</h4>
        <p>[1] <B to="https://youtu.be/JFpragDkxos"> Pantanal</B> toymituable</p>
        <p>[2] <B to="https://youtu.be/KTpZipdWgBs"> Savannah: Chapada Dos Veadeiros</B> Ricardo Madeira de Albuquerque</p>
        <p>[3] <B to="https://youtu.be/ZDEKF1iSALk"> Salvador, Bahía</B> Marcio Santos Oliveira</p>
        <p>[4] <B to="https://youtu.be/pxIbKerSAds"> Ibirapuera Park, São Paulo</B> Fabiano Garcia Silvestrin </p>
        <p>[5] <B to="https://youtu.be/c2LMAiaFkIE"> Mato Grosso</B> Kililla</p>
        <p>[6] <B to="https://youtu.be/0MYsZDqFISw"> Gramado, Rio Grande do Sul</B> TRAVEL PINGUIM</p>
        <p>[7] <B to="https://youtu.be/z9mh-zFmOHU"> Vitória, Espírito Santo</B> Rodrigo Varejão</p>
        <p>[8] <B to="https://youtu.be/yKY98-i4DSc"> Ouro Preto (Church, Town)</B> Flyerbee Imagens Aéreas com Drone</p>
        <p>[9] <B to="https://youtu.be/JadF08TlVaY"> Belo Horizonte, Minas Gerais</B> Rodrigo Varejão</p>
        <p>[10] <B to="https://youtu.be/a5KbhQAmYZk"> Canela, Rio Grande do Sul</B> TRAVEL PINGUIM</p>
        <p>[11] <B to="https://youtu.be/L2FgpnCFAjI"> Bombinhas, Santa Catarina</B> Marcelo Fraga</p>
        <p>[12] <B to="https://youtu.be/dSLSCMhkBbE"> Embraer airliner</B> Navgeek Aviation</p>
        <p>[13] <B to="https://youtu.be/oIe_SGA6IkA"> Carnaval</B> Filmowe relacje z wycieczek</p>
        <p>[14] <B to="https://youtu.be/sSRMovbtkN0"> Football</B> Real Madrid</p>
        <p>[15] <B to="https://youtu.be/cU1LqVlEXlA"> São Luis, Maranhão</B> Renan Matheus</p>
        <p>[21] <B to="https://youtu.be/4VnXf7ywHXo"> Ouro Preto</B> Pretty Fly Drone</p>
        <p>[24] <B to="https://youtu.be/SGG6e_UeLUo"> Construction of Brasilia</B> Kadumago History and Archeology </p>
        <p>[30] <B to="https://youtu.be/QVOqXet-cAU"> Vitória, Espírito Santo</B> Rodrigo Varejão</p>
        <p>[31] <B to="https://youtu.be/ykE-c--guLg"> Amazon Forest</B> Center for International Forestry Research (CIFOR)</p>
        <p>[38] <B to="https://youtu.be/y0tosWhDfXQ"> Mata Atlántica</B> O Agro Nosso (Forests of the SE Brazil)</p>
        <p>[39] <B to="https://youtu.be/CTo0WMg41fk"> Canela, Rio Grande do Sul</B> Marcelo Fraga (Forests of SE Brazil)</p>
        <p>[40] <B to="https://youtu.be/-E_KxvNkF8g"> Mato Grosso do Sul/ São Paulo</B> SharkDrones</p>
        <p>[41] <B to="https://youtu.be/KlU9zuJ3L6w"> Caatinga Scrub</B> Terra Negra</p>
        <p>[43] <B to="https://youtu.be/2ct9q259mFA"> Río de Janeiro</B> El Gringo Rojo</p>
        <p>[45] <B to="https://youtu.be/7-KcbVcMoFU"> Brasilia</B> 27 Giros</p>
        <p>[51] <B to="https://youtu.be/xHB36paAeIo"> Guarapari, Espírito Santo</B> Rodrigo Varejão </p>
        <p>[56] <B to="https://youtu.be/VtM9Doxw878"> Coffee</B> Genuine Origin</p>
        <p>[57] <B to="https://youtu.be/kQYnL44p_ik"> Mato Grosso (Large Dam)</B> Kililla </p>
        <p>[61] <B to="https://youtu.be/HqTv4OveqcI"> Aviation Industry</B> TV USP Piracicaba</p>
        <p>[72] <B to="https://youtu.be/A_dMmwhDuow"> São Paulo</B> Overvalued</p>
        <p></p>
        <h4>Sections of the following reproduced under Fair Use:</h4>
        <p>[37] <B to="https://www.cato.org/human-freedom-index-new">Cato Institute</B></p>
        <p>[55] <B to="https://oec.world/">OEC graph</B></p>
        <p>[60] Car Factory and Vintage Beetle <B to="https://www.youtube.com/volkswagendobrasil">Volkswagen do Brazil</B> </p>
        <p>[64] Canção_do_Amor_Demais - Festa (Label), BlackOrpheus Poster - Lopert Pictures </p>
        <p>[71] FIFA World Cup Winners - <B to="https://footyroom.co/">FootyRoom.com</B></p>
        <p>&nbsp;</p>
        <p>
          Regrettably, the records for the remaining Creative Commons credits were lost during a transfer of channel status. If you feel
          your property has been used in this video and not been given credit, then please contact me via my <B to="https://www.youtube.com/channel/UC1raaXFgsFBSFR8qNgchF2g/about">Channel "About" page</B>,
          referencing the number in the bottom left of the screen.
        </p>
      </Credits>
    </>
  );
}
