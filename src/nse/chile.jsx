import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseChile(props) {
  const headerImage = "chile-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="10"
        heading="Chile"
        //subheading="KOPPEN CODE: Af"
        documentTitle="Chile - Its history, geography, economy and culture"
        headerImageSrc={headerImage}
        youTubeCode="gGfBc4N86mA"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [1] <B to="https://commons.wikimedia.org/w/index.php?curid=67285278">Inca Empire Map - QQuantum</B>
        <br />
        [2] <B to="https://commons.wikimedia.org/w/index.php?curid=127351984">Atacama dispute - Janitoalevic</B>
        <br />
        [3]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=28121460">
          German Architecture - Yaneth Barrientos Cárcamo
        </B>
        <br />
        [4] <B to="https://commons.wikimedia.org/w/index.php?curid=51267452">Salt Petre Mine - Diego Delso</B>
        <br />
        [5] <B to="https://commons.wikimedia.org/w/index.php?curid=80171324">Basque Park - Igallards7</B>
        <br />
        [0]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=11201622">
          Allende Train - Biblioteca del Congreso Nacional de Chile
        </B>
        <br />
        [0]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=16325518">
          Allende Portrait - Biblioteca del Congreso Nacional de Chile
        </B>
        <br />
        [0] 1973 Coup Bombing - By Biblioteca del Congreso Nacional, CC BY 3.0 cl
        <br />
        [6]{" "}
        <B to="https://www.imdb.com/title/tt4393514/">2014 Maidan - Ukraine on Fire - Oliver Stone / Igor Lopotonok</B>
        <br />
        [7]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=20138288">
          The Disappeared - Kena Lorenzini - Museum of Memory and Human Rights, CC BY-SA 3.0
        </B>
        <br />
        [8]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=57041714">
          Photos of regime victims - CARLOS TEIXIDOR CADENAS
        </B>
        <br />
        [0]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=26016126">
          Pinochet hands over power - Biblioteca del Congreso Nacional de Chile
        </B>
        <br />
        [9]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=11802709">
          Five presidents - Gobierno de Chile - Izamiento de la Gran Bandera Nacional
        </B>
        <br />
        [10] <B to="https://commons.wikimedia.org/w/index.php?curid=83460148">2019 Protests - Hugo Morales</B>
        <br />
        [11] <B to="https://commons.wikimedia.org/w/index.php?curid=11930321">Congress building exterior - ffuentes</B>
        <br />
        [0]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=20785261">
          Congress building chamber - Biblioteca del Congreso Nacional de Chile
        </B>
        <br />
        [12]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=18573247">
          Presidential Palace - Ministerio Secretaria General de Gobierno
        </B>
        <br />
        [13]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=72018260">Chile regions map - Karte: NordNordWest</B>
        <br />
        [14]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Tectonic_plates_boundaries_World_map_Wt_180degE_centered-en.svg">
          Plate tectonics map - Eric Gaba
        </B>
        <br />
        [15]{" "}
        <B to="https://www.researchgate.net/figure/Map-of-Chile-Twelve-of-the-most-destructive-earthquakes-in-the-history-of-Chile_fig1_320746757">
          Earthquakes map - Ruedi et al
        </B>
        <br />
        [16]{" "}
        <B to="https://www.researchgate.net/figure/Map-of-southern-Chile-and-Argentina-showing-the-volcanoes-of-the-Andean-Southern-Volcanic_fig3_260558576">
          Volcanoes map - Stern, Siebert et al
        </B>
        <br />
        [17] <B to="">Koppen Map - Beck et al</B>
        <br />
        [18]{" "}
        <B to="https://www.researchgate.net/publication/349959779_Temporal_evolution_in_social_vulnerability_to_natural_hazards_in_Chile">
          Pop Density Map - Bronfman et al
        </B>
        <br />
        [19] <B to="https://commons.wikimedia.org/w/index.php?curid=80171324">Concepcion - Kabelleger / David Gubler</B>
        <br />
        [20] <B to="https://commons.wikimedia.org/w/index.php?curid=7142304">Spanish Dialects map - Hidra92</B>
        <br />
        [21] <B to="https://flic.kr/p/8AujKT">Mapuche Woman - Raul Urzua de la Sotta</B>
        <br />
        [22] <B to="">Copper mine - Milko Manfred Rupcich Rolack / K Shino</B>
        <br />
        [23]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=26016126">
          Chilean Mine Rescue - Hugo Infante / Government of Chile
        </B>
        <br />
        [24] <B to="https://commons.wikimedia.org/w/index.php?curid=26016126">Fishing Boat - Christopher Michel</B>
        <br />
        [0]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Pablo_Neruda_-_BCN.jpg">
          Pablo Neruda - Biblioteca del Congreso Nacional de Chile
        </B>
        <br />
        [25] <B to="https://flic.kr/p/cmKbXy">Jose Donoso - Elise Cabot</B>
        <br />
        [27]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Pedro_Pascal_by_Gage_Skidmore.jpg">
          Pablo Pascal - Gage Skidmore
        </B>
        <br />
        <h4>FAIR USE</h4>
        <p>
          [26] <B to="https://youtu.be/m0cJNR8HEw0">Jodorowsky's Dune Trailer - Sony Pictures Classics</B>
        </p>
      </Credits>
    </>
  );
}
