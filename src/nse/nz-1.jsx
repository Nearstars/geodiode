import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function NseNz1(props) {
  const headerImage = "nz-1-hero.jpg";

  return (
    <>
      <Chapter
        series="nse"
        seriesChapter="13"
        heading="The History of New Zealand"
        //subheading="KOPPEN CODE: Af"
        documentTitle="The History of New Zealand"
        headerImageSrc={headerImage}
        youTubeCode="wOhCI5m2jEY"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>
      <Credits>
        <p>(Referenced by number in the video, bottom left)</p>
        <p></p>
        <h4>A BIG THANK YOU TO THESE CARING AND SHARING FOLKS IN THE CREATIVE COMMONS:</h4>
        [10] <B to="https://commons.wikimedia.org/w/index.php?curid=4503992">Polynesian Migration Map - David Eccles</B>
        <br />
        [11] <B to="https://commons.wikimedia.org/w/index.php?curid=15332701">Kupe Statue - Heather Cuthill</B>
        <br />
        [12] <B to="https://commons.wikimedia.org/wiki/File:Maori_Waka_at_Waitangi.jpg">Maori Waka - Dirk Pons</B>
        <br />
        [13]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Canterbury_Museum,_Christchurch_-_Joy_of_Museums_-_Maori_Pare_-_Lintel_from_a_Maori_Meeting_House.jpg">
          Maori Pare - GordonMakryllos
        </B>
        <br />
        [14]
        <B to="https://commons.wikimedia.org/wiki/File:New_Zealand_Maori_Culture_002_(5397152967).jpg">
          Maori Man with Spear - Steve Evans
        </B>
        <br />
        [15] <B to="https://commons.wikimedia.org/w/index.php?curid=9434486">Mission House - Winstonwolfe</B>
        <br />
        [16] <B to="https://commons.wikimedia.org/w/index.php?curid=112272295">Auckland Map 1841 - Auckland Museum</B>
        <br />
        [17] <B to="https://commons.wikimedia.org/w/index.php?curid=4319607">Maori Meeting House - James Shook</B>
        <br />
        [18]{" "}
        <B to="https://commons.wikimedia.org/wiki/File:Lone_Pine_Cemetery_2013.07.26.jpg">
          Gallipoli Cemetery - Gary Blakeley
        </B>
        <br />
        [19]{" "}
        <B to="https://nzhistory.govt.nz/media/photo/anti-vietnam-war-protest-parliament">
          Vietnam War Protest - Alexander Turnbull Library
        </B>
        <br />
        [20]{" "}
        <B to="https://nzhistory.govt.nz/media/photo/1930s-protest-outside-parliament">
          1930s Protest - Alexander Turnbull Library
        </B>
        <br />
        [21]{" "}
        <B to="https://teara.govt.nz/en/photograph/21540/michael-joseph-savage">
          1939 Opening of Social Security Building - Alexander Turnbull Library
        </B>
        <br />
        [22] 1960s Working Maoris - Riethmaier
        <br />
        [23]<B to="https://commons.wikimedia.org/wiki/File:Penari_Maori_1986.jpg">Penari Maori Singer - Malekhanif</B>
        <br />
        [24]
        <B to="https://commons.wikimedia.org/w/index.php?curid=10884764">Whina Cooper Protest - By Christian Heinegg</B>
        <br />
        [25] <B to="https://commons.wikimedia.org/w/index.php?curid=86107470">UK EEC Map - Kolja21</B>
        <br />
        [26]
        <B to="https://commons.wikimedia.org/wiki/File:463_Okarito_New_Zealand_1970_(51213606780).jpg">
          Farm 1970 - Wilford Peloquin
        </B>
        <br />
        [27]
        <B to="https://commons.wikimedia.org/wiki/File:Somewhere_in_Auckland,_late_1970s..._(7493109940).jpg">
          Auckland Motorway 1980 - Riley
        </B>
        <br />
        [28]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=47608235">David Lange - Horowhenua Historical Society</B>
        <br />
        [29] <B to="https://commons.wikimedia.org/w/index.php?curid=45168960">Rainbow Warrior - Ot</B>
        <br />
        [30]{" "}
        <B to="https://commons.wikimedia.org/w/index.php?curid=56957024">
          NZ Regional Councils and Territorial Authorities 2017 - Korakys
        </B>
        <br />
      </Credits>
    </>
  );
}
