import React from "react";
import Chapter from "../components/chapter";

import B from "../components/B";
import Credits from "../components/credits";

export default function IgNile(props) {
  const headerImage = "nile-hero.jpg";

  return (
    <>
      <Chapter
        series="ig"
        seriesChapter="3"
        heading="The Nile"
        //subheading="KOPPEN CODE: Af"
        documentTitle="The 2,000 year search for the source of the Nile"
        headerImageSrc={headerImage}
        youTubeCode="6gz3b5Q45R8"
        // chapterPrevText="Introduction"
        // chapterPrevUrl="/climate/secrets-of-world-climate-introduction"
        // chapterNextText="Tropical Monsoon &amp; Savannah"
        // chapterNextUrl="/climate/tropical-monsoon-and-tropical-savannah"
      ></Chapter>

      <Credits>
        <h4>
          EXTRA SPECIAL THANKS to the following channels, whose Creative Commons content made this video possible:
        </h4>
        <B to="https://en.wikipedia.org/wiki/Nile#/media/File:Nile_basin_map.png">Nile Basin Map - Shannon1</B>
        <br />
        <B to="https://www.researchgate.net/figure/The-Nile-Rivers-hydrograph-showing-seasonal-variability-in-the-contributions-from_fig1_327942024">
          Nile Hydrograph - Solomon G Gebr/ehiwot
        </B>
        <br />
        <B to="https://www.aramcoworld.com/Articles/March-2018/Khartoum-A-Tale-of-Two-Rivers">
          Blue While Nile Confluence Khartoum - AramcoWorld
        </B>
        <br />
        <B to="https://commons.wikimedia.org/w/index.php?curid=9378702">Egypt Population Density Map - Giorgiogp2</B>
        <br />
        <B to="https://commons.wikimedia.org/w/index.php?curid=55229660">3rd Cataract - Clemens Schmillen</B>
        <br />
        Ruvubu River Confluence - SteveRwanda
        <br />
        <B to="https://commons.wikimedia.org/wiki/File:Nubia-_2008-_Piramidi_Khartoum.jpg">Pyramids of Meroë - COSV</B>
        <br />
        <B to="https://commons.wikimedia.org/w/index.php?curid=99457464">
          Nilus The River God Statue - Carole Raddato/ Vatican Museums
        </B>
        <br />
        <B to="https://commons.wikimedia.org/w/index.php?curid=81143164">Nero as Pharoah - Rama</B>
        <br />
        <B to="https://commons.wikimedia.org/wiki/File:Royal_Geographical_Society_Circlet.png">
          Royal Geographical Society Circlet - AlexD
        </B>
        <br />
        <B to="https://www.africangreatlakesinform.org/article/lake-victoria">
          Lake Victoria Basin - Evans A.K. Miriti
        </B>
        <br />
        <B to="https://flic.kr/p/2m7jj5o">Grand Ethiopian Renaissance Dam - Ana E Cascao</B>
        <br />
        <B to="https://www.researchgate.net/publication/276414381_Development_of_Nile_River_islands_between_Old_Aswan_Dam_and_new_Esna_barrages">
          Flowrate Before/After Aswan Dam - Yasser M. Raslan / Radwa Salama
        </B>
        <br />
      </Credits>
    </>
  );
}
