import React, { useEffect } from "react";
import NavPanelThumb from "../components/nav-panel-thumb";
import Share from "../components/share";

export default function Ig(props) {
  useEffect(() => {
    document.title = "Infamous Geography";
    document
      .querySelector('meta[name="description"]')
      .setAttribute("content", "A series on the collision of history and geography.");
  }, []);

  const ImageIg1 = "Thumbnail-IG1.jpg";
  const ImageIg2 = "Thumbnail-IG2.jpg";
  const ImageIg3 = "Thumbnail-IG3.jpg";

  return (
    <>
      <div className="title-block">
        <div className="title">Infamous Geography</div>
        <div className="summary">
          A series presenting the history, geography, culture, and more of the nations of the world.
        </div>
        <div className="chapters-heading">Chapters</div>
      </div>
      <div className="chapters-list">
        <NavPanelThumb src={ImageIg1} to="/ig/north-west-passage" title="" subtitle="" />
        <NavPanelThumb src={ImageIg2} to="/ig/krakatoa" title="" subtitle="" />
        <NavPanelThumb src={ImageIg3} to="/ig/nile" title="" subtitle="" />
      </div>
      <div className="center-cont">
        <p>&nbsp;</p>
        <p>
          <b>Prefer to watch the series as a playlist on Youtube? Then</b>{" "}
          <a
            href="https://www.youtube.com/playlist?list=PLu83ZzwRbQvLi4JgRceH3zZHICYlOOrDE"
            target="_blank"
            rel="noreferrer"
          >
            click here!
          </a>
        </p>
        <p>&nbsp;</p>
      </div>
      <Share />
    </>
  );
}
