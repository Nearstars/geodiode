import React from "react";

export default function Footer(props) {
  const year = new Date().getFullYear();

  return <div className="footer">©{year} Geodiode, B.J.Ranson</div>;
}
