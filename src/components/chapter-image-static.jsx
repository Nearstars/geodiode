import React, { useState } from "react";

export default function ChapterImageStatic(props) {
  const [isPoppedUp, setIsPoppedUp] = useState(false);

  let className = "chapter-image-cont";
  if (props.right) className += " float-right";
  else if (props.left) className += " float-left";

  return (
    <>
      <div className={className} onClick={() => setIsPoppedUp(true)}>
        <img className="chapter-image" src={props.src} alt={props.caption} />
        <div className="caption">{props.caption}</div>
      </div>
      {isPoppedUp && (
        <div className="image-popup" onClick={() => setIsPoppedUp(false)}>
          <img src={props.src} alt={props.caption} />
          <div className="caption">{props.caption}</div>
        </div>
      )}
    </>
  );
}

