import React from "react";

export default function Support(props) {
  return (
    <div className="support center-cont">
      <h2>Please help support the future development of this website, and of my YouTube channel</h2>
      <p>You can become a member of the channel - just click the "JOIN" button below any video</p>
    </div>
  );
}
