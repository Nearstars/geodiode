import React from "react";
import { Link } from "react-router-dom";

export default function NavPanelFull(props) {
  return (
    <Link to={props.to}>
      {/* <video autoPlay="autoPlay" loop preload="metadata" muted>
        <source src={props.src} type="video/mp4" />
        Your browser is not supported!
      </video> */}

      <img src={`${process.env.REACT_APP_IMAGES_DIR}/thumbs/${props.src}`} alt={props.title} />

      <div className="title-block">
        <div className="title">{props.title}</div>
        <div className="summary">{props.summary}</div>
      </div>
    </Link>
  );
}
