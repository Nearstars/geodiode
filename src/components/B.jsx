import React from "react";

export default function B(props) {
  return (
    <a href={props.to} target="_blank" rel="noreferrer">
      {props.children}
    </a>
  );
}
