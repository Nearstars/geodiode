import React from "react";

export default function Credits(props) {
  return (
    <div id="credits">
      <h3>Credits</h3>
      {props.children}
    </div>
  );
}
